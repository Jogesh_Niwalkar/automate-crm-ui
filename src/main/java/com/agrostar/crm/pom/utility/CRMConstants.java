package com.agrostar.crm.pom.utility;

import java.util.Hashtable;

public class CRMConstants {
	public static final boolean GRID_RUN=false;
	
	//paths

	public static final String CHROME_DRIVER_EXE=System.getProperty("user.dir")+"/src/main/resources/chromedriver";

	public static final String PAGE_LOADING_IMAGE ="//div[contains(text(),'Please wait ...') and @class='block-ui-message ng-binding' and @ng-class='$_blockUiMessageClass']";
	
			
			//
		//	"//div[contains(text(),'Loading ...')]";
	
	//  Home PAGE : LOCATORS
	
	
	public static final String REPORTSPAGE_LINK = "//a[contains(text(),'Reports')]";
	public static final String FARMERPAGE_LINK = "//a[contains(text(),'Farmer')]";
	public static final String SEARCHORDERPAGE_LINK = "//a[contains(text(),'Search Order')]";
	public static final String PROFILEPAGE_LINK = "//a[contains(text(),'Reports')]";
	public static final String ADMINPAGE_LINK = "//a[contains(text(),'Admin')]";
	public static final String PERFORMANCEPAGE_LINK = "//a[contains(text(),'Performance')]";
	
	//public static final String LOGOUT_BUTTON ="html/body/header/nav/div/div[2]/ul[2]/li/ul/li/a";
	public static final String LOGOUT_BUTTON="//a[contains(text(),'Log Out')]";
	public static final String LOGOUT_ICON="html/body/header/nav/div/div[2]/ul[2]/li/a";
	
	
	
	
	//  LOGIN PAGE : LOCATORS
	public static final String LOGIN_USERNAME = "//input[@placeholder='User Name']";
	public static final String LOGIN_PASSWORD = "//input[@placeholder='Password']";
	public static final String LOGIN_BUTTON="//button[contains(text(),'Log in')]";
	public static final String	TOASTER_MESSAGE="//div[@class='toast-message']";   // "//div[@id='toast-container']/div/div/div/div";
	public static final String	TOASTER_TITLE="//div[@class='toast-title']"; 
	public static final String	TOAST_CONTAINER="//div[@id='toast-container']/div/div";

	
	//FARMER SEARCH PAGE : LOCATORS
	public static final String FARMER_SEARCH_INPUTBOX ="//input[@placeholder='Search by number or name']";
	public static final String FARMER_SEARCH_BUTTON="//div[@block-ui-pattern='/farmerdetails/']/div[1]/form/button";
	
	public static final String FARMER_ID_SEARCH_INPUTBOX="//input[@placeholder='Search by farmer ID']";
	public static final String FARMER_ID_SEARCH_BUTTON="//div[@block-ui-pattern='/farmerdetails/']/div[2]/form/button";
	
	public static final String FARMER_NAME_ON_FARMER_SEARCH_GRID_PART1="//div[@ng-if='fSearchCtrl.farmerData.farmers.length']/div/table/tbody/tr[";
		
	public static final String FARMER_NAME_ON_FARMER_SEARCH_GRID_PART2="]/td[2]/farmername";
	public static final String COUNT_OF_FARMER_NAME_ON_FARMER_SEARCH_GRID="//div[@ng-if='fSearchCtrl.farmerData.farmers.length']/div/table/tbody/tr";
		
			
	
	
	
	
	
	
	//CREATE NEW FARMER PROFILE FORM 
	
	public static final String SMARTPHONE_AVAILABLE_CHECKBOX = "//button[@ng-model='fProfileCtrl.smartphone']";
	public static final String LANGUAGE_DROPDOWN="//select[@ng-model='fProfileCtrl.language']";
	public static final String FIRST_NAME_INPUTBOX="//input[@uib-tooltip='First Name']";
	public static final String MIDDLE_NAME_INPUTBOX="//input[@uib-tooltip='Mid Name']";
	public static final String LAST_NAME_INPUTBOX="//input[@uib-tooltip='Last Name']";
	public static final String FARMER_TYPE_DROPDOWN="//select[@ng-model='fProfileCtrl.fType']";
	public static final String AGE_INPUTBOX="//input[@ aria-describedby='age' and @type='number']";
	public static final String DESCRIPTION_INPUTBOX = "//input[@placeholder='Description']";
	public static final String IRRIGATION_SOURCE_DROPDOWN="//select[@ng-model='fProfileCtrl.irrigationSource']";
	public static final String IRRIGATION_TYPE_DROPDOWN="//select[@ng-model='fProfileCtrl.irrigationType']";
	public static final String HEARD_ABOUT_AGROSTAR_DROPDOWN="//select[@ng-model='fProfileCtrl.heardAbtSelected']";
	public static final String LAND_HOLDING_UNIT_DROPDOWN="//select[@ng-model='fProfileCtrl.landHoldingUnit']";
	public static final String LANDHOLDING_INPUTBOX = "//input[@placeholder='Landholding']";
	public static final String VILLAGE_INPUTBOX = "//input[@placeholder='Village']";
	public static final String ADDRESS_INPUTBOX = "//textarea[@placeholder='Address']";
	public static final String SUBMIT_FARMER_DETAILS_BUTTON="//button[contains(text(),'Submit')]";
	public static final String CROP_INPUTBOX = "//input[@placeholder='Crop']";
	public static final String POST_OFFICE_INPUTBOX ="//input[@placeholder='Post Office']";
	
	public static final String TALUKA_INPUTBOX ="//input[@placeholder='Post Office']";
	public static final String DISTRICT_INPUTBOX ="//input[@placeholder='Taluka']";
	public static final String STATE_INPUTBOX ="//input[@placeholder='State']";
	public static final String PHONE_NUMBER_COUNT_ON_PROFILE ="//div[@class='col-sm-12']/ul/li";
	
	
	
	//FARMER PROFILE PAGE : LOCATORS
	public static final String FARMER_LOGOUT_BUTTON ="//button[contains(text(),'Tag Call / Farmer Log Out')]";
	public static final String PRODUCT_INQUIERY_BUTTON="//button[contains(text(),'Product Inquiry')]";
	public static final String ORDER_PLACED_BUTTON="//button[contains(text(),'Order Placed')]";
	public static final String NEW_PRODUCT_INQUIRY_BUTTON="//button[contains(text(),'New Product Inquiry')]";
	public static final String ORDER_RELATED_INQUIERY_BUTTON="//button[contains(text(),'Order related Inquiry')]";
	public static final String COMPLAINT_BUTTON="//button[contains(text(),'Complaint')]";
	public static final String AGRONOMY_INQUIERY_BUTTON="//button[contains(text(),'Agronomy Inquiry')]";
	public static final String OTHERS_BUTTON="//button[contains(text(),'Others')]";
	public static final String TAG_AND_LOGOUT_FARMER_BUTTON="//button[contains(text(),'Tag Call and Log Out Farmer')]";
	public static final String PHONE_NUMBER_INPUTBOX = "//input[@placeholder='Phone Number']";
	public static final String ADD_NUMBER_BUTTON ="//button[@class='btn btn-link btn-add-number' and @type='button' and @ng-click='fProfileCtrl.onaddNumberBtnHandler()']";
	public static final String EDIT_PROFILE_LINK ="//a[@class='farmer-profile pull-right ng-isolate-scope' and @ng-click='fStatisticCtrl.gotoFarmerProfile()']";
	
	public static final String PLACE_AN_ORDER_BUTTON ="//button[contains(text(),'Place An Order')]";
	public static final String FARMER_PROFILE_BUTTON ="//a[contains(text(),'Farmer Profile')]";
	
	// DO NOT DELETE = "html/body/div[1]/ui-view/div/div[2]/ui-view[2]/div/div[2]/a";
	
	public static final String PARTIAL_PHONE_NUBMER1="//div[@class='col-sm-12']/ul/li[";   
	public static final String PARTIAL_PHONE_NUBMER2="]/label";
	
	public static final String REMOVE_PHONE_NUMBER_PART1="//div[@class='col-sm-12']/ul/li[";   
	public static final String REMOVE_PHONE_NUMBER_PART2="]/label/button";
	
	
	public static final String FARMER_PROFILE_TITLE ="//farmername[@class='farmar-name-ellipses ng-binding ng-isolate-scope' and @farmer='fHomeCtrl.farmer']";
	public static final String FARMER_PHONE_NUMBER="//p[@class='mobile-number']/span";
	public static final String FARMER_PHONE_NUMBER_PART1="//p[@class='mobile-number']/span[";
	public static final String FARMER_PHONE_NUMBER_PART2="]";
	
	
	public static final String FARMER_ALTERNATE_PHONE_NUMBER_PART1="//p[@class='mobile-number']/span[";
	public static final String FARMER_ALTERNATE_PHONE_NUMBER_PART2="]";
	
	public static final String PHONENUMBER_COUNT_ON_PROFILE="//p[@class='mobile-number']/span";

	public static final String FARMER_ADDRESS="//span[@class='ng-binding ng-scope']/span";
	
	public static final String PRODUCTS_IN_TABLE="//div[@class='custom-list ng-scope' and @ng-if='fProductCtrl.products.length']/ul[@class='list-item ng-scope']";
	
	public static final String CHECK_OUT_BUTTON="//button[@class='btn btn-success pull-right' and @ng-click='fProductCtrl.redirectToCheckout()']";
	
	
	public static final String IS_ORDER_HISTORY_TAB_SELECTED="//a[contains(text(),'Order History')]";
	
	public static final String TICKET_HISTORY_TAB="//a[contains(text(),'Ticket History')]";
	public static final String ORDER_HISTORY_TAB_ORDER_COUNT="//div[@class='table-responsive']/table/tbody/tr";
	public static final String ORDER_HISTORY_TAB_ORDER_ID_PART1="//div[@class='table-responsive']/table/tbody/tr[";
	public static final String ORDER_HISTORY_TAB_ORDER_ID_PART2="]/td[1]/a";
	
	
	public static final String TICKET_HISTORY_TAB_TICKET_COUNT="//div[@class='table-responsive']/table/tbody/tr";
	public static final String TICKET_HISTORY_TAB_TICKET_ID_PART1="//div[@class='table-responsive']/table/tbody/tr[";
	public static final String TICKET_HISTORY_TAB_TICKET_ID_PART2="]/td[1]/button";
	
	public static final String TICKET_HISTORY_TAB_ORDER_ID_PART1="//div[@class='table-responsive']/table/tbody/tr[";
	public static final String TICKET_HISTORY_TAB_ORDER_ID_PART2="]/td[3]/a";
	
	public static final String TICKET_HISTORY_TAB_PRODUCT_NAME_PART1="//div[@class='table-responsive']/table/tbody/tr[";
	public static final String TICKET_HISTORY_TAB_PRODUCT_NAME_PART2="]/td[8]/div";
	
	
	
	public static final String ORDER_STATUS_PART1="//div[@class='table-responsive']/table/tbody/tr[";
	public static final String ORDER_STATUS_PART2="]/td[8]";
	
	
	
	public static final String ORDER_DETAILS_PAGE_ORDER_ID="//li[@class='row']/label[contains(text(),'Order No')]/following-sibling::div";
	public static final String CANCEL_ORDER_BUTTON="//button[contains(text(),'Cancel Order')]";
	public static final String ORDER_DETAILS_ORDER_STATUS="//li[@class='row']/label[contains(text(),'Order Status')]/following-sibling::div";
	public static final String ORDER_DETAILS_UNICOMMERSE_ID="//li[@class='row']/label[contains(text(),'Unicommerce Id')]/following-sibling::div";
	
	
	public static final String ADD_REAL_CASH_TO_WALLET_LINK="//li[@class='balance-count']/a[contains(text(),'Add/Remove')]";
	public static final String ADD_AGROSTAR_POINTS_TO_WALLET_LINK="//li[@class='balance-count point']/a";
	public static final String REAL_CASH_AMOUNT="//li[@class='balance-count']/div";
	public static final String AGROSTAR_POINTS="//li[@class='balance-count point']/div";
	
	
	public static final String PRODUCT_DETAILS_MODAL_PRODUCT_NAME="//h4[@class='media-heading ng-binding']";
	
	//Order CheckOut Page PAGE : LOCATORS
	
	public static final String MY_BAG_PRODUCT_COUNT="//ul[@class='list-group']/li";
	public static final String MY_BAG_PRODUCT_NAME_PART1="//ul[@class='list-group']/li[";
	public static final String MY_BAG_PRODUCT_NAME_PART2="]/ul/li/label[@ng-bind='product.ItemName']";
	public static final String CREATE_ORDER="//button[contains(text(),'Create Order')]";
	
	public static final String ORDER_CONFIRMATION_MODAL_DIALOG_CONTENT_MESSAGE="//div[@class='modal-content']/div/p[@class='msg ng-binding']";
	public static final String ORDER_CONFIRMATION_MODAL_DIALOG_ORDER_ID="//div[@class='modal-content']/div/p[@class='numbers ng-binding ng-scope']";
	public static final String ORDER_CONFIRMATION_MODAL_DIALOG_CONTENT_DETAILS="//div[@class='modal-content']/div/p[@class='msg-details ng-binding']";
	public static final String ORDER_CONFIRMATION_MODAL_DIALOG_CLOSE_BUTTON="//div[@class='modal-content']/div/div/button[@ng-click='successCtrl.close()' and contains(text(),'Close')]";
	
	public static final String BILLING_ADDRESS_LINK="//div[@class='clearfix']/label[contains(text(),'Billing Address')]/following-sibling::button";
	public static final String SHIPPING_ADDRESS_LINK="//div[@class='clearfix']/label[contains(text(),'Shipping Address')]/following-sibling::button";
	
	public static final String NO_OF_BILLING_ADDRESS_AVAILABLE_ON_PAGE="//ul[@block-ui-pattern='/address/']/li/ul/li";
	public static final String NO_OF_SHIPPING_ADDRESS_AVAILABLE_ON_PAGE="//li[@ng-hide='orderCheckoutCtrl.order.SAsameAsBA']/ul/li";
	
	public static final String BILLING_ADDRESS_VIEW_MORE_LINK="//button[contains(text(),'View More')]";
	public static final String SHIPPING_ADDRESS_VIEW_MORE_LINK="//button[@ng-if='orderCheckoutCtrl.shippingAddressLimit' and @ng-click='orderCheckoutCtrl.shippingAddressLimit=undefined']";
	
	
	public static final String SA_AS_BA_CHECKBOX="//span[@ng-model='orderCheckoutCtrl.order.SAsameAsBA']";
	public static final String EMPTY_BAG_BUTTON="//button[contains(text(),'Empty Bag')]";
	public static final String ORDER_CHECKOUT_FOOTER_SECTION="//div[@class='row active-footer-section']";
	public static final String ADD_MORE_PRODUCTS_BUTTON="//button[contains(text(),'Add more products')]";
	public static final String UPDATE_ORDER_BUTTON="//button[contains(text(),'Update Order')]";	
	public static final String ADVANCE_ORDER_DATE_PICKER="//input[@placeholder='DD-MM-YYYY']";
	
	public static final String NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART1="//ul[@block-ui-pattern='/address/']/li/ul/li[";
	public static final String NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART2="]/div[2]/label";
	
	
	public static final String FARMERS_BILLING_ADDRESS_ON_CHECKOUT_PAGE_PART1="//ul[@block-ui-pattern='/address/']/li/ul/li[";
	public static final String FARMERS_BILLING_ADDRESS_ON_CHECKOUT_PAGE_PART2="]/div[2]/label/address/span/span";
	
	public static final String FARMERS_SHIPPING_ADDRESS_ON_CHECKOUT_PAGE_PART1="//li[@ng-hide='orderCheckoutCtrl.order.SAsameAsBA']/ul/li[";
	public static final String FARMERS_SHIPPING_ADDRESS_ON_CHECKOUT_PAGE_PART2="]/div[2]/label/address/span/span";
	
	public static final String COD_AMOUNT_FOOTER_SECTION="//div[@class='row active-footer-section']";

	public static final String ADVANCED_ORDER_TODAYS_DATE="//div[@class='_720kb-datepicker-calendar-body']/a[@class='_720kb-datepicker-calendar-day ng-binding ng-scope _720kb-datepicker-active']";

	public static final String SOURCE_FOR_ORDER="//select[@ng-model='orderCheckoutCtrl.order.externalSource']";
	
	public static final String INTERNAL_NOTE_INPUTBOX="//textarea[@ng-model='orderCheckoutCtrl.order.InternalNote']";
	
	//SEARCH ORDER PAGE : LOCATORS
	
	public static final String SEARCH_ORDER_INPUTBOX="//input[@placeholder='Search by unicommerce or order ID' and @ng-model='ordersCtrl.label.SalesOrderId']";

	public static final String ORDER_IDs_LIST="//div[@class='ac-container' and @aria-autocomplete='list' and @role='listbox' and @ng-show='show_autocomplete']/ul/li/a";
	public static final String ORDER_IDs_LIST_PART1="//div[@class='ac-container' and @aria-autocomplete='list' and @role='listbox' and @ng-show='show_autocomplete']/ul/li/a[";
	public static final String ORDER_IDs_LIST_PART2="]";
	
	
	
	//ADMIN PAGE : LOCATORS
	//OFFER TAB
	
	public static final String OFFER_NEW_BUTTON="//button[contains(text(),'New') and @ui-sref='.new' and @href='#/administrator/offers/new']";
	public static final String OFFERS_TITLE="//button[ @ui-sref='app.admin.offers' and @href='#/administrator/offers']";
	public static final String OFFER_NAME_INPUTBOX="//input[@placeholder='Name' and @ng-model='offersCtrl.newOffer.offerName']";
	public static final String OFFER_DESCRIPTION_INPUTBOX="//textarea[@placeholder='Description' and @ng-model='offersCtrl.newOffer.offerDescription']";
	public static final String OFFER_VALID_FROM="//input[@placeholder='DD-MM-YYYY' and @ng-model='offersCtrl.newOffer.validFrom']";
	public static final String OFFER_VALID_TO="//input[@placeholder='DD-MM-YYYY' and @ng-model='offersCtrl.newOffer.validTo']";
	public static final String MAX_NUMBER_OF_USAGES="//input[@placeholder='No. of Usages' and @ng-model='offersCtrl.newOffer.maxNumberOfUsages']";
	public static final String SELECT_SOURCES_FOR_OFFERLINK="//button[contains(text(),'Select Sources') and @ng-click='offersCtrl.selectSources()']";
	public static final String SELECT_PRODCUCTS_LINK="//button[contains(text(),'Select Products') and @ng-click='offersCtrl.selectProducts()']";
	public static final String CREATE_OFFER_BUTTON="//button[contains(text(),'Create Offer') and @ng-click='offersCtrl.createOffer()']";
	public static final String CANCEL_OFFER_BUTTON="//button[contains(text(),'Cancel') and @ui-sref='app.admin.offers' and @href='#/administrator/offers']";
	public static final String OFFERS_TAB="//a[contains(text(),'Offers')]";
	public static final String PERCENT_DISCOUNT_VALUE="//input[@ng-model='e.value' and @placeholder='Value']";
	public static final String PERCENT_DISCOUNT_DESCRIPTION="//input[@placeholder='Description' and @ng-model='e.description']";
	
	
	public static final String OFFER_VALIDITY_START_DATE="//div[@class='_720kb-datepicker-calendar-body']/a[@class='_720kb-datepicker-calendar-day ng-binding ng-scope _720kb-datepicker-active']";
	public static final String OFFER_VALIDITY_END_DATE="//div[@class='_720kb-datepicker-calendar-body']/a";
	public static final String OFFER_TYPE_DROPDOWN="//select[@ng-model='offersCtrl.newOffer.entitlements']";
	
	public static final String ALL_SOURCES_AGROSTAR="//div[@title='AGROSTAR']/span[2]/span/input";
	public static final String SELECT_SOURCES_SUBMIT_BUTTON="//button[@type='submit' and @ng-click='sourceSelectCtrl.submit()']";
	public static final String SEARCH_PRODUCT_BY_NAME_SKU_INPUTBOX="//input[@placeholder='Search products by SKU or name']";
	public static final String SELECT_PRODUCT_SUBMIT_BUTTON="//button[@type='submit' and @ng-click='productSelectCtrl.submit()']";
	
	public static final String OFFERS_COUNT_ON_OFFERS_GRID="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr";
	
	
	public static final String OFFER_NAME_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr[";
	public static final String OFFER_NAME_ON_GRID_PART2="]/td[1]/button";
	
	public static final String OFFER_DESCRIPTION_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr[";
	public static final String OFFER_DESCRIPTION_ON_GRID_PART2="]/td[2]";

	public static final String OFFER_START_DATE_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr[";
	public static final String OFFER_START_DATE_ON_GRID_PART2="]/td[3]";
	
	public static final String OFFER_END_DATE_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr[";
	public static final String OFFER_END_DATE_ON_GRID_PART2="]/td[4]";
	
	public static final String DISABLE_OFFER_BUTTON_PART1="//div[@class='table-responsive ng-scope' and @ng-if='offersCtrl.offers.length']/table/tbody/tr[";
	public static final String DISABLE_OFFER_BUTTON_PART2="]/td[5]/button";
	
	public static final String DISABLE_OFFER_COMMENT_INPUTBOX="//textarea[@placeholder='Add Comment...' and  @ng-model='commentCtrl.comment']";
	public static final String DISABLE_OFFER_COMMENT_SUBMIT_BUTTON="//button[@type='submit' and @ng-click='commentCtrl.submit()']";
	

	public static final String  SelectedSourcesOnCreateOfferPage="//div[@class='form-group']/span[@class='col-sm-12 offer-coupon-source ng-binding ng-scope' and @ng-repeat='source in offersCtrl.newOffer.offerSources']";
	public static final String  SelectedSourcesTableOnCreateOfferPage="//table[@class='table']";
	public static final String  NumberOfRowsOnSelectProductModalDialog="//div[@class='table-responsive ng-scope' and @ng-if='productSelectCtrl.products.length']/table/tbody/tr";
	
	//COUPON TAB
	
	public static final String COUPON_TAB="//a[contains(text(),'Coupons')]";
	public static final String COUPON_NEW_BUTTON="//button[contains(text(),'New') and @ui-sref='.new' and @href='#/administrator/coupons/new']";
	
	public static final String COUPON_TITLE="//button[@ui-sref='app.admin.coupons' and @href='#/administrator/coupons']";
	public static final String COUPON_CODE_INPUTBOX="//input[@placeholder='Name'and @ng-model='couponsCtrl.newCoupon.couponName']";
	public static final String COUPON_DESCRIPTION_INPUTBOX="//textarea[@placeholder='Description' and @ng-model='couponsCtrl.newCoupon.couponDescription']";
	public static final String COUPON_VALID_FROM="//input[@placeholder='DD-MM-YYYY' and @ng-model='couponsCtrl.newCoupon.validFrom']";
	public static final String COUPON_VALID_TO="//input[@placeholder='DD-MM-YYYY' and @ng-model='couponsCtrl.newCoupon.validTo']";
	
	public static final String MAX_NUMBER_OF_ORDERS_INPUTBOX="//input[@placeholder='No. of Usages' and @ng-model='couponsCtrl.newCoupon.maxNumberOfUsages']";
	public static final String SELECT_SOURCES_FOR_COUPON_LINK="//button[contains(text(),'Select Sources') and @ng-click='couponsCtrl.selectSources()']";
	public static final String CREATE_COUPON_BUTTON="//button[contains(text(),'Create Coupon') and @ng-click='couponsCtrl.createCoupon()']";
	public static final String CANCEL_COUPON_BUTTON="//button[contains(text(),'Cancel') and @ui-sref='app.admin.coupons' and @href='#/administrator/coupons']";
	public static final String IS_FREE_FLOATING_COUPON_CHECKBOX="//span[@ng-model='couponsCtrl.newCoupon.isFreeFloating']";
	public static final String ORDER_VALUE_INPUTBOX="//input[@placeholder='Value more than' and @ng-model='couponsCtrl.newCoupon.orderValue']";
	public static final String COUPON_VALIDITY_START_DATE="//div[@class='_720kb-datepicker-calendar-body']/a[@class='_720kb-datepicker-calendar-day ng-binding ng-scope _720kb-datepicker-active']";
//	public static final String COUPON_VALIDITY_END_DATE="//div[@class='_720kb-datepicker-calendar-body']/a";

	public static final String DATE_PICKER_DATE_LIST="//div[@class='_720kb-datepicker-calendar-body']/a";//div[@class='_720kb-datepicker-calendar-body']/a
	public static final String COUPON_TYPE_DROPDOWN="//select[@ng-model='couponsCtrl.newCoupon.entitlements']";
	
	public static final String SELECT_DATE_FROM_DATE_PICKER1="//div[@class='_720kb-datepicker-calendar-body']/a[";
	public static final String SELECT_DATE_FROM_DATE_PICKER2="]";
	
	public static final String COUPON_COUNT_ON_COUPONS_GRID="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr";
	
	public static final String COUPON_NAME_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr[";
	public static final String COUPON_NAME_ON_GRID_PART2="]/td[1]/button";
	
	public static final String COUPON_DESCRIPTION_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr[";
	public static final String COUPON_DESCRIPTION_ON_GRID_PART2="]/td[2]";

	public static final String COUPON_START_DATE_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr[";
	public static final String COUPON_START_DATE_ON_GRID_PART2="]/td[3]";
	
	public static final String COUPON_END_DATE_ON_GRID_PART1="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr[";
	public static final String COUPON_END_DATE_ON_GRID_PART2="]/td[4]";
	
	public static final String DISABLE_COUPON_BUTTON_PART1="//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr[";
	public static final String DISABLE_COUPON_BUTTON_PART2="]/td[5]/button";
	
	public static final String DISABLE_COUPON_COMMENT_INPUTBOX="//textarea[@placeholder='Add Comment...' and  @ng-model='commentCtrl.comment']";
	public static final String DISABLE_COUPON_COMMENT_SUBMIT_BUTTON="//button[@type='submit' and @ng-click='commentCtrl.submit()']";
	
	
	public static final String PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1="//div[@class='table-responsive ng-scope' and @ng-if='productSelectCtrl.products.length']/table/tbody/tr[";
	public static final String PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2="]/td[3]";
	
	public static final String PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1="//div[@class='table-responsive ng-scope' and @ng-if='productSelectCtrl.products.length']/table/tbody/tr[";	
	public static final String PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2="]/td[2]";
	
	public static final String CHECKBOX_SELECTION_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1="//div[@class='table-responsive ng-scope' and @ng-if='productSelectCtrl.products.length']/table/tbody/tr[";
	public static final String CHECKBOX_SELECTION_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2="]/td[1]/span";
	
	public static final String PRODUCT_LIST="//div[@class='custom-list ng-scope' and @ng-if='fProductCtrl.products.length']/ul";
	
	
	public static final String PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1 ="//div[@class='custom-list ng-scope' and @ng-if='fProductCtrl.products.length']/ul[";
	public static final String PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2 ="]/li[3]/button";
	
	public static final String ADD_TO_CART_BUTTON_ON_PRODUCTS_PAGE_PART1 ="//div[@class='custom-list ng-scope' and @ng-if='fProductCtrl.products.length']/ul[";
	public static final String ADD_TO_CART_BUTTON_ON_PRODUCTS_PAGE_PART2 ="]/li[9]/button";
	
	public static final String ADD_TO_CART_PRODUCT_DETAILS_PAGE="//button[@ng-click='productDetailsCtrl.updateCart()']";
	
	public static final String NEXT_NAVIGATION_BUTTON="//a[contains(text(),'›')]";
	
	//public static final String PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1 ="//div[@class='custom-list ng-scope' and @ng-if='fProductCtrl.products.length']/ul/li[";
	//public static final String PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2 ="]";
	
	
	
	
	//PERFORMANCE PAGE -LOCATORS
	public static final String IS_TODAYS_SALES_TAB_SELECTED="//ul[@class='nav nav-tabs']/li[@class='active']/a";
	public static final String TODAYS_SALES_TAB="//a[contains(text(),'Today sales')]";
	public static final String ADVANCED_ORDER_TAB="//a[contains(text(),'Adv. Order')]";
	
	public static final String SALES_ORDER_ID_PART1="//div[@ng-if='aDashboardCtrl.todaysTotalOrder']/div/table/tbody/tr[";
	public static final String SALES_ORDER_ID_PART2="]/td[1]/button";
	public static final String NO_OF_ORDERS_ON_GRID="//div[@ng-if='aDashboardCtrl.todaysTotalOrder']/div/table/tbody/tr";
	
	public static final String ADVANCED_ORDER_ID_PART1="//div[@ng-if='aDashboardCtrl.orders.advance_orders.length']/div/table/tbody/tr[";
	public static final String ADVANCED_ORDER_ID_PART2="]/td[1]/button";
	public static final String NO_OF_ADVANCED_ORDERS_ON_GRID="//div[@ng-if='aDashboardCtrl.orders.advance_orders.length']/div/table/tbody/tr";

	public static final String ZERO_ISSUE_SELECTED_TEXT="//span[contains(text(),'0 Issue Selected :')]";
	public static final String NEXT_NAVIGATION_ARROW="//a[@ng-click='selectPage(page + 1, $event)']";
	
			//div[@class='table-responsive ng-scope' and @ng-if='couponsCtrl.coupons.length']/table/tbody/tr["+j+
	

	
	
	
	
	//ORDER DETAILS PAGE- LOCATORS
	public static final String CREATE_NEW_ORDER_TICKET_BUTTON="//button[contains(text(),'Create an Order Ticket')]";
	
	public static final String SELECT_ORDER_ISSUE_DROPDOWN="//select[@name='orderIssue']";
	public static final String SELECT_ITEM_SELECTED_DROPDOWN="//select[@ng-change='createTicketCtrl.onOrderItemSelection()' and @ng-model='createTicketCtrl.ticket.orderItem']";
	
	public static final String SELECT_REASON_DROPDOWN="//button[@class='dropdown-toggle']";
	
	public static final String NO_OF_ORDER_ISSUE_REASON="//ul[@class='dropdown-menu']/li";
	public static final String ORDER_ISSUE_REASON_PART1="//ul[@class='dropdown-menu']/li[";
	public static final String ORDER_ISSUE_REASON_PART2="]/button/span[1]";
	public static final String ORDER_ISSUE_REASON_CHECKBOX_PART1="//ul[@class='dropdown-menu']/li[";
	public static final String ORDER_ISSUE_REASON_CHECKBOX_PART2="]/button/span[2]";
	public static final String ORDER_ISSUE_SUBMIT_BUTTON="//button[contains(text(),'Submit')]";
	public static final String ORDER_ISSUE_REMARKS_INPUTBOX="//textarea[@name='remarks']";
	public static final String GENERATED_TICKET_ID="//button[@ng-click='orderDetailsCtrl.gotoTicketHistory(ticket.ticketUUID)']";
	public static final String TICKET_MODAL_HEADER="//div[@class='modal-header']";
	public static final String PRODUCT_NAME_PART1="//ul[@class='media-list product-media-list']/li[";	
	public static final String PRODUCT_NAME_PART2="]/div[2]//ul/li[1]/label";			
	public static final String CREATE_NEW_TICKET_ON_ITEM_LEVEL_PART1="//ul[@class='media-list product-media-list']/li[";
	public static final String CREATE_NEW_TICKET_ON_ITEM_LEVEL_PART2="]/div[2]//ul/li[6]/button";
	public static final String TICKET_ID_PART1="//ul[@class='media-list product-media-list']/li[";
	public static final String TICKET_ID_PART2="]/div[2]//ul/li[6]/div/ul/li[1]/button";
	public static final String PRODUCT_COUNT="//ul[@class='media-list product-media-list']/li";
	public static final String EDIT_ORDER_BUTTON="//button[contains(text(),'Edit Order')]";
	
   // Wallet Page
	
	public static final String UPDATE_WALLET_BUTTON="//button[contains(text(),'Update Wallet')]";
	
	public static final String CASH_TYPE_DROPDOWN="//select[@name='cashType']";
	public static final String ACCOUNT_ENTRY_TYPE_DROPDOWN="//select[@name='entryType']";
	public static final String CASH_AMOUNT="//input[@name='amount']";
	public static final String TRANSACTION_REASON="//select[@name='transactionReason']";
	public static final String TRANSACTION_REMARKS="//textarea[@name='remarks']";
	
	
	
	// URLs-prod
	public static final String PROD_HOMEPAGE_URL = "http://crmtest.agrostar.in";// I know its crm.agrostar.in....intentionally kept
	public static final String PROD_USERNAME = "nirmal";
	public static final String PROD_PASSWORD = "tirmal100$";
	
	// URLs-uat
	public static final String UAT_HOMEPAGE_URL = "http://crmtest.agrostar.in";
	public static final String UAT_USERNAME = "nirmal";
	public static final String UAT_PASSWORD = "tirmal100$";
		
	
	public static final String ENV="PROD"; //PROD, UAT,SAT 
			

	//paths
	public static final String REPORTS_PATH =System.getProperty("user.dir")+"/ExecutionReports/";
	
	
	
	public static final String DATA_XLS_PATH = System.getProperty("user.dir")+"/TestData/CRM_TEST_DATA.xlsx";
	public static final String TESTDATA_SHEET = "TestData";
	public static final Object RUNMODE_COL = "Runmode";
	public static final String TESTCASES_SHEET = "TestCases";
	
	
	
	public static final String SUITE_SHEET="Suite";
	public static final String SUITENAME_COL="SuiteName";
	
	public static Hashtable<String,String> table;
	
	public static Hashtable<String,String> getEnvDetails(){
		if(table==null){
			table = new Hashtable<String,String>();
			if(ENV.equals("PROD")){
				table.put("url", PROD_HOMEPAGE_URL);
				table.put("username", PROD_USERNAME);
				table.put("password", PROD_PASSWORD);
			}else if(ENV.equals("UAT")){
				table.put("url", UAT_HOMEPAGE_URL);
				table.put("username", UAT_USERNAME);
				table.put("password", UAT_PASSWORD);
			}
			
		}
		return table;
		 
	}




	


	


	




	



	

}
