/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;

public class WalletPage extends BasePage {


	@FindBy(xpath=CRMConstants.CASH_TYPE_DROPDOWN)
	public WebElement cashTypeDropdown;
	
	@FindBy(xpath=CRMConstants.ACCOUNT_ENTRY_TYPE_DROPDOWN)
	public WebElement accountEntryTypeDropdown;
	
	@FindBy(xpath=CRMConstants.CASH_AMOUNT)
	public WebElement cashAmount;
	
	@FindBy(xpath=CRMConstants.TRANSACTION_REASON)
	public WebElement transactionReason;
	
	@FindBy(xpath=CRMConstants.TRANSACTION_REMARKS)
	public WebElement transactionRemarks;
	
	@FindBy(xpath=CRMConstants.UPDATE_WALLET_BUTTON)
	public WebElement updateWallet_Button;
	
	@FindBy(xpath=CRMConstants.FARMER_PROFILE_BUTTON)
	public WebElement farmerProfileHomePageLink;
	
	
	

	public WalletPage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}

	public void selectValueFromCashType_Dropdown(String CashType)
	{
		wait.until(ExpectedConditions.elementToBeClickable(cashTypeDropdown));
		new Select(cashTypeDropdown).selectByVisibleText(CashType);
		
	}
	public void CashTypeDropdown_Click()
	{
		cashTypeDropdown.click();
		
	}
		
	public void AccountEntryTypeDropdown_Click()
	{
		accountEntryTypeDropdown.click();
		
	}
	public void selectValueFromAccountEntryType_Dropdown(String entryType)
	{
		wait.until(ExpectedConditions.elementToBeClickable(accountEntryTypeDropdown));
		new Select(accountEntryTypeDropdown).selectByVisibleText(entryType);
		
	}
	

	public String amount_InputBox(String Amount)
	{
		cashAmount.sendKeys(Amount);
		return Amount;
	}
	
	public void transactionReason_Click()
	{
		transactionReason.click();
		
	}
	public void transactionReason_InputBox(String TransactionReason)
	{
		wait.until(ExpectedConditions.elementToBeClickable(transactionReason));
		new Select(transactionReason).selectByVisibleText(TransactionReason);
		
	}
	
	
	public void remarks_InputBox(String remarks)
	{
		transactionRemarks.sendKeys(remarks);
		
	}
	public Object isNavigatedToWalletPage()
	{	
		boolean isUpdateWalletButtonAvailable=isElementPresent(CRMConstants.UPDATE_WALLET_BUTTON);
		if(isUpdateWalletButtonAvailable)
		{
			
			WalletPage walletPage=new WalletPage(driver, test);
			PageFactory.initElements(driver, walletPage);
			return walletPage;
		}
		else
		{	FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
			PageFactory.initElements(driver, farmerProfilePage);
			return farmerProfilePage;
		}
		
	}
	
	public void updateWalletButton()
	{
		wait.until(ExpectedConditions.elementToBeClickable(updateWallet_Button));
		updateWallet_Button.click();
		
	}
	
	public void farmerProfile_Button()
	{	
		
		wait.until(ExpectedConditions.elementToBeClickable(farmerProfileHomePageLink));
		farmerProfileHomePageLink.click();
		
	}
}
