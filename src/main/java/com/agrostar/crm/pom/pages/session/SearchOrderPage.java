/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;

public class SearchOrderPage extends BasePage {

	
	@FindBy(xpath=CRMConstants.SEARCH_ORDER_INPUTBOX)
	public WebElement searchOrderInputBox;
	
	@FindBy(xpath=CRMConstants.ORDER_IDs_LIST)
	public WebElement orderIdsList;
	
	
	

	public SearchOrderPage(WebDriver driver,ExtentTest test ){

		super(driver,test);

	}
	

	public void searchOrderByOrderId(String OrderId)
	{		
		searchOrderInputBox.sendKeys(OrderId);
		takeScreenShot();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		searchOrderInputBox.sendKeys(Keys.DOWN);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		searchOrderInputBox.sendKeys(Keys.ENTER);
		
	}

	public void selectOrderByOrderId(String ExpectedOrderId)
	{	
		
		
		//wait =new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.visibilityOf(orderIdsList));
		
		int orderListCount=driver.findElements(By.xpath(CRMConstants.ORDER_IDs_LIST)).size();
		for(int i=1;i<=orderListCount;i++)
		{	
			String availableOrderIdOnPage=driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).getText();
			String orderIdTrimmed=availableOrderIdOnPage.substring(15);
			System.out.println(availableOrderIdOnPage);
			if(orderIdTrimmed.equals(ExpectedOrderId))
			{	
				String xpath=CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
				driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).click();
				waitForPageLoad();
			//	return availableOrderIdOnPage;
				
			}
		}
		
	}
	
	public void searchOrderByUnicommerceId(String uniCommerseId)
	{
		searchOrderInputBox.sendKeys(uniCommerseId);
		takeScreenShot();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		searchOrderInputBox.sendKeys(Keys.DOWN);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		searchOrderInputBox.sendKeys(Keys.ENTER);
	}

	public void searchAndSelectOrderByUnicommerceId(String uniCommerseId)
	{	
		
		searchOrderInputBox.sendKeys(uniCommerseId);
		takeScreenShot();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	searchOrderInputBox.sendKeys(Keys.DOWN);
		//wait =new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.visibilityOf(orderIdsList));
		
		int orderListCount=driver.findElements(By.xpath(CRMConstants.ORDER_IDs_LIST)).size();
		for(int i=1;i<=orderListCount;i++)
		{	
			String availableOrderIdOnPage=driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).getText();
			String orderIdTrimmed=availableOrderIdOnPage.substring(39);
			System.out.println(availableOrderIdOnPage);
			if(orderIdTrimmed.equals(uniCommerseId))
			{	
				String xpath=CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
				driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).click();
				waitForPageLoad();
			
				
			}
			
		}

}
	
	public void searchAndSelectOrderByOrderId(String orderId)
	{	
		
		searchOrderInputBox.sendKeys(orderId);
		takeScreenShot();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	//	searchOrderInputBox.sendKeys(Keys.DOWN);
		//wait =new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.visibilityOf(orderIdsList));
		
		int orderListCount=driver.findElements(By.xpath(CRMConstants.ORDER_IDs_LIST)).size();
		for(int i=1;i<=orderListCount;i++)
		{	
			String availableOrderIdOnPage=driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).getText();
			String orderIdTrimmed=availableOrderIdOnPage.substring(15);
			System.out.println(availableOrderIdOnPage);
			if(orderIdTrimmed.equals(orderId))
			{	
				String xpath=CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
				driver.findElement(By.xpath(CRMConstants.ORDER_IDs_LIST_PART1+i+CRMConstants.ORDER_IDs_LIST_PART2)).click();
				waitForPageLoad();
			
				
			}
			
		}

}
	

}
