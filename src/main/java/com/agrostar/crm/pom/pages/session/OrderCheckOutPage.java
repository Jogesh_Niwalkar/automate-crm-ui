/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import static org.testng.Assert.expectThrows;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.agrostar.crm.pom.base.BasePage;

import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class OrderCheckOutPage extends BasePage{
	
	@FindBy(xpath=CRMConstants.MY_BAG_PRODUCT_COUNT)
	public WebElement myBagProductCount;

	@FindBy(xpath=CRMConstants.CREATE_ORDER)
	public WebElement createOrder;
	
	@FindBy(xpath=CRMConstants.ORDER_CONFIRMATION_MODAL_DIALOG_CONTENT_MESSAGE)
	public WebElement orderConfirmationContentMessage;
	
	@FindBy(xpath=CRMConstants.ORDER_CONFIRMATION_MODAL_DIALOG_ORDER_ID)
	public WebElement orderId_ForConfirmedOrder;
	
	@FindBy(xpath=CRMConstants.ORDER_CONFIRMATION_MODAL_DIALOG_CONTENT_DETAILS)
	public WebElement orderConfirmationContentDetails;
	
	@FindBy(xpath=CRMConstants.ORDER_CONFIRMATION_MODAL_DIALOG_CLOSE_BUTTON)
	public WebElement orderConfirmedModalDialog_CloseButton;
	
	@FindBy(xpath=CRMConstants.BILLING_ADDRESS_LINK)
	public WebElement addNewBillingAddress_link;
	
	@FindBy(xpath=CRMConstants.SHIPPING_ADDRESS_LINK)
	public WebElement addNewShippingAddress_link;
	
	@FindBy(xpath=CRMConstants.VILLAGE_INPUTBOX)
	public WebElement village_InputBox;
	
	@FindBy(xpath=CRMConstants.ADDRESS_INPUTBOX)
	public WebElement address_InputBox;
	
	@FindBy(xpath=CRMConstants.SUBMIT_FARMER_DETAILS_BUTTON)
	public WebElement submitNewAddressButton;
	

	@FindBy(xpath=CRMConstants.SA_AS_BA_CHECKBOX)
	public WebElement SAasBA_Checkbox;
	
	@FindBy(xpath=CRMConstants.EMPTY_BAG_BUTTON)
	public WebElement emptyBagButton;
	

	@FindBy(xpath=CRMConstants.POST_OFFICE_INPUTBOX)
	public WebElement postOfficeInputBox;
	
	@FindBy(xpath=CRMConstants.TALUKA_INPUTBOX)
	public WebElement talukaInputbox;
	
	@FindBy(xpath=CRMConstants.DISTRICT_INPUTBOX)
	public WebElement districtInputbox;
	@FindBy(xpath=CRMConstants.STATE_INPUTBOX)
	public WebElement stateInputbox;
	
	@FindBy(xpath=CRMConstants.BILLING_ADDRESS_VIEW_MORE_LINK)
	public WebElement billingAddress_viewMoreLink;
	
	@FindBy(xpath=CRMConstants.SHIPPING_ADDRESS_VIEW_MORE_LINK)
	public WebElement shippingAddress_viewMoreLink;
	
	@FindBy(xpath=CRMConstants.COD_AMOUNT_FOOTER_SECTION)
	public WebElement codAmountFooterSection;
	
	@FindBy(xpath=CRMConstants.ADD_MORE_PRODUCTS_BUTTON)
	public WebElement addMoreProductsButton;
	
	@FindBy(xpath=CRMConstants.UPDATE_ORDER_BUTTON)
	public WebElement updateAnOrderButton;
	
	@FindBy(xpath=CRMConstants.ADVANCE_ORDER_DATE_PICKER)
	public WebElement advanceOrderDatePicker;
	
	@FindBy(xpath=CRMConstants.ADVANCED_ORDER_TODAYS_DATE)
	public WebElement advancedOrderTodaysDate;
	
	@FindBy(xpath=CRMConstants.SOURCE_FOR_ORDER)
	public WebElement orderSource;

	@FindBy(xpath=CRMConstants.INTERNAL_NOTE_INPUTBOX)
	public WebElement internalNoteInputbox;
	
	int TodaysDate;
	int advanceOrderDate;
	int advanceOrderDate_userValue;
	//input[@ng-model='addressCtrl.taluka' and @placeholder='Taluka']
	

	 public OrderCheckOutPage(WebDriver driver,ExtentTest test)
	 {
		super(driver,test);
	 }
	 
	
	 public void advanceOrderDate_InputBox()
	 {
		 wait.until(ExpectedConditions.visibilityOf(advanceOrderDatePicker));
		 advanceOrderDatePicker.click();
	 }
	 
	 public void selectSource_Inputbox()
	 {
		 wait.until(ExpectedConditions.visibilityOf(orderSource));
		 orderSource.click();
	 }
	 
	 public void selectSource_Dropdown(String source)
	 {
		new Select(orderSource).selectByVisibleText(source);
	 }
	 
	 
	 public void internalNote_Inputbox(String internalNote)
	 {
		 internalNoteInputbox.sendKeys(internalNote);
	 }
	 
	 
		public int advancedOrderDate()
		{
			TodaysDate=Integer.parseInt(advancedOrderTodaysDate.getAttribute("text"));
			advanceOrderDate_userValue=TodaysDate+3;
			java.util.List<WebElement> CalenderDatelist=driver.findElements(By.xpath(CRMConstants.DATE_PICKER_DATE_LIST));

			for(int i=1;i<=CalenderDatelist.size();i++)
			{
				String getAdvanceOrderDate=driver.findElement(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).getAttribute("text");
				advanceOrderDate=Integer.parseInt(getAdvanceOrderDate);
				if(advanceOrderDate_userValue==advanceOrderDate)
				{

					int var_ele_size =driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).size();
					driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).get(var_ele_size-1).click();
					return advanceOrderDate;
				}	
			}
			
			return 0;
		}
	 
	 
	 
	 public String productsIn_MyBag(String expectedProductinBag)
	 {
		 int noOfProductsInMyBag=driver.findElements(By.xpath(CRMConstants.MY_BAG_PRODUCT_COUNT)).size();
		 for(int i=1;i<=noOfProductsInMyBag;i++)
		 {
			 String productInBag=driver.findElement(By.xpath(CRMConstants.MY_BAG_PRODUCT_NAME_PART1+i+CRMConstants.MY_BAG_PRODUCT_NAME_PART2)).getText();
			 if(productInBag.equalsIgnoreCase(expectedProductinBag))
			 {
				
				 
				 return productInBag;
				 
			 }
			
		 }
		 
		 takeScreenShot();
		 return " Product Not Found In My Bag ";
	
	 }
	 
	 public void SA_AS_BA_Checkbox()
	 {
		 
		 try 
		    {
			 	Thread.sleep(1500);
			 } catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
		//wait.until(ExpectedConditions.visibilityOf(codAmountFooterSection));
				//visibilityOfElementLocated("//div[@class='row active-footer-section']"));
		//((org.openqa.selenium.JavascriptExecutor) driver).executeScript("scroll(0, 350)");
		 wait.until(ExpectedConditions.visibilityOf(SAasBA_Checkbox));
		wait.until(ExpectedConditions.elementToBeClickable(SAasBA_Checkbox));
		
		Actions actions = new Actions(driver);

		actions.moveToElement(SAasBA_Checkbox).click().perform();
		//((org.openqa.selenium.JavascriptExecutor) driver).executeScript("arguments[0].click();", SAasBA_Checkbox);
		//SAasBA_Checkbox.click();
	   
		 
	 }
	 public void createOrder_Button()
	 {
		 wait.until(ExpectedConditions.elementToBeClickable(createOrder));	
		 createOrder.click();
	 }
	 
	 public void addNewBillingAddress_link()
	 {
		 wait.until(ExpectedConditions.elementToBeClickable(addNewBillingAddress_link));
		 addNewBillingAddress_link.click();
	 }
	 
	 public void addNewShippingAddress_link()
	 {	((org.openqa.selenium.JavascriptExecutor) driver).executeScript("scroll(0,100)");
	//	 wait.until(ExpectedConditions.elementToBeClickable(addNewShippingAddress_link));
	 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 
	 	addNewShippingAddress_link.click();
	 }
	 
	 public void village_InputBox(String Village)
	 {
		 
		 village_InputBox.sendKeys(Village);
		 try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 village_InputBox.sendKeys(Keys.ENTER);
		 village_InputBox.sendKeys(Keys.DOWN);	
		 
	 }
	
	 public boolean retrivePO_PinCode_Taluka_District_State()
	 {	
		 String PostOfficeAvailableOnPage=postOfficeInputBox.getText();
		 String TalukaAvailableOnPage=talukaInputbox.getText();
		 String DistrictAvailableOnPage=districtInputbox.getText();
		 String StateAvailableOnPage= stateInputbox.getText();
		 
		 if(!PostOfficeAvailableOnPage.isEmpty() && !TalukaAvailableOnPage.isEmpty() && !DistrictAvailableOnPage.isEmpty() && !StateAvailableOnPage.isEmpty())
		 {
			return true; 
		 }
		 else
		 {
			 return false;
		 }
		 
	 }
	 
	 
	 
	public boolean  verifyCompleteBillingAddressOnCheckoutPage(String Village, String expectedBillingAddress)
	{
		//boolean isPO_Tah_Dis_StateAvailable=retrivePO_PinCode_Taluka_District_State();
		
		int billingAddressesCount=driver.findElements(By.xpath(CRMConstants.NO_OF_BILLING_ADDRESS_AVAILABLE_ON_PAGE)).size();
	//	int addressesCount=driver.findElements(By.xpath(CRMConstants.NO_OF_BILLING_ADDRESS_AVAILABLE_ON_PAGE)).size();
		
		
		for (int i=1;i<=billingAddressesCount;i++ )
		{
			String billingAddressOnPage =driver.findElement(By.xpath(CRMConstants.FARMERS_BILLING_ADDRESS_ON_CHECKOUT_PAGE_PART1+i+ CRMConstants.FARMERS_BILLING_ADDRESS_ON_CHECKOUT_PAGE_PART2)).getText();
		//	String NameOnPage=driver.findElement(By.xpath(CRMConstants.NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART1+i+ CRMConstants.NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART2)).getText();
			if(billingAddressOnPage.contains(expectedBillingAddress))
					
			{
				takeScreenShot();
				return true;
				
			}
			
		
		}
		
		takeScreenShot();
		return false;
	
	}
	
	public boolean  verifyCompleteShippingAddressOnCheckoutPage(String Village, String expectedShippingAddress)
	{
		//boolean isPO_Tah_Dis_StateAvailable=retrivePO_PinCode_Taluka_District_State();
		
		int shippingaddressesCount=driver.findElements(By.xpath(CRMConstants.NO_OF_SHIPPING_ADDRESS_AVAILABLE_ON_PAGE)).size();
	//	int addressesCount=driver.findElements(By.xpath(CRMConstants.NO_OF_BILLING_ADDRESS_AVAILABLE_ON_PAGE)).size();
		
		
		for (int i=1;i<=shippingaddressesCount;i++ )
		{
			String shippingAddressOnPage =driver.findElement(By.xpath(CRMConstants.FARMERS_SHIPPING_ADDRESS_ON_CHECKOUT_PAGE_PART1+i+ CRMConstants.FARMERS_SHIPPING_ADDRESS_ON_CHECKOUT_PAGE_PART2)).getText();
		//	String NameOnPage=driver.findElement(By.xpath(CRMConstants.NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART1+i+ CRMConstants.NAME_OF_FARMER_ON_CHECKOUT_PAGE_PART2)).getText();
			if(shippingAddressOnPage.contains(expectedShippingAddress))
					
			{
				takeScreenShot();
				return true;
				
			}
			
		
		}
		
		return false;
	
	}
	
	
	
	public void billingAddress_viewMore_link()
	{
		((org.openqa.selenium.JavascriptExecutor) driver).executeScript("scroll(0,200)");
		 wait.until(ExpectedConditions.elementToBeClickable(billingAddress_viewMoreLink));
		billingAddress_viewMoreLink.click();
		takeScreenShot();
	}
	 
	public void shippingAddress_viewMore_link()
	{
		((org.openqa.selenium.JavascriptExecutor) driver).executeScript("scroll(0,400)");
		 wait.until(ExpectedConditions.elementToBeClickable(shippingAddress_viewMoreLink));
		shippingAddress_viewMoreLink.click();
		takeScreenShot();
	}
	public void emptyBag_Button()
	{
		 wait.until(ExpectedConditions.elementToBeClickable(emptyBagButton));
		emptyBagButton.click();
	}
	
	
	
	
	public String getVillageFromCheckoutPage()
	 {
		
		 
		 return "";
	 }
	 public String getAddressFromCheckoutPage()
	 {
		 
		 
		
		 
		 
		 return "";
	 }
	
	 
	 
	 
	 public void address_InputBox(String Address)
	 { 
		 address_InputBox.sendKeys(Address);
		 
	 }
	 
	 public void submitBillingNewAddress_Button()
	 { 
		 wait.until(ExpectedConditions.elementToBeClickable(submitNewAddressButton));
		 submitNewAddressButton.click();
	 }
	 
	 
	 public String orderConfirmation_ModalDialog(String expectedOrderSuccessContentMessage, String expectedorderSuccessContentDetails)
	 {		
		 	boolean isOrderIdElementAvailableOnModalDialog=isElementPresent(CRMConstants.ORDER_CONFIRMATION_MODAL_DIALOG_ORDER_ID);
		 	
		 	
		 
		 	if(isOrderIdElementAvailableOnModalDialog)
		 	{	
		 		
		 		try {
					Thread.sleep(1450);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 		String modalDialog_ContentMessage=orderConfirmationContentMessage.getText();
			 	String orderId=orderId_ForConfirmedOrder.getText();
			 	String ModalDialog_ContentDetails=orderConfirmationContentDetails.getText();
		 		
		 		if(modalDialog_ContentMessage.contains(expectedOrderSuccessContentMessage)&& ModalDialog_ContentDetails.contains(expectedorderSuccessContentDetails))
		 		{
		 			test.log(LogStatus.INFO, " Order Confirmation Modal Dialog has been launched successfully with text shown as below...");
		 			takeScreenShot();
		 			test.log(LogStatus.INFO, modalDialog_ContentMessage);
		 			test.log(LogStatus.INFO, ModalDialog_ContentDetails);
		 			return orderId;
		 		}
		 		else
		 		{	
		 			return "";
		 			
		 		}
		 		
		 		
		 	}
		 
		return ""; 	
		//
		 
	 }
	 
	 public void closeModalDialog()
	 {
		 wait.until(ExpectedConditions.elementToBeClickable(orderConfirmedModalDialog_CloseButton));
		 orderConfirmedModalDialog_CloseButton.click();
	 }
	 
	 public void addMoreProducts_Button()
	 {
		 wait.until(ExpectedConditions.elementToBeClickable(addMoreProductsButton));
		 addMoreProductsButton.click();
	 }
	 
	 public void updateAnOrder_Button()
	 {
		 wait.until(ExpectedConditions.elementToBeClickable(updateAnOrderButton));
		 updateAnOrderButton.click();
	 }
	 

	 //

}
