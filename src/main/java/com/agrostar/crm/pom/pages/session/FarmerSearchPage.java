/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.pages.session;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class FarmerSearchPage extends BasePage {


	String responseFromServer;
	@FindBy(xpath=CRMConstants.FARMER_SEARCH_INPUTBOX)
	public WebElement farmerSearchInputBox;

	@FindBy(xpath=CRMConstants.FARMER_SEARCH_BUTTON)
	public WebElement farmerSearchButton;
	
	@FindBy(xpath=CRMConstants.FARMER_ID_SEARCH_INPUTBOX)
	public WebElement farmerIdSearchInputBox;
	
	@FindBy(xpath=CRMConstants.FARMER_ID_SEARCH_BUTTON)
	public WebElement farmerIdSearchButton;
	


	@FindBy(xpath=CRMConstants.SMARTPHONE_AVAILABLE_CHECKBOX)
	public WebElement smartphoneAvailableCheckBox;

	@FindBy(xpath=CRMConstants.LANGUAGE_DROPDOWN)
	public WebElement languageDropdown;

	@FindBy(xpath=CRMConstants.MIDDLE_NAME_INPUTBOX)
	public WebElement middleNameInputBox;

	@FindBy(xpath=CRMConstants.LAST_NAME_INPUTBOX)
	public WebElement LastNameInputBox;

	@FindBy(xpath=CRMConstants.FIRST_NAME_INPUTBOX)
	public WebElement firstNameInputBox;

	@FindBy(xpath=CRMConstants.FARMER_TYPE_DROPDOWN)
	public WebElement farmerTypeDropdown;

	@FindBy(xpath=CRMConstants.AGE_INPUTBOX)
	public WebElement ageInputBox;

	@FindBy(xpath=CRMConstants.DESCRIPTION_INPUTBOX)
	public WebElement descriptionInputbox;

	@FindBy(xpath=CRMConstants.IRRIGATION_SOURCE_DROPDOWN)
	public WebElement irrigationSourceDropdown;

	@FindBy(xpath=CRMConstants.IRRIGATION_TYPE_DROPDOWN)
	public WebElement irrigationTypeDropdown;

	@FindBy(xpath=CRMConstants.HEARD_ABOUT_AGROSTAR_DROPDOWN)
	public WebElement heardAboutAgroStarDropdown;

	@FindBy(xpath=CRMConstants.LAND_HOLDING_UNIT_DROPDOWN)
	public WebElement landHoldingUnitDropdown;

	@FindBy(xpath=CRMConstants.LANDHOLDING_INPUTBOX)
	public WebElement landHoldingInputbox;

	@FindBy(xpath=CRMConstants.VILLAGE_INPUTBOX)
	public WebElement villageInputBox;

	@FindBy(xpath=CRMConstants.ADDRESS_INPUTBOX)
	public WebElement addressInputBox;

	@FindBy(xpath=CRMConstants.SUBMIT_FARMER_DETAILS_BUTTON)
	public WebElement submitFarmerDetailsButton;

	@FindBy(xpath=CRMConstants.CROP_INPUTBOX)
	public WebElement cropInputBox;


	@FindBy(xpath=CRMConstants.POST_OFFICE_INPUTBOX)
	public WebElement postOfficeInputBox;

	@FindBy(xpath=CRMConstants.TOASTER_MESSAGE)
	public WebElement toasterMessage;	

	@FindBy(xpath=CRMConstants.PHONE_NUMBER_INPUTBOX)
	public WebElement phoneNumberInputBox;

	@FindBy(xpath=CRMConstants.ADD_NUMBER_BUTTON)
	public WebElement addNumberButton;


	@FindBy(xpath=CRMConstants.PARTIAL_PHONE_NUBMER1)
	public WebElement phoneNumber_text1;

	@FindBy(xpath=CRMConstants.PARTIAL_PHONE_NUBMER2)
	public WebElement phoneNumber_text2;
	
	@FindBy(xpath=CRMConstants.PERFORMANCEPAGE_LINK)
	public WebElement performancePageLink;
	

	public FarmerSearchPage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}

	public Object farmerSearchByPhoneNumber(String PhoneNumber){

		
		
		
		test.log(LogStatus.INFO, "Entering  the Farmer's Phone Number : "+ PhoneNumber+" ...");
		farmerSearchInputBox.sendKeys(PhoneNumber);
		takeScreenShot();
		wait.until(ExpectedConditions.elementToBeClickable(farmerSearchButton));
		farmerSearchButton.click();
		waitForPageLoad();
		boolean isFarmerProfilePage=isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);
		if(isFarmerProfilePage)
		{
			FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
			PageFactory.initElements(driver, farmerProfilePage);
			farmerProfilePage.takeScreenShot();
			return farmerProfilePage;
			
		}
		else
		{		
			FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
			PageFactory.initElements(driver, farmerSearchPage);
			farmerSearchPage.takeScreenShot();
			return farmerSearchPage;
			
		}

	}
	
public Object searchFarmerByFarmerId(String farmer_Id)
{

		
		
		
		test.log(LogStatus.INFO, "Entering  the Farmer's Id : "+ farmer_Id+" ...");
		farmerIdSearchInputBox.sendKeys(farmer_Id);
		takeScreenShot();
		wait.until(ExpectedConditions.elementToBeClickable(farmerIdSearchButton));
		farmerIdSearchButton.click();
		waitForPageLoad();
		boolean isFarmerProfilePage=isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);
		if(isFarmerProfilePage)
		{
			FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
			PageFactory.initElements(driver, farmerProfilePage);
			farmerProfilePage.takeScreenShot();
			return farmerProfilePage;
			
		}
		else
		{		
			FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
			PageFactory.initElements(driver, farmerSearchPage);
			farmerSearchPage.takeScreenShot();
			return farmerSearchPage;
			
		}

	}
	
	public Object goToPerformancePageFromFarmerSearchPage()
	{
		test.log(LogStatus.INFO, "Going to Performance Page... ");
		boolean isPerformanceTabClickable=isElementPresent(CRMConstants.PERFORMANCEPAGE_LINK);
		
		if (isPerformanceTabClickable)
		{	
			test.log(LogStatus.INFO, "Clicking on Performance Tab ");
			wait.until(ExpectedConditions.elementToBeClickable(farmerSearchButton));
			performancePageLink.click();
	//		waitForPageLoad();
			test.log(LogStatus.INFO, "Click Success!");	
			takeScreenShot();
			PerformancePage performancePage=new PerformancePage(driver, test);
			PageFactory.initElements(driver, performancePage);
			return performancePage;
		}
		else
		{	
			//test.log(LogStatus.FAIL, "Element Not Found : Unable to Click on Farmer Tab -  ");
			FarmerSearchPage farmerSearchPage = new FarmerSearchPage(driver,test);
			PageFactory.initElements(driver, farmerSearchPage);
			farmerSearchPage.takeScreenShot();
			return farmerSearchPage;
		}
	}
	
	public Object farmerSearchByName(String Farmername){

		
		test.log(LogStatus.INFO, "Entering  the Farmer Name: "+ Farmername+" ...");
		farmerSearchInputBox.sendKeys(Farmername);
		wait.until(ExpectedConditions.elementToBeClickable(farmerSearchButton));
		farmerSearchButton.click();
		waitForPageLoad();
		boolean isFarmerProfilePage=isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);
		if(isFarmerProfilePage)
		{
			FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
			PageFactory.initElements(driver, farmerProfilePage);
			farmerProfilePage.takeScreenShot();
			return farmerProfilePage;
			
		}
		else
		{		
			FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
			PageFactory.initElements(driver, farmerSearchPage);
			farmerSearchPage.takeScreenShot();
			return farmerSearchPage;
			
		}
		

	}

	public boolean isFarmerNameFromSearchFarmerGridSelected(String FarmerName)
	{	
		int FarmersCountOnSearchFarmerGrid=driver.findElements(By.xpath(CRMConstants.COUNT_OF_FARMER_NAME_ON_FARMER_SEARCH_GRID)).size();
		for(int i=1;i<FarmersCountOnSearchFarmerGrid;i++)
		{
			String getFarmerNameFromGrid=driver.findElement(By.xpath(CRMConstants.FARMER_NAME_ON_FARMER_SEARCH_GRID_PART1+i+CRMConstants.FARMER_NAME_ON_FARMER_SEARCH_GRID_PART2)).getText();
			
			if(getFarmerNameFromGrid.equals(FarmerName))
			{
				driver.findElement(By.xpath(CRMConstants.FARMER_NAME_ON_FARMER_SEARCH_GRID_PART1+i+CRMConstants.FARMER_NAME_ON_FARMER_SEARCH_GRID_PART2)).click();
				waitForPageLoad();
				return true;
			}
		
		}
		return false;
		
	}
	
	
	public void switch_To_New_Farmer_Form()
	{
		test.log(LogStatus.INFO, "Opening the Farmer Profile Form....");
		driver.switchTo().activeElement();
	}

	public void selectLanguage_Dropdown(String Language)
	{
		new Select(languageDropdown).selectByVisibleText(Language);
	}
	
	public void smartPhoneAvailable_Checkbox()
	{
		wait.until(ExpectedConditions.elementToBeClickable(smartphoneAvailableCheckBox));
		smartphoneAvailableCheckBox.click();
	}
	
	public void firstName_InputBox(String FirstName)
	{
		firstNameInputBox.sendKeys(FirstName);
	}
	
	public void middleName_InputBox(String MiddleName)
	{
		middleNameInputBox.sendKeys(MiddleName);
	}
	
	public void LastName_InputBox(String LastName)
	{
		LastNameInputBox.sendKeys(LastName);
	}
	
	public void selectFarmerType_Dropdown(String farmerType)
	{
		new Select(farmerTypeDropdown).selectByVisibleText(farmerType);
	}
	
	public void farmersAge_InputBox(String Age)
	{
		ageInputBox.sendKeys(Age);
	}	
	
	public void description_InputBox(String Description)
	{
		descriptionInputbox.sendKeys(Description);
	}
	public void selectIrrigationSource_Dropdown(String IrrigationSource)
	{
		new Select(irrigationSourceDropdown).selectByVisibleText(IrrigationSource);
	}
	
	public void selectIrrigationType_Dropdown(String IrrigationType)
	{
		new Select(irrigationTypeDropdown).selectByVisibleText(IrrigationType);
	}
	
	public void crop_InputBox(String crop)
	{
		cropInputBox.sendKeys(crop);
		cropInputBox.sendKeys(Keys.ENTER);
	}	
	
	public void selectHeardAboutAgroStar_Dropdown(String HeardAboutAgroStar)
	{
		new Select(heardAboutAgroStarDropdown).selectByVisibleText(HeardAboutAgroStar);
	}
	
	public void landHolding_InputBox(String LandHolding)
	{
		landHoldingInputbox.sendKeys(LandHolding);
	}
	
	public void selectLandHoldingUnit_Dropdown(String LandHoldingUnits)
	{
		new Select(landHoldingUnitDropdown).selectByVisibleText(LandHoldingUnits);
	}
	
	public void village_InputBox(String Village)
	{
		villageInputBox.sendKeys(Village);

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		villageInputBox.sendKeys(Keys.ENTER);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		villageInputBox.sendKeys(Keys.DOWN);
	}
	
	public void address_InputBox(String Address)
	{
		addressInputBox.sendKeys(Address);
	}
	
	
	public void submitFarmerDetails_Button()
	{	
		test.log(LogStatus.INFO, "Clicking on Submit Button to create New Farmer Profile..");
		wait.until(ExpectedConditions.elementToBeClickable(submitFarmerDetailsButton));
		submitFarmerDetailsButton.click();
	
	}
	
	
	public void  phoneNumber_InputBox(String NewPhoneNumber)
	{
		phoneNumberInputBox.sendKeys(NewPhoneNumber);
	}
	
	public void  addPhoneNumber_Button()
	{
		addNumberButton.click();
	}

	public int getPhoneNumbersCountFromProfile()
	{
		int CountForPhoneNumbers = driver.findElements(By.xpath(CRMConstants.PHONE_NUMBER_COUNT_ON_PROFILE)).size();
		return CountForPhoneNumbers;
	}
	
	public void clearVillage_InputBox()
	{
		villageInputBox.clear();
	}
	public void clearAddress_InputBox()
	{
		addressInputBox.clear();
		
	}
	
	/*
	//Overloaded Function to Create Farmer Profile with all Fields (Mandate and Non Mandate)
	public Object createNewFarmerProfile(String PhoneNumber,String Language,String FirstName,String MiddleName,String LastName,
			String FarmerType, String Age,String Description,String IrrigationSource,
			String  IrrigationType, String Crop1,String Crop2,String Crop3, String  HeardAboutAgroStar, 
			String LandHolding ,String LandHoldingUnits,String Village,String VillageToSelect, String Address)	                                   

	{
		
		waitForPageLoad();
		wait.until(ExpectedConditions.elementToBeClickable(farmerSearchButton));
		farmerSearchButton.click();
		waitForPageLoad();

		//Verifying the Farmer Logout Button is clickable or not.
		boolean farmerLogoutButtonClickable=isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);

		if(farmerLogoutButtonClickable)
		{
			FarmerProfilePage farmerProfilePage = new FarmerProfilePage(driver,test);
			PageFactory.initElements(driver, farmerProfilePage);
			return farmerProfilePage;

		}
		else
		{
			//waitForPageLoad();

			test.log(LogStatus.INFO, "Opening the Farmer Profile Form....");
			driver.switchTo().activeElement();

			new Select(languageDropdown).selectByVisibleText(Language);
			
			
			wait.until(ExpectedConditions.elementToBeClickable(smartphoneAvailableCheckBox));
			smartphoneAvailableCheckBox.click();
			firstNameInputBox.sendKeys(FirstName);
			middleNameInputBox.sendKeys(MiddleName);
			LastNameInputBox.sendKeys(LastName);
			new Select(farmerTypeDropdown).selectByVisibleText(FarmerType);
			ageInputBox.sendKeys(Age);
			descriptionInputbox.sendKeys(Description);
			new Select(irrigationSourceDropdown).selectByVisibleText(IrrigationSource);
			new Select(irrigationTypeDropdown).selectByVisibleText(IrrigationType);
			cropInputBox.sendKeys(Crop1);
			cropInputBox.sendKeys(Keys.ENTER);
			cropInputBox.sendKeys(Crop2);
			cropInputBox.sendKeys(Keys.ENTER);
			cropInputBox.sendKeys(Crop3);
			cropInputBox.sendKeys(Keys.ENTER);
			new Select(heardAboutAgroStarDropdown).selectByVisibleText(HeardAboutAgroStar);
			landHoldingInputbox.sendKeys(LandHolding);
			new Select(landHoldingUnitDropdown).selectByVisibleText(LandHoldingUnits);
			villageInputBox.sendKeys(Village);
			
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			villageInputBox.sendKeys(Keys.ENTER);
			villageInputBox.sendKeys(Keys.DOWN);
			selectOptionWithIndex(1);
			addressInputBox.sendKeys(Address);
			wait.until(ExpectedConditions.elementToBeClickable(submitFarmerDetailsButton));
			submitFarmerDetailsButton.click();

			
			
			
			
			
			
			
			boolean isAnyServerResponseInToaster=isElementPresent(CRMConstants.TOASTER_MESSAGE);

			if (isAnyServerResponseInToaster)
			{	
				responseFromServer=toasterMessage.getText();
				takeScreenShot();	

			}

			if(responseFromServer.contains("farmer is created successfully!!"))
			{

				test.log(LogStatus.PASS, "Farmer Profile Name i.e. "+ FirstName+ "  "+ LastName +" has been created Successfully");
			}
			else
			{
				reportFailure("Something Went Wrong ...Please Check the screenshot and Logs");
				
			}

			FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
			PageFactory.initElements(driver, farmerSearchPage);
			return farmerSearchPage;

		}

	}

	//	Overloaded Function to Create Farmer Profile with Mandatory Fields only

	public Object createNewFarmerProfile(String PhoneNumber,String FirstName,String MiddleName,String LastName,String Address,String Village)

	{

		test.log(LogStatus.INFO, "Entering Phone Number of the Farmer i.e.: "+ PhoneNumber);
		farmerSearchInputBox.sendKeys(PhoneNumber);
		wait.until(ExpectedConditions.elementToBeClickable(farmerSearchButton));
		farmerSearchButton.click();
		waitForPageLoad();

		//Verifying the Farmer Logout Button is clickable or not.
		boolean farmerLogoutButtonClickable=isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);

		if(farmerLogoutButtonClickable)
		{
			FarmerProfilePage farmerProfilePage = new FarmerProfilePage(driver,test);
			PageFactory.initElements(driver, farmerProfilePage);
			return farmerProfilePage;

		}
		else
		{

			test.log(LogStatus.INFO, "Opening the Farmer Profile Form....");
			driver.switchTo().activeElement();
			firstNameInputBox.sendKeys(FirstName);
			LastNameInputBox.sendKeys(LastName);
			villageInputBox.sendKeys(Village);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			villageInputBox.sendKeys(Keys.ENTER);
			villageInputBox.sendKeys(Keys.DOWN);
			selectOptionWithIndex(1);	
			addressInputBox.sendKeys(Address);
			wait.until(ExpectedConditions.elementToBeClickable(submitFarmerDetailsButton));
			submitFarmerDetailsButton.click();

			boolean isAnyServerResponseInToaster=isElementPresent(CRMConstants.TOASTER_MESSAGE);

			if (isAnyServerResponseInToaster)
			{	
				responseFromServer=toasterMessage.getText();
				takeScreenShot();	
			}

			if(responseFromServer.contains("farmer is created successfully!!"))
			{
				test.log(LogStatus.PASS, "Farmer Profile Name i.e. "+ FirstName+ "  "+ LastName +" has been created Successfully");
			}
			else
			{
				test.log(LogStatus.FAIL, "Something Went Wrong ...Please Check the screenshot and Logs");
			}

			FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
			PageFactory.initElements(driver, farmerSearchPage);
			return farmerSearchPage;


		}
	}
*/	
	public boolean removePhoneNumber(String newPhoneNumber)
	{
		int phoneNumberCount=driver.findElements(By.xpath(CRMConstants.PHONENUMBER_COUNT_ON_PROFILE)).size();
		for(int i=1;i<=phoneNumberCount;i++)
		{
			String getUpdatedPhoneNumber= driver.findElement(By.xpath(CRMConstants.PARTIAL_PHONE_NUBMER1+i+CRMConstants.PARTIAL_PHONE_NUBMER2)).getText().trim();
			if(getUpdatedPhoneNumber.equals(newPhoneNumber))
			{	
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.REMOVE_PHONE_NUMBER_PART1+i+CRMConstants.REMOVE_PHONE_NUMBER_PART2)));
				driver.findElement(By.xpath(CRMConstants.REMOVE_PHONE_NUMBER_PART1+i+CRMConstants.REMOVE_PHONE_NUMBER_PART2)).click();
				return true;
			}	

		}
		return false;
	}
	
	public boolean isUpdatedPhoneNumberVisible(int availablePhoneNumbersCount,String newPhoneNumber)
	{
		
		for(int i=1;i<=availablePhoneNumbersCount;i++)
		{
			String getUpdatedPhoneNumber= driver.findElement(By.xpath(CRMConstants.PARTIAL_PHONE_NUBMER1+i+CRMConstants.PARTIAL_PHONE_NUBMER2)).getText();
			if(getUpdatedPhoneNumber.equals(newPhoneNumber))
			{	
				return true;
			}	

		}
		
		return false;
	}
	

	public Object editExistingFarmerProfile(String phoneNumber, String NewPhoneNumber, String FirstName, String MiddleName, String LastName, String NewVillage, String NewAddress)
	{	

		test.log(LogStatus.INFO, "Verifying the Farmer Name and Phone Number before the updating it...");
				
		String FarmerName=driver.findElement(By.xpath(CRMConstants.FARMER_PROFILE_TITLE)).getText();

		String PhoneNumber=driver.findElement(By.xpath(CRMConstants.FARMER_PHONE_NUMBER)).getText();

		//String existingVillage=driver.findElement(By.xpath("//span[@class='ng-binding ng-scope']/span")).getText();
		String existingAddress=driver.findElement(By.xpath(CRMConstants.FARMER_ADDRESS)).getText();
		
		

		boolean isEditProfileLinkClickable= isElementPresent(CRMConstants.EDIT_PROFILE_LINK);

		if(isEditProfileLinkClickable)
		{
			driver.findElement(By.xpath(CRMConstants.EDIT_PROFILE_LINK)).click();
			// DO NOT DELETE "html/body/div[1]/ui-view/div/div[2]/ui-view[2]/div/div[2]/a" 
			waitForPageLoad();
			driver.switchTo().activeElement();

			if(PhoneNumber.contentEquals(phoneNumber)&& FarmerName.contentEquals(FirstName+" "+MiddleName+" "+LastName))
			{	
				test.log(LogStatus.PASS, " Farmer Profile Verified....");
				test.log(LogStatus.INFO, "Updating the Farmer Details");
				test.log(LogStatus.INFO, "Adding a new Smartphone Number");
				
				phoneNumberInputBox.sendKeys(NewPhoneNumber);
				
				wait.until(ExpectedConditions.elementToBeClickable(smartphoneAvailableCheckBox));
				smartphoneAvailableCheckBox.click();
				wait.until(ExpectedConditions.elementToBeClickable(addNumberButton));
				addNumberButton.click();
				int liCountForPhoneNumber = driver.findElements(By.xpath("//ul[@class='horizontal-list']/li")).size();

				
				
				
				
				System.out.println(liCountForPhoneNumber);
				for(int i=1;i<=liCountForPhoneNumber;i++)
				{
					String getUpdatedPhoneNumber= driver.findElement(By.xpath(CRMConstants.PARTIAL_PHONE_NUBMER1+i+CRMConstants.PARTIAL_PHONE_NUBMER2)).getText();
					if(getUpdatedPhoneNumber.equals(NewPhoneNumber))
					{	
						test.log(LogStatus.INFO, "Phone Number :"+ getUpdatedPhoneNumber +" has been added on Farmer Profile Modal Dialog");
						test.log(LogStatus.INFO, "Now verifying the alternate phone number will get added on click of submit button ");
						break;
					}
					

				}

				
				


				if(!( existingAddress.equals(NewAddress))) //existingVillage.equals(NewVillage) && 
				{	

					villageInputBox.clear();
					villageInputBox.sendKeys(NewVillage);
					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					villageInputBox.sendKeys(Keys.DOWN);
					villageInputBox.sendKeys(Keys.ENTER);	

					addressInputBox.clear();
					addressInputBox.sendKeys(NewAddress);
					wait.until(ExpectedConditions.elementToBeClickable(submitFarmerDetailsButton));
					submitFarmerDetailsButton.click();
					
					waitForPageLoad();
					

					

				}
				else
				{
					
					reportFailure("New Village and New Address is same as existing Village and Address. Stopping test....quiting driver...!!"); 
					
		
				}

				FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
				PageFactory.initElements(driver, farmerProfilePage);
				return farmerProfilePage;


			}
			else
			{
				
				reportFailure("Phone Number and Farmer Name mismatch with Expected Farmer Profile- Profile Should not be edited .Stopping test....quiting driver...!!");
				FarmerSearchPage farmerSearchPage=new FarmerSearchPage(driver, test);
				PageFactory.initElements(driver, farmerSearchPage);
				driver.quit();
				return farmerSearchPage;
				//	return "";

			}
		}
		else	
		{
			
			reportFailure("Edit Profile link is not clickable..Test Failed. quiting driver...!!");
			FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
			PageFactory.initElements(driver, farmerProfilePage);
			driver.quit();
			return farmerProfilePage;

			//	return "";
		}




	}


}
