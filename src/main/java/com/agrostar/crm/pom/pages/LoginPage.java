package com.agrostar.crm.pom.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPage extends BasePage{
	
	
	String messageFromServer;
	
	@FindBy(xpath=CRMConstants.LOGIN_USERNAME)
	public WebElement username;
	
	@FindBy(xpath=CRMConstants.LOGIN_PASSWORD)
	public WebElement password;
	
	@FindBy(xpath=CRMConstants.LOGIN_BUTTON)
	public WebElement loginButton;
	
	@FindBy(xpath=CRMConstants.TOASTER_MESSAGE)
	public WebElement toasterMessage;
	
	public LoginPage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}
	
	
	
	
	
	public Object doLogin(String usName,String pWord){
		
		test.log(LogStatus.INFO, " Logging in with credentials -"+usName+"/"+pWord);
		wait.until(ExpectedConditions.visibilityOf(username));
		username.sendKeys(usName);
		wait.until(ExpectedConditions.visibilityOf(password));
		password.sendKeys(pWord);
		//password.sendKeys(Keys.ENTER);
		
		wait.until(ExpectedConditions.elementToBeClickable(loginButton));
		loginButton.click();
		waitForPageLoad();
		takeScreenShot();
		// logic - validate
        boolean isAnyServerResponseInToaster=isElementPresent(CRMConstants.TOASTER_MESSAGE);
		if (isAnyServerResponseInToaster)
		{	
			messageFromServer=toasterMessage.getText();
			//takeScreenShot();
			
		}
	   // waitForPageLoad();
		boolean loginSuccess=isElementPresent(CRMConstants.REPORTSPAGE_LINK);
		
		if(loginSuccess)
		{
		
			test.log(LogStatus.INFO, "Login Success");
			HomePage homePage = new HomePage(driver,test);
			PageFactory.initElements(driver, homePage);
			homePage.takeScreenShot();
			return homePage;
		}
		else
		{	
			
			test.log(LogStatus.FAIL, "CRM login failed with reason : "+ messageFromServer);
			LoginPage loginPage = new LoginPage(driver,test);
			PageFactory.initElements(driver, loginPage);
			loginPage.takeScreenShot();
			return loginPage;
		}

		
	}
	

	
	

}
