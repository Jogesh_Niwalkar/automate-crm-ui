/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class OrderDetailsPage extends BasePage {

	@FindBy(xpath=CRMConstants.ORDER_DETAILS_PAGE_ORDER_ID)
	public WebElement OrderId;

	@FindBy(xpath=CRMConstants.ORDER_DETAILS_ORDER_STATUS)
	public WebElement orderStatus;

	@FindBy(xpath=CRMConstants.CREATE_NEW_ORDER_TICKET_BUTTON)
	public WebElement createNewOrderTicket_Button;

	@FindBy(xpath=CRMConstants.SELECT_ORDER_ISSUE_DROPDOWN)
	public WebElement selectOrderIssueDropdown;

	@FindBy(xpath=CRMConstants.SELECT_REASON_DROPDOWN)
	public WebElement selectReasonDropdown;

	@FindBy(xpath=CRMConstants.ORDER_ISSUE_SUBMIT_BUTTON)
	public WebElement orderIssueSubmitButton;

	@FindBy(xpath=CRMConstants.ORDER_ISSUE_REMARKS_INPUTBOX)
	public WebElement orderIssueRemarksInputbox;

	@FindBy(xpath=CRMConstants.ZERO_ISSUE_SELECTED_TEXT)
	public WebElement zeroIssueSelectedText;

	@FindBy(xpath=CRMConstants.GENERATED_TICKET_ID)
	public WebElement generatedTicketId;

	@FindBy(xpath=CRMConstants.TICKET_MODAL_HEADER)
	public WebElement ticketModalHeader;

	@FindBy(xpath=CRMConstants.SELECT_ITEM_SELECTED_DROPDOWN)
	public WebElement selectItemSelectedDropdown;
	
	@FindBy(xpath=CRMConstants.ORDER_DETAILS_ORDER_STATUS)
	public WebElement orderStatusFromOrderDetailsPage;
	
	@FindBy(xpath=CRMConstants.ORDER_DETAILS_PAGE_ORDER_ID)
	public WebElement getOrderIdFromOrderDetails;

	@FindBy(xpath=CRMConstants.CANCEL_ORDER_BUTTON)
	public WebElement cancelOrderButton;

	@FindBy(xpath=CRMConstants.EDIT_ORDER_BUTTON)
	public WebElement editOrderButton;

	@FindBy(xpath=CRMConstants.FARMER_PROFILE_BUTTON)
	public WebElement farmerProfileHomePageLink;
	
	@FindBy(xpath=CRMConstants.ORDER_DETAILS_UNICOMMERSE_ID)
	public WebElement getUniCommerseIdFromOrderDetails;
	public OrderDetailsPage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}


	public String getOrderStatus()
	{	
		String getOrderStatus =orderStatus.getText();
		return getOrderStatus;
	}


	public String getOrderId()
	{
		String getOrderId=OrderId.getText();
		String trimOrderId=getOrderId.substring(10);
		takeScreenShot();
		return trimOrderId;
	}

	public void createNewOrderTicket_Button()
	{	
		wait.until(ExpectedConditions.elementToBeClickable(createNewOrderTicket_Button));
		createNewOrderTicket_Button.click();
		waitForPageLoad();

	}
	public void selectOrderIssueDropdownBox_Click()
	{	
		selectOrderIssueDropdown.click();

	}

	public void selectOrderIssueFromDropdown(String orderIssue)
	{	

		wait.until(ExpectedConditions.elementToBeClickable(selectOrderIssueDropdown));
		new Select(selectOrderIssueDropdown).selectByVisibleText(orderIssue);


	}

	public void selectReasonDropdownBox_Click()
	{	
		selectReasonDropdown.click();

	}
	public void selectReasonFromDropdown(String orderIssueReason)
	{	
		int noOfOrderIssueCount=driver.findElements(By.xpath(CRMConstants.NO_OF_ORDER_ISSUE_REASON)).size();

		for(int i=1;i<=noOfOrderIssueCount;i++)
		{
			String getOrderIssueReason=driver.findElement(By.xpath(CRMConstants.ORDER_ISSUE_REASON_PART1+i+CRMConstants.ORDER_ISSUE_REASON_PART2)).getText();
			if(getOrderIssueReason.equalsIgnoreCase(orderIssueReason))
			{
				driver.findElement(By.xpath(CRMConstants.ORDER_ISSUE_REASON_CHECKBOX_PART1+i+CRMConstants.ORDER_ISSUE_REASON_CHECKBOX_PART2)).click();
				break;
			}
		}


	}

	public void orderIssueSubmit_Button()
	{	
		wait.until(ExpectedConditions.elementToBeClickable(orderIssueSubmitButton));
		orderIssueSubmitButton.click();

	}

	public void orderIssueRemarks_Inputbox(String remarks)
	{	
		orderIssueRemarksInputbox.sendKeys(remarks);

	}

	public void zeroIssueSelectedText()
	{	
		zeroIssueSelectedText.click();

	}

	public void modelHeader()
	{	
		ticketModalHeader.click();
		//zeroIssueSelectedText.click();

	}

	public String getGeneratedTicketIdForOrder()
	{	
		String ticketId=generatedTicketId.getText();
		return ticketId;


	}
	public String getGeneratedTicketIdForEachItemInOrder(String expectedProductName)
	{	
		int countOfItemInOrder=driver.findElements(By.xpath(CRMConstants.PRODUCT_COUNT)).size();
		for(int i=1;i<=countOfItemInOrder;i++)
		{
			String productName=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_PART1+i+CRMConstants.PRODUCT_NAME_PART2)).getText();
			if(productName.equalsIgnoreCase(expectedProductName))
			{
				String ticketId=driver.findElement(By.xpath(CRMConstants.TICKET_ID_PART1+i+CRMConstants.TICKET_ID_PART2)).getText();
				return ticketId;
			}
			
		}
		
		
	return "";

	}
	

	public boolean isExpectedProductAvailableOnPage(String expectedProductName)
	{
		int productCount=driver.findElements(By.xpath(CRMConstants.PRODUCT_COUNT)).size();
	
		for(int i=1;i<=productCount;i++)
		{
			String productName=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_PART1+i+CRMConstants.PRODUCT_NAME_PART2)).getText();
			if(productName.equalsIgnoreCase(expectedProductName))
			{
				return true;
			}
		}
		
		return false;
	}

	
	public void newTicketButton_Click(String expectedProductName)
	{
		int productCount=driver.findElements(By.xpath(CRMConstants.PRODUCT_COUNT)).size();
		
		for(int i=1;i<=productCount;i++)
		{
			String productName=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_PART1+i+CRMConstants.PRODUCT_NAME_PART2)).getText();
			if(productName.equalsIgnoreCase(expectedProductName))
			{	
				test.log(LogStatus.INFO, "Clicking on New Ticket Button");
				String xpathForNewTicketButton=CRMConstants.CREATE_NEW_TICKET_ON_ITEM_LEVEL_PART1+i+CRMConstants.CREATE_NEW_TICKET_ON_ITEM_LEVEL_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathForNewTicketButton)));
				driver.findElement(By.xpath(xpathForNewTicketButton)).click();
				break;
			}
		}
	
	}	

	
	public void selectItemSelectedDropdownBox_Click()
	{
		selectItemSelectedDropdown.click();

	}
	
	public void selectItemFromDropdown(String ProductName)
	{
		wait.until(ExpectedConditions.elementToBeClickable(selectItemSelectedDropdown));
		new Select(selectItemSelectedDropdown).selectByVisibleText(ProductName);
	

	}
	
	public String getOrderStatusFromOrderDetailsPage()
	{	
		String orderStatus =orderStatusFromOrderDetailsPage.getText();
		return orderStatus;
	}
	

	public String getOrderIdFromOrderDetailsPage()
	{
		String orderId=getOrderIdFromOrderDetails.getText();
		takeScreenShot();
		return orderId;
	}
	public String getUnicommerseIdFromOrderDetailsPage()
	{
		String UniCommerseId=getUniCommerseIdFromOrderDetails.getText();
		takeScreenShot();
		return UniCommerseId;
	}
	

	
	public void cancelOrder_button()
	{	
		wait.until(ExpectedConditions.elementToBeClickable(cancelOrderButton));
		cancelOrderButton.click();
	}
	
	public void farmerProfile_Button()
	{	
		
		wait.until(ExpectedConditions.elementToBeClickable(farmerProfileHomePageLink));
		farmerProfileHomePageLink.click();
		
	}
	
	public void editOrder_button()
	{	
		wait.until(ExpectedConditions.elementToBeClickable(editOrderButton));
		editOrderButton.click();
	}
	//button[contains(text(),'Edit Order')]
	
	
}
