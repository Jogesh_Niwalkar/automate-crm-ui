/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class AdminPage extends BasePage {

	// Offer Tab = WebElements
	int offerStartDate;
	int offerEndDate_UserValue;
	int	offerEndDate;
	public String getServerResponseInToasterMessage;
	public String getServerResponseInToasterTitle;
	int couponStartDate;
	int couponEndDate;
	int CouponEndDate_UserValue;

	@FindBy(xpath=CRMConstants.TOASTER_MESSAGE)
	public WebElement toasterMessage;
	@FindBy(xpath=CRMConstants.TOASTER_TITLE)
	public WebElement toasterTitle;	


	@FindBy(xpath=CRMConstants.OFFERS_TAB)
	public WebElement offerTab;

	@FindBy(xpath=CRMConstants.OFFER_NEW_BUTTON)
	public WebElement offerNewButton;

	@FindBy(xpath=CRMConstants.OFFERS_TITLE)
	public WebElement offerTitle;

	@FindBy(xpath=CRMConstants.OFFER_NAME_INPUTBOX)
	public WebElement offerName;

	@FindBy(xpath=CRMConstants.OFFER_DESCRIPTION_INPUTBOX)
	public WebElement offerDescription;

	@FindBy(xpath=CRMConstants.OFFER_VALID_FROM)
	public WebElement offerValidFrom_InputBox;

	@FindBy(xpath=CRMConstants.OFFER_VALID_TO)
	public WebElement offerValidTo_InputBox;

	@FindBy(xpath=CRMConstants.MAX_NUMBER_OF_USAGES)
	public WebElement maxNoOfUsages;

	@FindBy(xpath=CRMConstants.SELECT_SOURCES_FOR_OFFERLINK)
	public WebElement selectSourcesLink;

	@FindBy(xpath=CRMConstants.SELECT_PRODCUCTS_LINK)
	public WebElement selectProductsLink;

	@FindBy(xpath=CRMConstants.CREATE_OFFER_BUTTON)
	public WebElement createOfferButton;

	@FindBy(xpath=CRMConstants.CANCEL_OFFER_BUTTON)
	public WebElement CancelOrderButton;

	@FindBy(xpath=CRMConstants.PERCENT_DISCOUNT_VALUE)
	public WebElement PercentDiscountValue;

	@FindBy(xpath=CRMConstants.PERCENT_DISCOUNT_DESCRIPTION)
	public WebElement PercentDiscountDescription;

	@FindBy(xpath=CRMConstants.OFFER_VALIDITY_START_DATE)
	public WebElement offerValidityStartDate;

	@FindBy(xpath=CRMConstants.OFFER_VALIDITY_END_DATE)
	public WebElement offerValidityEndDate;

	@FindBy(xpath=CRMConstants.OFFER_TYPE_DROPDOWN)
	public WebElement offerTypeDropdown;

	@FindBy(xpath=CRMConstants.ALL_SOURCES_AGROSTAR)
	public WebElement sourcesAllAgrostar;

	@FindBy(xpath=CRMConstants.SELECT_SOURCES_SUBMIT_BUTTON)
	public WebElement selectSourcesSubmitButton;

	@FindBy(xpath=CRMConstants.SEARCH_PRODUCT_BY_NAME_SKU_INPUTBOX)
	public WebElement searchProductByNameSKUInputBox;

	@FindBy(xpath=CRMConstants.SELECT_PRODUCT_SUBMIT_BUTTON)
	public WebElement selectProductSubmitButton;
	
	@FindBy(xpath=CRMConstants.DISABLE_OFFER_COMMENT_INPUTBOX)
	public WebElement disableOfferCommentInputBox;
	
	@FindBy(xpath=CRMConstants.DISABLE_OFFER_COMMENT_SUBMIT_BUTTON)
	public WebElement disbleOfferCommentBoxSubmitButton;

	// Coupons Tab = WebElements

	@FindBy(xpath=CRMConstants.COUPON_TAB)
	public WebElement couponTab;

	@FindBy(xpath=CRMConstants.COUPON_NEW_BUTTON)
	public WebElement couponNewButton;

	@FindBy(xpath=CRMConstants.COUPON_TITLE)
	public WebElement couponPageTitle;

	@FindBy(xpath=CRMConstants.COUPON_CODE_INPUTBOX)
	public WebElement couponCodeInputbox;

	@FindBy(xpath=CRMConstants.COUPON_DESCRIPTION_INPUTBOX)
	public WebElement couponCodeDescriptionbox;

	@FindBy(xpath=CRMConstants.COUPON_VALID_FROM)
	public WebElement couponValidFromInputBox;

	@FindBy(xpath=CRMConstants.COUPON_VALID_TO)
	public WebElement couponValidToInputBox;

	@FindBy(xpath=CRMConstants.MAX_NUMBER_OF_ORDERS_INPUTBOX)
	public WebElement maxNoOfOrders;

	@FindBy(xpath=CRMConstants.SELECT_SOURCES_FOR_COUPON_LINK)
	public WebElement couponSelectSourcesLink;

	@FindBy(xpath=CRMConstants.CREATE_COUPON_BUTTON)
	public WebElement createCouponButton;

	@FindBy(xpath=CRMConstants.CANCEL_COUPON_BUTTON)
	public WebElement cancelCouponButton;

	@FindBy(xpath=CRMConstants.IS_FREE_FLOATING_COUPON_CHECKBOX)
	public WebElement freeFloatingCouponCheckBox;

	@FindBy(xpath=CRMConstants.ORDER_VALUE_INPUTBOX)
	public WebElement orderValueInputBox;

	@FindBy(xpath=CRMConstants.COUPON_VALIDITY_START_DATE)
	public WebElement couponValidityStartDate;

	@FindBy(xpath=CRMConstants.COUPON_TYPE_DROPDOWN)
	public WebElement couponTypeDropdown;

	@FindBy(xpath=CRMConstants.IS_FREE_FLOATING_COUPON_CHECKBOX)
	public WebElement freeFloatingCheckBox;

	@FindBy(xpath=CRMConstants.PERCENT_DISCOUNT_VALUE)
	public WebElement PercentDiscount;

	@FindBy(xpath=CRMConstants.SELECT_SOURCES_FOR_COUPON_LINK)
	public WebElement selectSourcesCouponLink;

	@FindBy(xpath=CRMConstants.DISABLE_COUPON_COMMENT_INPUTBOX)
	public WebElement disableCouponCommentInputBox;
	
	@FindBy(xpath=CRMConstants.DISABLE_COUPON_COMMENT_SUBMIT_BUTTON)
	public WebElement disbleCouponCommentBoxSubmitButton;
	
	
	

	public AdminPage(WebDriver driver,ExtentTest test){

		super(driver,test);

	}






	public boolean isOfferSideMenuSelectedByDefault()
	{
		test.log(LogStatus.INFO,"Verifying the OFFER SideMenu is selected by default ");
		String ClassValueOfActiveTab=offerTab.getAttribute("class");
		if(ClassValueOfActiveTab.equals("list-group-item active"))
		{	
			takeScreenShot();
			return true;
		}
		else
		{
			takeScreenShot();
			return false;

		}

	}

	public void createNewOffer_Button()
	{
		test.log(LogStatus.INFO,"Creating a New Offer by clicking 'New' Button..");
		wait.until(ExpectedConditions.elementToBeClickable(offerNewButton));
		offerNewButton.click();
	}

	
	public String getOfferPage_Title()
	{
		String offerTitleText=offerTitle.getText();
		takeScreenShot();
		return offerTitleText;
	}

	
	public void offerName_InputBox(String OfferName)
	{
		offerName.sendKeys(OfferName);
	}
	
	
	public void offerDescription_InputBox(String OfferDescription)
	{
		offerDescription.sendKeys(OfferDescription);
	}

	public void offerValidity_StartDate(String offerName)
	{
		test.log(LogStatus.INFO, "Creating an Offer:"+ offerName +" with 1 Day validity from current date i.e. " + DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
		
		wait.until(ExpectedConditions.elementToBeClickable(offerValidFrom_InputBox));
		offerValidFrom_InputBox.click();
		test.log(LogStatus.INFO, "Selecting todays Date from calender");
		wait.until(ExpectedConditions.elementToBeClickable(offerValidFrom_InputBox));
		offerValidityStartDate.click();
		offerStartDate=Integer.parseInt(offerValidityStartDate.getAttribute("text"));
	}
	

	
	public void offerValidity_EndDate()
	{
		wait.until(ExpectedConditions.elementToBeClickable(offerValidTo_InputBox));
		offerValidTo_InputBox.click();
		test.log(LogStatus.INFO, "Selecting Offer End Date i.e "+ (offerStartDate));
		offerEndDate_UserValue=offerStartDate;
		java.util.List<WebElement> CalenderDatelist=driver.findElements(By.xpath(CRMConstants.DATE_PICKER_DATE_LIST));

		for(int i=1;i<=CalenderDatelist.size();i++)
		{
			String getEndDate=driver.findElement(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).getAttribute("text");
			offerEndDate=Integer.parseInt(getEndDate);
			if(offerEndDate_UserValue==offerEndDate)
			{
				int var_ele_size =driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).size();
				driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).get(var_ele_size-1).click();
				break;
			}	
		}
	}

	public void offerAppliesToMaxOrders(String MaxNoOfUsages)
	{

		wait.until(ExpectedConditions.visibilityOf(maxNoOfUsages));
		maxNoOfUsages.sendKeys(MaxNoOfUsages);
	}

	public void offerType_Dropdown(String Entitlements_OfferType)
	{

		wait.until(ExpectedConditions.visibilityOf(offerTypeDropdown));
		new Select(offerTypeDropdown).selectByVisibleText(Entitlements_OfferType);
	}

	public void offerPercentDiscountValue_InputBox(String percentDiscount)
	{
		wait.until(ExpectedConditions.visibilityOf(PercentDiscountValue));
		PercentDiscountValue.sendKeys(percentDiscount);
	}

	public void offerPercentDiscountDescription_InputBox(String percentDiscDescription)
	{
		wait.until(ExpectedConditions.visibilityOf(PercentDiscountDescription));
		PercentDiscountDescription.sendKeys(percentDiscDescription);
	}

	public void selectSourcesForOffer_Link()
	{
		wait.until(ExpectedConditions.elementToBeClickable(selectSourcesLink));
		selectSourcesLink.click();
		
	}

	public void selectSourcesForOffer_CheckBox() throws InterruptedException
	{
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean isSelected=sourcesAllAgrostar.isSelected();
		if(!isSelected)
		{
			sourcesAllAgrostar.click();
		}

	}

	public void selectSourcesForOffer_SubmitButton()
	{
		
		wait.until(ExpectedConditions.elementToBeClickable(selectSourcesSubmitButton));
		selectSourcesSubmitButton.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.SelectedSourcesOnCreateOfferPage)));
	}

	public void SelectProductsForOffer_Link()
	{		
		wait.until(ExpectedConditions.elementToBeClickable(selectProductsLink));
		selectProductsLink.click();
	}

	public void searchProductsByNameOrSKU_InputBox(String OfferAppliedProducts )
	{		
		wait.until(ExpectedConditions.visibilityOf(searchProductByNameSKUInputBox));
		searchProductByNameSKUInputBox.sendKeys(OfferAppliedProducts);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.SelectedSourcesTableOnCreateOfferPage)));
	}

	public void selectProductFromProductListToApplyOffer(String OfferAppliedProducts ,String ProductSKUCode)
	{
		int noOfRows= driver.findElements(By.xpath(CRMConstants.NumberOfRowsOnSelectProductModalDialog)).size();
		try 
		{
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int j =1;j<=noOfRows;j++)
		{
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1+j+CRMConstants.PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2)));
			String AvailableProductCottonSeedNameOnDOM=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1+j+CRMConstants.PRODUCT_NAME_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2)).getText().trim();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1+j+CRMConstants.PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2)));
			String AvailableProductCottonSeedSKUCodeOnDOM=driver.findElement(By.xpath(CRMConstants.PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1+j+CRMConstants.PRODUCT_SKUCODE_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2)).getText().trim();


			if(AvailableProductCottonSeedNameOnDOM.equals(OfferAppliedProducts) && AvailableProductCottonSeedSKUCodeOnDOM.equals(ProductSKUCode))
			{

				driver.findElement(By.xpath(CRMConstants.CHECKBOX_SELECTION_ON_SELECT_PRODUCT_MODAL_DIALOG_PART1+j+CRMConstants.CHECKBOX_SELECTION_ON_SELECT_PRODUCT_MODAL_DIALOG_PART2)).click();
				try 
				{
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;

			}

		}

	}

	public void selectProductsForOffer_SubmitButton()
	{
		wait.until(ExpectedConditions.elementToBeClickable(selectProductSubmitButton));
		selectProductSubmitButton.click();
	}

	public void createOffer_Button()
	{
		wait.until(ExpectedConditions.elementToBeClickable(createOfferButton));
		createOfferButton.click();

	}
	
	public boolean disableOffer(String offerName, String OfferDescription )
	{
		
		int noOfRows= driver.findElements(By.xpath(CRMConstants.OFFERS_COUNT_ON_OFFERS_GRID)).size();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int j =1;j<=noOfRows;j++)
		{
			String OfferNameOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_NAME_ON_GRID_PART1+j+CRMConstants.OFFER_NAME_ON_GRID_PART2)).getText();

			String OfferDescriptionOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_DESCRIPTION_ON_GRID_PART1+j+CRMConstants.OFFER_DESCRIPTION_ON_GRID_PART2)).getText();
			String OfferStartDateOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_START_DATE_ON_GRID_PART1+j+CRMConstants.OFFER_START_DATE_ON_GRID_PART2)).getText();
			String OfferEndDateOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_END_DATE_ON_GRID_PART1+j+CRMConstants.OFFER_END_DATE_ON_GRID_PART2)).getText();
			String extractedOfferEndDateFromPage=OfferEndDateOnPage.substring(0, 2);
			String extractedOfferstartDateFromPage=OfferStartDateOnPage.substring(0, 2);

			int OfferEndDateFromPage=Integer.parseInt(extractedOfferEndDateFromPage);
			int OfferStartDateFromPage=Integer.parseInt(extractedOfferstartDateFromPage);


			if(OfferNameOnPage.equals(offerName) && OfferDescriptionOnPage.equals(OfferDescription) && OfferStartDateFromPage==offerStartDate && OfferEndDateFromPage==offerEndDate_UserValue)
			{
				driver.findElement(By.xpath(CRMConstants.DISABLE_OFFER_BUTTON_PART1+j+CRMConstants.DISABLE_OFFER_BUTTON_PART2)).click();
				driver.switchTo().activeElement();
				disableOfferCommentInputBox.sendKeys("Test Offers");
				wait.until(ExpectedConditions.elementToBeClickable(disbleOfferCommentBoxSubmitButton));
				disbleOfferCommentBoxSubmitButton.click();
				waitForPageLoad();
				String getServerResponse=getServerResponseInToasterBothTitleNMessage();
				
				if(getServerResponse.equals("Offer Disabled! "+offerName))
				{
					
					takeScreenShot();
					return true;
				}
			
			}
			

		}

		takeScreenShot();
		return false;
	}

	public boolean disableCoupon(String CouponName,String CouponDescription)
	{
		int noOfRows= driver.findElements(By.xpath(CRMConstants.COUPON_COUNT_ON_COUPONS_GRID)).size();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i =1;i<=noOfRows;i++)
		{
			String CouponNameOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_NAME_ON_GRID_PART1+i+CRMConstants.COUPON_NAME_ON_GRID_PART2)).getText();

			String CouponDescriptionOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_DESCRIPTION_ON_GRID_PART1+i+CRMConstants.COUPON_DESCRIPTION_ON_GRID_PART2)).getText();
			String CouponStartDateOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_START_DATE_ON_GRID_PART1+i+CRMConstants.COUPON_START_DATE_ON_GRID_PART2)).getText();
			String CouponEndDateOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_END_DATE_ON_GRID_PART1+i+CRMConstants.COUPON_END_DATE_ON_GRID_PART2)).getText();
			String extractedCouponEndDateFromPage=CouponEndDateOnPage.substring(0, 2);
			String extractedCouponStartDateFromPage=CouponStartDateOnPage.substring(0, 2);

			int CouponEndDateFromPage=Integer.parseInt(extractedCouponEndDateFromPage);
			int CouponStartDateFromPage=Integer.parseInt(extractedCouponStartDateFromPage);


			if(CouponNameOnPage.equals(CouponName) && CouponDescriptionOnPage.equals(CouponDescription) && CouponStartDateFromPage==couponStartDate && CouponEndDateFromPage==CouponEndDate_UserValue)
			{
				driver.findElement(By.xpath(CRMConstants.DISABLE_COUPON_BUTTON_PART1+i+CRMConstants.DISABLE_COUPON_BUTTON_PART2)).click();
				driver.switchTo().activeElement();
				disableCouponCommentInputBox.sendKeys("Test Coupon");
				wait.until(ExpectedConditions.elementToBeClickable(disbleCouponCommentBoxSubmitButton));
				disbleCouponCommentBoxSubmitButton.click();
				waitForPageLoad();
				String getServerResponse=getServerResponseInToasterBothTitleNMessage();
				
				if(getServerResponse.equals("Coupon Disabled! "+CouponName))
				{
					
					takeScreenShot();
					return true;
				}
			}
			


		}
		takeScreenShot();
		return false;
		
	}
	
	public boolean isOfferAvailableOnGridAfterDisabling(String OfferName)
	{
			int noOfRows= driver.findElements(By.xpath(CRMConstants.OFFERS_COUNT_ON_OFFERS_GRID)).size();

			for (int i =1;i<=noOfRows;i++)
			{
				String OfferNameOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_NAME_ON_GRID_PART1+i+CRMConstants.OFFER_NAME_ON_GRID_PART2)).getText();
				
				if(OfferNameOnPage.equals(OfferName))
				{
					takeScreenShot();
					return true;
				}
			}
		takeScreenShot();
			return false;		
	}
	
	public boolean isCouponAvailableOnGridAfterDisabling(String CouponName)
	{
			int noOfRows= driver.findElements(By.xpath(CRMConstants.COUPON_COUNT_ON_COUPONS_GRID)).size();

			for (int i =1;i<=noOfRows;i++)
			{
				String CouponNameOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_NAME_ON_GRID_PART1+i+CRMConstants.COUPON_NAME_ON_GRID_PART2)).getText();
				
				if(CouponNameOnPage.equals(CouponName))
				{
					takeScreenShot();
					return true;
				}
			}
			takeScreenShot();
			return false;		
	}
		
	
	public boolean IsCouponAvailableOnGrid(String CouponName,String CouponDescription)
	{

		int noOfRows= driver.findElements(By.xpath(CRMConstants.COUPON_COUNT_ON_COUPONS_GRID)).size();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i =1;i<=noOfRows;i++)
		{
			String CouponNameOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_NAME_ON_GRID_PART1+i+CRMConstants.COUPON_NAME_ON_GRID_PART2)).getText().trim();

			String CouponDescriptionOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_DESCRIPTION_ON_GRID_PART1+i+CRMConstants.COUPON_DESCRIPTION_ON_GRID_PART2)).getText().trim();
			String CouponStartDateOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_START_DATE_ON_GRID_PART1+i+CRMConstants.COUPON_START_DATE_ON_GRID_PART2)).getText().trim();
			String CouponEndDateOnPage=driver.findElement(By.xpath(CRMConstants.COUPON_END_DATE_ON_GRID_PART1+i+CRMConstants.COUPON_END_DATE_ON_GRID_PART2)).getText().trim();
			String extractedCouponEndDateFromPage=CouponEndDateOnPage.substring(0, 2).trim();
			String extractedCouponStartDateFromPage=CouponStartDateOnPage.substring(0, 2).trim();

			int CouponEndDateFromPage=Integer.parseInt(extractedCouponEndDateFromPage);
			int CouponStartDateFromPage=Integer.parseInt(extractedCouponStartDateFromPage);


			if(CouponNameOnPage.equals(CouponName) && CouponDescriptionOnPage.equals(CouponDescription) && CouponStartDateFromPage==couponStartDate && CouponEndDateFromPage==CouponEndDate_UserValue)
			{
				takeScreenShot();
				return true;
			}
			

		}

		takeScreenShot();
		return false;
	}

	public boolean IsOfferAvailableOnGrid(String OfferName,String OfferDescription)
	{

		int noOfRows= driver.findElements(By.xpath(CRMConstants.OFFERS_COUNT_ON_OFFERS_GRID)).size();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int j =1;j<=noOfRows;j++)
		{
			String OfferNameOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_NAME_ON_GRID_PART1+j+CRMConstants.OFFER_NAME_ON_GRID_PART2)).getText().trim();

			String OfferDescriptionOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_DESCRIPTION_ON_GRID_PART1+j+CRMConstants.OFFER_DESCRIPTION_ON_GRID_PART2)).getText().trim();
			String OfferStartDateOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_START_DATE_ON_GRID_PART1+j+CRMConstants.OFFER_START_DATE_ON_GRID_PART2)).getText().trim();
			String OfferEndDateOnPage=driver.findElement(By.xpath(CRMConstants.OFFER_END_DATE_ON_GRID_PART1+j+CRMConstants.OFFER_END_DATE_ON_GRID_PART2)).getText().trim();
			String extractedOfferEndDateFromPage=OfferEndDateOnPage.substring(0, 2).trim();
			String extractedOfferstartDateFromPage=OfferStartDateOnPage.substring(0, 2).trim();

			int OfferEndDateFromPage=Integer.parseInt(extractedOfferEndDateFromPage);
			int OfferStartDateFromPage=Integer.parseInt(extractedOfferstartDateFromPage);


			if(OfferNameOnPage.equals(OfferName) && OfferDescriptionOnPage.equals(OfferDescription) && OfferStartDateFromPage==offerStartDate && OfferEndDateFromPage==offerEndDate_UserValue)
			{
				takeScreenShot();
				return true;
			}
			

		}

		takeScreenShot();
		return false;
	}

	public void couponTab_Button()
	{
		test.log(LogStatus.INFO,"Clicking on the Coupon SideMenu/Tab to create the Coupon ");
		wait.until(ExpectedConditions.elementToBeClickable(couponTab));
		couponTab.click();
	}

	public void createNewCoupon_Button()
	{
		test.log(LogStatus.INFO,"Clicking on New Button to create a New Coupon ");
		wait.until(ExpectedConditions.elementToBeClickable(couponNewButton));
		couponNewButton.click();

	}

	public String getCouponPage_Title()
	{
		String CouponTitleOnPage=couponPageTitle.getText().trim();
		return CouponTitleOnPage;
	}

	public void couponCode_InputBox(String couponName)
	{
		wait.until(ExpectedConditions.visibilityOf(couponCodeInputbox));
		couponCodeInputbox.sendKeys(couponName);
	}

	public void couponCodeDescription_InputBox(String couponCodeDescription)
	{
		wait.until(ExpectedConditions.visibilityOf(couponCodeDescriptionbox));
		couponCodeDescriptionbox.sendKeys(couponCodeDescription);
	}

	public void couponValidity_StartDate(String couponName)
	{

		test.log(LogStatus.INFO, " Creating an Coupon:"+ couponName +" with 1 Day validity from current date i.e. " + DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH));
		wait.until(ExpectedConditions.elementToBeClickable(couponValidFromInputBox));
		couponValidFromInputBox.click();
		test.log(LogStatus.INFO, "Selecting todays Date from calender");
		wait.until(ExpectedConditions.elementToBeClickable(couponValidityStartDate));
		couponValidityStartDate.click();
		couponStartDate=Integer.parseInt(couponValidityStartDate.getAttribute("text"));

	}

	public void couponValidity_EndDate()
	{
		wait.until(ExpectedConditions.elementToBeClickable(couponValidToInputBox));
		couponValidToInputBox.click();
		test.log(LogStatus.INFO, "Selecting Coupon Validity End Date i.e "+ (couponStartDate));
		CouponEndDate_UserValue=couponStartDate;


		java.util.List<WebElement> CalenderDateslist=driver.findElements(By.xpath(CRMConstants.DATE_PICKER_DATE_LIST));

		for(int i=1;i<=CalenderDateslist.size();i++)
		{
			String getEndDate=driver.findElement(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).getAttribute("text");
			couponEndDate=Integer.parseInt(getEndDate);
			if(CouponEndDate_UserValue==couponEndDate)
			{

				int var_ele_size =driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).size();
				driver.findElements(By.xpath(CRMConstants.SELECT_DATE_FROM_DATE_PICKER1+i+CRMConstants.SELECT_DATE_FROM_DATE_PICKER2)).get(var_ele_size-1).click();
				break;
			}	
		}

	}

	public void couponAppliesToMaxOrders(String MaxNoOfUsages)
	{
		wait.until(ExpectedConditions.visibilityOf(maxNoOfOrders));
		maxNoOfOrders.sendKeys(MaxNoOfUsages);

	}

	public void couponType_Dropdown(String Entitlements_CouponType)
	{
		wait.until(ExpectedConditions.visibilityOf(couponTypeDropdown));
		new Select(couponTypeDropdown).selectByVisibleText(Entitlements_CouponType);

	}

	public void PercentDiscountValue_InputBox(String percentDiscount)
	{
		wait.until(ExpectedConditions.visibilityOf(PercentDiscountValue));
		PercentDiscountValue.sendKeys(percentDiscount);
	}

	public void PercentDiscountDescription_InputBox(String percentDiscDescription)
	{
		wait.until(ExpectedConditions.visibilityOf(PercentDiscountDescription));
		PercentDiscountDescription.sendKeys(percentDiscDescription);
	}

	public void selectSourcesForCoupon_Link()
	{
		wait.until(ExpectedConditions.elementToBeClickable(selectSourcesCouponLink));
		selectSourcesCouponLink.click();
	}

	public void selectSources_CheckBox()
	{
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		boolean isSelected=sourcesAllAgrostar.isSelected();
		if(!isSelected)
		{
			wait.until(ExpectedConditions.elementToBeClickable(sourcesAllAgrostar));
			sourcesAllAgrostar.click();
		}

	}

	public void selectSources_SubmitButton()
	{
		wait.until(ExpectedConditions.elementToBeClickable(selectSourcesSubmitButton));
		selectSourcesSubmitButton.click();
	}

	public void createCoupon_Button()
	{
		wait.until(ExpectedConditions.elementToBeClickable(createCouponButton));
		createCouponButton.click();
	}





}



