package com.agrostar.crm.pom.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class LaunchPage extends BasePage{
	
	
	public LaunchPage(WebDriver driver,ExtentTest test){
		super(driver,test);
			
	}
	
	
	public LoginPage gotoLoginPage(){
		// log
		test.log(LogStatus.INFO, " Opening the url - "+CRMConstants.getEnvDetails().get("url"));
		driver.get(CRMConstants.getEnvDetails().get("url"));
		test.log(LogStatus.INFO, "URL Opened - "+CRMConstants.getEnvDetails().get("url"));
		LoginPage loginPage = new LoginPage(driver,test);
		PageFactory.initElements(driver, loginPage);
		return loginPage;
	}

	

}
