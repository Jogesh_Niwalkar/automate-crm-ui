/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class FarmerProfilePage extends BasePage {

	

	@FindBy(xpath=CRMConstants.FARMER_LOGOUT_BUTTON)
	public WebElement farmerLogoutButton;
	
	@FindBy(xpath=CRMConstants.PRODUCT_INQUIERY_BUTTON)
	public WebElement productInquiryButton;
	

	@FindBy(xpath=CRMConstants.ORDER_PLACED_BUTTON)
	public WebElement orderPlacedButton;
	

	@FindBy(xpath=CRMConstants.TAG_AND_LOGOUT_FARMER_BUTTON)
	public WebElement tagAndLogoutFarmerButton;
	
	@FindBy(xpath=CRMConstants.FARMER_PROFILE_TITLE)
	public WebElement farmerProfileTitle;
	
	@FindBy(xpath=CRMConstants.FARMER_PHONE_NUMBER)
	public WebElement farmerPhoneNumber;
	
	@FindBy(xpath=CRMConstants.PLACE_AN_ORDER_BUTTON)
	public WebElement placeAnOrderButton;
	
	@FindBy(xpath=CRMConstants.SEARCH_PRODUCT_BY_NAME_SKU_INPUTBOX)
	public WebElement searchProductByNameOrSKU;
	
	
	@FindBy(xpath=CRMConstants.PRODUCT_LIST)
	public WebElement productList;
	
	
	@FindBy(xpath=CRMConstants.NEXT_NAVIGATION_BUTTON)
	public WebElement nextNavigationButton;
	
	@FindBy(xpath=CRMConstants.CHECK_OUT_BUTTON)
	public WebElement checkOutButton;
	
	@FindBy(xpath=CRMConstants.IS_ORDER_HISTORY_TAB_SELECTED)
	public WebElement orderHistoryTab;
	
	@FindBy(xpath=CRMConstants.TICKET_HISTORY_TAB)
	public WebElement ticketHistoryTab;
	
	@FindBy(xpath=CRMConstants.FARMER_PROFILE_BUTTON)
	public WebElement farmerProfileButton;
	
	@FindBy(xpath=CRMConstants.ADD_REAL_CASH_TO_WALLET_LINK)
	public WebElement addRealCashToWallet_link;
	
	@FindBy(xpath=CRMConstants.ADD_AGROSTAR_POINTS_TO_WALLET_LINK)
	public WebElement addAgroStarPointsToWallet_link;
	
	@FindBy(xpath=CRMConstants.REAL_CASH_AMOUNT)
	public WebElement realCashAmountInWallet;
	
	@FindBy(xpath=CRMConstants.AGROSTAR_POINTS)
	public WebElement agrostarPointsInWallet;
	
	@FindBy(xpath=CRMConstants.PRODUCT_DETAILS_MODAL_PRODUCT_NAME)
	public WebElement productDetailsModalProductName;
	
	@FindBy(xpath=CRMConstants.ADD_TO_CART_PRODUCT_DETAILS_PAGE)
	public WebElement addToCartButtonOnProductDetailsPage;
	
	@FindBy(xpath=CRMConstants.EDIT_PROFILE_LINK)
	public WebElement editProfileLink;
	
	
	String isOrderPlacedButtonSelected;
	
	public FarmerProfilePage(WebDriver driver, ExtentTest test){
		super(driver,test);
	}
	

	public void farmerLogOut()
	{	
		test.log(LogStatus.INFO, "Logging Out Farmer....");
		wait.until(ExpectedConditions.elementToBeClickable(farmerLogoutButton));
		farmerLogoutButton.click();
		driver.switchTo().activeElement();
	//	boolean value=isElementPresent(CRMConstants.ORDER_PLACED_BUTTON);
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		wait.until(ExpectedConditions.elementToBeClickable(orderPlacedButton));
		orderPlacedButton.click();
		waitForPageLoad();
		
		
		
		wait.until(ExpectedConditions.elementToBeClickable(tagAndLogoutFarmerButton));
		tagAndLogoutFarmerButton.click();
		test.log(LogStatus.INFO, "Farmer has been logged out successfully!!!!");
		
		
	}
	
	
	public void farmerLogout_Button()
	{
		test.log(LogStatus.INFO, "Logging Out Farmer....");
		wait.until(ExpectedConditions.elementToBeClickable(farmerLogoutButton));
		farmerLogoutButton.click();
		waitForPageLoad();
		driver.switchTo().activeElement();
	}
	
	public void orderPlaced_Button()
	{	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		wait.until(ExpectedConditions.elementToBeClickable(orderPlacedButton));
		orderPlacedButton.click();
		isOrderPlacedButtonSelected=orderPlacedButton.getAttribute("class");
		System.out.println(isOrderPlacedButtonSelected);
		waitForPageLoad();
		
	}
	
	public void tagCallLogoutFarmer_Button()
	{
		
		wait.until(ExpectedConditions.elementToBeClickable(tagAndLogoutFarmerButton));
		tagAndLogoutFarmerButton.click();
		waitForPageLoad();
		test.log(LogStatus.INFO, " Farmer has been logged out successfully!!!!");
	}
	public void tagCallLogoutFarmerForOrderPlacedSelection_Button()
	{
		if(isOrderPlacedButtonSelected.equals("btn btn-default btn-block ng-valid ng-binding ng-scope active ng-dirty ng-valid-parse ng-touched"))
		{
			
			wait.until(ExpectedConditions.elementToBeClickable(tagAndLogoutFarmerButton));
			tagAndLogoutFarmerButton.click();
		}
		else
		{
			reportFailure("Failure : unable to perform Farmer Logout");
		}
		waitForPageLoad();
		test.log(LogStatus.INFO, "Farmer has been logged out successfully!!!!");
	}
	
	public String getAlternatePhoneNumberFromFarmerProfilePage(String NewPhoneNumber)
	{
		int phoneNumberCount=driver.findElements(By.xpath(CRMConstants.PHONENUMBER_COUNT_ON_PROFILE)).size();
		for(int i=1;i<=phoneNumberCount;i++)
		{
			String getAlternateNumberFromFarmerProfile=driver.findElement(By.xpath(CRMConstants.FARMER_ALTERNATE_PHONE_NUMBER_PART1+i+CRMConstants.FARMER_ALTERNATE_PHONE_NUMBER_PART2)).getText();
			
			if(getAlternateNumberFromFarmerProfile.equals(NewPhoneNumber))
			{
				return getAlternateNumberFromFarmerProfile;
			}
		}
	
		return "";
	}
	
	
	public String getAddressFromFarmerProfile()
	{
		String getAddress=driver.findElement(By.xpath(CRMConstants.FARMER_ADDRESS)).getText();
		
		return getAddress;
	}

	public void PlaceAnOrder_Button()
	{
		wait.until(ExpectedConditions.elementToBeClickable(placeAnOrderButton));
		placeAnOrderButton.click();
	}
	
	public void searchProductbyNameOrSKU_InputBox(String productName)
	{
	
		test.log(LogStatus.INFO, "Entering Product Name i.e "+productName);
		searchProductByNameOrSKU.sendKeys(productName);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		takeScreenShot();
		
	}
	
	public void clear_searchProductbyNameOrSKU_InputBox()
	{
		test.log(LogStatus.INFO, "Clearing SearchProduct_InputBox");
		searchProductByNameOrSKU.clear();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String selectProductFromProductList(String productName)
	{
		int productCountInList= driver.findElements(By.xpath(CRMConstants.PRODUCT_LIST)).size();
		for(int i=2;i<=productCountInList;i++)
		{
			String getProductNameFromPage=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1+i+CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2)).getText();
			
			if(getProductNameFromPage.equals(productName))
			{
				return getProductNameFromPage;
			}
		}
		if(nextNavigationButton.isEnabled())
		{
			
			wait.until(ExpectedConditions.elementToBeClickable(nextNavigationButton));
			nextNavigationButton.click();
		}
		return "";
		
	}
	
	public void selectProductFromProductNameLink(String productName)
	{
		int productCountInList= driver.findElements(By.xpath(CRMConstants.PRODUCT_LIST)).size();
		for(int i=2;i<=productCountInList;i++)
		{
			String getProductNameFromPage=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1+i+CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2)).getText();
			
			if(getProductNameFromPage.equals(productName))
			{
				driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1+i+CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2)).click();
			}
		}
	
		
	}
	
	public String  getProductNameFromProductDetailsModal()
	{
		String productName=productDetailsModalProductName.getText();
		return productName;
	}
	public void productQuantity()
	{
		
	}
	public void farmerProfile_Button()
	{	
		
		wait.until(ExpectedConditions.elementToBeClickable(farmerProfileButton));
		farmerProfileButton.click();
		
	}
	
	
	public void addToCart_Button(String Productname)
	{
		
		int productCountInList= driver.findElements(By.xpath(CRMConstants.PRODUCT_LIST)).size();
		for(int i=2;i<=productCountInList;i++)
		{
			String getProductNameFromPage=driver.findElement(By.xpath(CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART1+i+CRMConstants.PRODUCT_NAME_ON_PRODUCTS_PAGE_PART2)).getText();
			
			if(getProductNameFromPage.equals(Productname))
			{
				test.log(LogStatus.INFO, "Product Name :"+getProductNameFromPage+" has been selected and added to the Cart");
				driver.findElement(By.xpath(CRMConstants.ADD_TO_CART_BUTTON_ON_PRODUCTS_PAGE_PART1+i+CRMConstants.ADD_TO_CART_BUTTON_ON_PRODUCTS_PAGE_PART2)).click();
		
				
			}
		}
		
	/*	if(nextNavigationButton.isEnabled())
		{
			nextNavigationButton.click();
		}
		return "";*/
		
	}
	
	public void productDetailsModal_addToCart_Button()
	{
		wait.until(ExpectedConditions.elementToBeClickable(addToCartButtonOnProductDetailsPage));
		addToCartButtonOnProductDetailsPage.click();
		
	}
	
	public void checkOut_Button()
	{
		wait.until(ExpectedConditions.elementToBeClickable(checkOutButton));
		checkOutButton.click();
	}
	public void BillingAddress_RadioButton()
	{
		
	}
	
	
	public void internalNote_InputBox()
	{
		
	}
	public void verifyOrderDetails()
	{
		
	}
	
	public String openPlacedOrder(String ExpectedOrderId)
	{
		int orderCountOnOrderHistoryTab= driver.findElements(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_COUNT)).size();
		for(int i=1;i<=orderCountOnOrderHistoryTab;i++)
		{
			String getOrderid=driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).getText();
			
			if(getOrderid.equals(ExpectedOrderId))
			{
				test.log(LogStatus.INFO, "Clicking on Order Id :"+ getOrderid);
				waitForPageLoad();
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				takeScreenShot();
				driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).click();
				waitForPageLoad();
				return getOrderid;
			}
			
			
		}
		if(nextNavigationButton.isEnabled())
		{
			wait.until(ExpectedConditions.elementToBeClickable(nextNavigationButton));
			nextNavigationButton.click();
			openPlacedOrder(ExpectedOrderId);
		}
		
		return "";
		
	}
	
	
	public boolean verifyCancelledOrderOnOrderHistoryGrid(String expectedOrderId,String expectedOrderStatus)
	{
		int orderCountOnOrderHistoryTab= driver.findElements(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_COUNT)).size();
		for(int i=1;i<=orderCountOnOrderHistoryTab;i++)
		{
			String getOrderid=driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).getText();
			String getOrderStatus=driver.findElement(By.xpath(CRMConstants.ORDER_STATUS_PART1+i+CRMConstants.ORDER_STATUS_PART2)).getText();
			
			
			
			if(getOrderid.equals(expectedOrderId)&& getOrderStatus.equals(expectedOrderStatus))
			{
				//test.log(LogStatus.INFO, "Clicking on Order Id :"+ getOrderid);
				//driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).click();
				waitForPageLoad();
				return true;
			}
			
			
		}
		
		if(nextNavigationButton.isEnabled())
		{
			wait.until(ExpectedConditions.elementToBeClickable(nextNavigationButton));
			nextNavigationButton.click();
			openPlacedOrder(expectedOrderId);
		}
		return false;
		
	}
	

	public boolean verifyUpdatedOrderDetailsOnOrderHistoryGrid(String expectedUpdatedOrderId,String updatedOrderStatus,String earlierOrderId)
	{
		int orderCountOnOrderHistoryTab= driver.findElements(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_COUNT)).size();
		for(int i=1;i<=orderCountOnOrderHistoryTab;i++)
		{
			String expectedOrderStatusForOldOrderId=updatedOrderStatus+expectedUpdatedOrderId;
			String getUpdatedOrderId=driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).getText();
			String getUpdatedOrderStatus=driver.findElement(By.xpath(CRMConstants.ORDER_STATUS_PART1+i+CRMConstants.ORDER_STATUS_PART2)).getText();
			
			
			
			if(getUpdatedOrderId.equals(expectedUpdatedOrderId)&& getUpdatedOrderStatus.equals("CREATED"))
			{
				for(int j=1;j<=orderCountOnOrderHistoryTab;j++)
				{
					String getOldOrderId=driver.findElement(By.xpath(CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART1+j+CRMConstants.ORDER_HISTORY_TAB_ORDER_ID_PART2)).getText();
					String getUpdatedOrderStatusForOldOrderId=driver.findElement(By.xpath(CRMConstants.ORDER_STATUS_PART1+j+CRMConstants.ORDER_STATUS_PART2)).getText();
					if(getOldOrderId.equals(earlierOrderId)&& getUpdatedOrderStatusForOldOrderId.equals(expectedOrderStatusForOldOrderId))
					{
						return true;
					}
					
				}
				
			}
			
			
		}
		
		if(nextNavigationButton.isEnabled())
		{
			wait.until(ExpectedConditions.elementToBeClickable(nextNavigationButton));
			nextNavigationButton.click();
			verifyUpdatedOrderDetailsOnOrderHistoryGrid(expectedUpdatedOrderId,updatedOrderStatus,earlierOrderId);
		}
		return false;
		
	}

	
	public void orderHistory_Tab()
	{
		wait.until(ExpectedConditions.elementToBeClickable(orderHistoryTab));
		orderHistoryTab.click();
		waitForPageLoad();
	}
	
	public boolean isPhoneNumberAvailableOnProfilePage(String expectedPhoneNumber)
	{
		int getPhoneNumbersCount=driver.findElements(By.xpath(CRMConstants.FARMER_PHONE_NUMBER)).size();		
		for(int i =1;i<=getPhoneNumbersCount;i++)
		{
			String phoneNumberOnPage=driver.findElement(By.xpath(CRMConstants.FARMER_PHONE_NUMBER_PART1+i+CRMConstants.FARMER_PHONE_NUMBER_PART2)).getText();
			if(phoneNumberOnPage.equals(expectedPhoneNumber))
			{
				takeScreenShot();
				return true;
			}

		}	
		takeScreenShot();
		return false;
	}

	public boolean isExpectedFarmerNameAvailableOnProfilePage(String expectedFarmerName)
	{
		String getFarmerNameFromProfilePage=farmerProfileTitle.getText();
		if(getFarmerNameFromProfilePage.equals(expectedFarmerName))
		{
			takeScreenShot();
			return true ;
		}
		else
		{
			takeScreenShot();
			return false;
		}
		
	}
	
	
	public void gotoTicketHistoryTab()
	{
		wait.until(ExpectedConditions.elementToBeClickable(ticketHistoryTab));
		ticketHistoryTab.click();
		waitForPageLoad();
		
	}
	

	public boolean isTicketIdAvailableOnTicketHistoryTab(String expectedTicketId,String expectedOrderId,String productName)
	{
		int ticketCountOnTicketHistoryTab= driver.findElements(By.xpath(CRMConstants.TICKET_HISTORY_TAB_TICKET_COUNT)).size();
		for(int i=1;i<=ticketCountOnTicketHistoryTab;i++)
		{
			String getOrderid=driver.findElement(By.xpath(CRMConstants.TICKET_HISTORY_TAB_ORDER_ID_PART1+i+CRMConstants.TICKET_HISTORY_TAB_ORDER_ID_PART2)).getText();
			
			String getTicketId=driver.findElement(By.xpath(CRMConstants.TICKET_HISTORY_TAB_TICKET_ID_PART1+i+CRMConstants.TICKET_HISTORY_TAB_TICKET_ID_PART2)).getText();
			
			String getProductName=driver.findElement(By.xpath(CRMConstants.TICKET_HISTORY_TAB_PRODUCT_NAME_PART1+i+CRMConstants.TICKET_HISTORY_TAB_PRODUCT_NAME_PART2)).getText();
			
			if(getOrderid.equals(expectedOrderId)&& getTicketId.equals(expectedTicketId)&& getProductName.equalsIgnoreCase(productName))
			{
				
				return true;
			}
		
			
			
		}
		if(nextNavigationButton.isEnabled())
		{
			wait.until(ExpectedConditions.elementToBeClickable(nextNavigationButton));
			nextNavigationButton.click();
			isTicketIdAvailableOnTicketHistoryTab(expectedTicketId,expectedOrderId,productName);
		}
		
		return false;
		
	}
	
	
	
		
	public void addRealCashToWallet_Link()
	{
	wait.until(ExpectedConditions.elementToBeClickable(addRealCashToWallet_link));
	addRealCashToWallet_link.click();
	}
	
	public void addAgroStarPointsToWallet_Link()
	{	
		wait.until(ExpectedConditions.elementToBeClickable(addAgroStarPointsToWallet_link));
		addAgroStarPointsToWallet_link.click();
		
	}
	public String realCashInWallet()
	{
		String realCashInWallet=realCashAmountInWallet.getText();
		return realCashInWallet;
	}
	
	public String agrostarPointsInWallet()
	{
		String AgrostarPointsInWallet=agrostarPointsInWallet.getText();
		return AgrostarPointsInWallet;
	}
	
	
	public String getFarmerNameFromFarmerProfile()	
	{
		String FarmerName=driver.findElement(By.xpath(CRMConstants.FARMER_PROFILE_TITLE)).getText();
		return FarmerName;
		
	}
	public String getFarmersPhoneNumber()	
	{
		String PhoneNumber=driver.findElement(By.xpath(CRMConstants.FARMER_PHONE_NUMBER)).getText();
		return PhoneNumber;
		
	}
	public void editProfile_Link()
	{
		wait.until(ExpectedConditions.elementToBeClickable(editProfileLink));
		editProfileLink.click();
	}
	
	/*public void searchAndOpenOrderByOrderId(String OrderId)
	{
		
		int noOfordersCount=driver.findElements(By.xpath(CRMConstants.NO_OF_ORDERS_ON_GRID)).size();
		for(int i=1;i<=noOfordersCount;i++)
		{
			String getOrderId=driver.findElement(By.xpath(CRMConstants.SALES_ORDER_ID_PART1+i+CRMConstants.SALES_ORDER_ID_PART2)).getText();
			if(getOrderId.equals(OrderId))
			{
				takeScreenShot();
				String orderIdXpath=CRMConstants.SALES_ORDER_ID_PART1+i+CRMConstants.SALES_ORDER_ID_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(orderIdXpath)));
				driver.findElement(By.xpath(orderIdXpath)).click();
				waitForPageLoad();
				takeScreenShot();
				test.log(LogStatus.INFO, "Order Details Page with Order Id : "+ OrderId+" has been opened Successfully!");
				break;
				
			}
		
		}*/
	
	
	
}
