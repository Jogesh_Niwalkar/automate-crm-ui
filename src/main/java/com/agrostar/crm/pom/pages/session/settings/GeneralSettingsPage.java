//package com.agrostar.crm.pom.pages.session.settings;
//
//
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//
//
//import com.agrostar.crm.pom.base.BasePage;
//import com.agrostar.crm.pom.utility.CRMConstants;
//import com.relevantcodes.extentreports.ExtentTest;
//import com.relevantcodes.extentreports.LogStatus;
//
//public class GeneralSettingsPage extends BasePage{
//
///*	@FindBy(xpath=CRMConstants.PASSWORD_CHANGE)
//	public WebElement editPassword;
//	
//	@FindBy(xpath=CRMConstants.OLD_PASSWORD)
//	public WebElement oldPassword;
//	
//	@FindBy(xpath=CRMConstants.NEW_PASSWORD)
//	public WebElement newPassword;
//	
//	@FindBy(xpath=CRMConstants.CONFIRM_CHANGE)
//	public WebElement confirmPassword;
//	
//	@FindBy(xpath=CRMConstants.SAVE_CHANGES)
//	public WebElement saveChanges;
//	
//	@FindBy(xpath=CRMConstants.KILL_SESSION)
//	public WebElement killSessionRadio;
//	
//	@FindBy(xpath=CRMConstants.CONTINUE_PASSWORD_CHANGE_BUTTON)
//	public WebElement continuePasswdChange;
//	*/
//	
//	public GeneralSettingsPage(WebDriver driver,ExtentTest test){
//		this.driver=driver;
//		this.test=test;
//	}
//	
//	public void gotoPasswordChange(){
//		test.log(LogStatus.INFO, "clicking on password change");
//		if(!isElementPresent(CRMConstants.PASSWORD_CHANGE)){	
//			reportFailure("Element not found "+ CRMConstants.PASSWORD_CHANGE);
//		}
//			
//		editPassword.click();
//		test.log(LogStatus.INFO, "On password change Page");
//		
//
//	}
//
//	public String doPasswordChange(String oPassword,String nPassword) {
//		test.log(LogStatus.INFO, "Changing password");
//		oldPassword.sendKeys(oPassword);
//		newPassword.sendKeys(nPassword);
//		confirmPassword.sendKeys(nPassword);
//		saveChanges.click();
//		if(!isElementPresent(CRMConstants.PASSWORD_CHANGE))
//			return "Unsuccessful";
//		
//		killSessionRadio.click();
//		continuePasswdChange.click();
//		test.log(LogStatus.INFO, "Changed Password Successfully");
//		return "Success";
//	}
//}
