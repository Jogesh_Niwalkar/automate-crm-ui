/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class HomePage extends BasePage{
	
	
	@FindBy(xpath=CRMConstants.PROFILEPAGE_LINK)
	public WebElement profileLink;
	
	@FindBy(xpath=CRMConstants.FARMERPAGE_LINK)
	public WebElement farmerTabLink;
	
	@FindBy(xpath=CRMConstants.ADMINPAGE_LINK)
	public WebElement adminTabLink;
	
	@FindBy(xpath=CRMConstants.SEARCHORDERPAGE_LINK)
	public WebElement searchOrderTabLink;
	
	@FindBy(xpath=CRMConstants.PERFORMANCEPAGE_LINK)
	public WebElement performanceTabLink;
	
	
	
	@FindBy(xpath=CRMConstants.LOGOUT_BUTTON)
	public WebElement logOutButton;
	
	@FindBy(xpath=CRMConstants.LOGOUT_ICON)
	public WebElement logOutIcon;
	
	
	public HomePage(WebDriver driver,ExtentTest test){
		super(driver,test);
	}
	
	public ProfilePage gotoProfilePage() {
		test.log(LogStatus.INFO, "Going to profile page...");
		wait.until(ExpectedConditions.elementToBeClickable(profileLink));
		profileLink.click();
		ProfilePage profilePage = new ProfilePage(driver,test);
		PageFactory.initElements(driver, profilePage);
		return profilePage;
	}
	
	public Object logOut()
	{
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(CRMConstants.TOAST_CONTAINER)));
		
		test.log(LogStatus.INFO, "Logging Out....!");;
		wait.until(ExpectedConditions.elementToBeClickable(logOutIcon));
		logOutIcon.click();
		
		boolean isLogoutButtonClickable=isElementPresent(CRMConstants.LOGOUT_BUTTON);
		
		if(isLogoutButtonClickable)
		{
			wait.until(ExpectedConditions.elementToBeClickable(logOutButton));
			logOutButton.click();
			
			LoginPage logiPage=new LoginPage(driver, test);
			PageFactory.initElements(driver, logiPage);
			return logiPage;
			
		}
		else
		{
			HomePage homePage=new HomePage(driver, test);
			PageFactory.initElements(driver, homePage);
			return homePage;
			
		}
		
		
	

	}
	
	
	public Object goToFarmerSearchPage()
	{
		test.log(LogStatus.INFO, "Going to Farmer Page... ");
		boolean isFarmerTabClickable=isElementPresent(CRMConstants.FARMERPAGE_LINK);
		
		if (isFarmerTabClickable)
		{	
			test.log(LogStatus.INFO, "Clicking on Farmer Tab ");
			wait.until(ExpectedConditions.elementToBeClickable(farmerTabLink));
			farmerTabLink.click();
	//		waitForPageLoad();
			test.log(LogStatus.INFO, "Click Success!");	
			takeScreenShot();
			FarmerSearchPage farmerSearchPage = new FarmerSearchPage(driver,test);
			PageFactory.initElements(driver, farmerSearchPage);
			return farmerSearchPage;
		}
		else
		{	
			//test.log(LogStatus.FAIL, "Element Not Found : Unable to Click on Farmer Tab -  ");
			HomePage homePage = new HomePage(driver,test);
			PageFactory.initElements(driver, homePage);
			homePage.takeScreenShot();
			return homePage;
		}
		
	}
	
	
	public Object goToSearchOrderPage()
	{
		test.log(LogStatus.INFO, "Going to Search Order Page... ");
		boolean isSearchOrderTabClickable=isElementPresent(CRMConstants.SEARCHORDERPAGE_LINK);
		
		if (isSearchOrderTabClickable)
		{	
			test.log(LogStatus.INFO, " Clicking on Farmer Tab ");
			
			wait.until(ExpectedConditions.elementToBeClickable(searchOrderTabLink));
			searchOrderTabLink.click();
			test.log(LogStatus.INFO, "Click Success!");	
			takeScreenShot();
			SearchOrderPage searchOrderPage = new SearchOrderPage(driver,test);
			PageFactory.initElements(driver, searchOrderPage);
			return searchOrderPage;
		}
		else
		{	
			//test.log(LogStatus.FAIL, "Element Not Found : Unable to Click on Farmer Tab -  ");
			HomePage homePage = new HomePage(driver,test);
			PageFactory.initElements(driver, homePage);
			homePage.takeScreenShot();
			return homePage;
		}
		
	}
	
	public Object goToPerformancePage()
	{
		test.log(LogStatus.INFO, "Going to Performance Page... ");
		boolean isPerformanceTabClickable=isElementPresent(CRMConstants.PERFORMANCEPAGE_LINK);
		
		if (isPerformanceTabClickable)
		{	
			test.log(LogStatus.INFO, "Clicking on Performance Tab ");
			wait.until(ExpectedConditions.elementToBeClickable(performanceTabLink));
			performanceTabLink.click();
			waitForPageLoad();
			test.log(LogStatus.INFO, "Click Success!");	
			takeScreenShot();
			PerformancePage performancePage=new PerformancePage(driver, test);
			PageFactory.initElements(driver, performancePage);
			return performancePage;
		}
		else 
		{	
			//test.log(LogStatus.FAIL, "Element Not Found : Unable to Click on Farmer Tab -  ");
			HomePage homePage = new HomePage(driver,test);
			PageFactory.initElements(driver, homePage);
			homePage.takeScreenShot();
			return homePage;
		}
	}
	
	public Object goToAdminPage(String Username)
	{
		test.log(LogStatus.INFO, "Going to Admin Page... ");
		boolean isAdminTabClickable=isElementPresent(CRMConstants.ADMINPAGE_LINK);
		
		if (isAdminTabClickable)
		{	
			test.log(LogStatus.INFO, "Clicking on Admin Tab ");
			waitForPageLoad();
			wait.until(ExpectedConditions.elementToBeClickable(adminTabLink));
			adminTabLink.click();
			waitForPageLoad();
			test.log(LogStatus.PASS, "Click Success!");	
			takeScreenShot();
			AdminPage adminPage = new AdminPage(driver,test);
			PageFactory.initElements(driver, adminPage);
			return adminPage;
		}
		else
		{	
			test.log(LogStatus.INFO,"Admin Tab Link is not visible for the User: " + Username +" Hence "+ Username +" can not access Admin Page to create Offer");
			HomePage homePage = new HomePage(driver,test);
			PageFactory.initElements(driver, homePage);
			homePage.takeScreenShot();
			return homePage;
		}
		
	}
	
}
