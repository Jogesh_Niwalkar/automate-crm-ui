/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.agrostar.crm.pom.base.BasePage;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class PerformancePage extends BasePage{


	@FindBy(xpath=CRMConstants.IS_TODAYS_SALES_TAB_SELECTED)
	public WebElement isTodaysSalesTabSelected;
	
	@FindBy(xpath=CRMConstants.TODAYS_SALES_TAB)
	public WebElement todaysSalesTab;
	
	@FindBy(xpath=CRMConstants.	ADVANCED_ORDER_TAB)
	public WebElement advancedOrderTab;


	@FindBy(xpath=CRMConstants.NO_OF_ORDERS_ON_GRID)
	public WebElement noOfOrdersOnGrid;
	
	@FindBy(xpath=CRMConstants.NEXT_NAVIGATION_BUTTON)
	public WebElement nextNavigationButton;
	
	@FindBy(xpath=CRMConstants.NEXT_NAVIGATION_ARROW)
	public WebElement nextNavigationArrow;
	

	public PerformancePage(WebDriver driver,ExtentTest test){
		super(driver,test);
	}

	
	
	public void TodaysSalesTab_Click()
	{
		wait.until(ExpectedConditions.elementToBeClickable(todaysSalesTab));
		todaysSalesTab.click();
	}
	
	public void advancedOrderTab_Click()
	{
		wait.until(ExpectedConditions.elementToBeClickable(advancedOrderTab));
		advancedOrderTab.click();
	}
	public boolean isTodaysSalesTabSelected()
	{
		
		
		//a[contains(text(),'Today sales')]
		String isTodaysSalesTextAvailable=isTodaysSalesTabSelected.getText();
		if(isTodaysSalesTextAvailable.equals("Today sales"))
		{
			
			return true;
		}
		else
		{
			
			return false;
		}
	
	
	}
	
	public boolean isAdvancedOrderTabSelected()
	{
		
	
	
		return false;
	}
	
	public void isExpectedSalesOrderIdsVisible(ArrayList<String> listOfOrderId )
	{
		
		int orderIdCount=listOfOrderId.size();
		test.log(LogStatus.INFO, " Now verifying all the placed Orders .Total Order Count : "+orderIdCount);
		
		int j=orderIdCount-1; // list started with index 0 , list like 0,1,2,3...
		
		for(int i=1;i<=orderIdCount;i++)
		{
			String getOrderId=driver.findElement(By.xpath(CRMConstants.SALES_ORDER_ID_PART1+i+CRMConstants.SALES_ORDER_ID_PART2)).getText();
			
			/* First placed Order in Sales Order List on performance Page will be listed last. 
			so I need to traverse in Arraylist from last to first because I am storing the Order Id in the list at the time of Creation
			*/
			
			if(getOrderId.equals(listOfOrderId.get(j)))
			{
				takeScreenShot();
				test.log(LogStatus.PASS, "One of the Placed Order with Order Id: " +getOrderId+" is showing correctly on Today sales Tab ");
				
				--j; 
				
			}
			else
			{
				test.log(LogStatus.FAIL, "Unable to find the placed Order for Order Id : "+ listOfOrderId.get(j));
				takeScreenShot();
			}
			
		}
		
	

	}
	
	
	public void  isExpectedAdvancedOrderIdsVisible(ArrayList<String> listOfOrderId )
	{
		
		int advancedOrderIdsCount=driver.findElements(By.xpath(CRMConstants.NO_OF_ADVANCED_ORDERS_ON_GRID)).size();
		int expectedOrderIdCount=listOfOrderId.size();
		int j=expectedOrderIdCount-1; // list started with index 0 , list like 0,1,2,3...
		
		for(int i=1;i<=advancedOrderIdsCount;i++)
		{
			
			
			
				String getOrderId=driver.findElement(By.xpath(CRMConstants.ADVANCED_ORDER_ID_PART1+i+CRMConstants.ADVANCED_ORDER_ID_PART2)).getText();
			
				/* First placed Order in Sales Order List on performance Page will be listed last. 
				so I need to traverse in Arraylist from last to first because I am storing the Order Id in the list at the time of Creation
				 */
				for(int k=0;k<expectedOrderIdCount;k++)
				{
				
					if(getOrderId.equals(listOfOrderId.get(k)))
					{
						takeScreenShot();
						test.log(LogStatus.PASS, " Advance Order placed with Order Id: " +getOrderId+" is showing correctly on Advanced Order Tab ");
						//--j; 
				
					}
			
				}
				/*else
				{
					test.log(LogStatus.FAIL, "Unable to find the placed advanced Order for Order Id : "+ listOfOrderId.get(j));
					takeScreenShot();
				}*/
			
			}
			
			String disabledAttribute=nextNavigationArrow.getAttribute("disabled");
			if(disabledAttribute==null)
			{	
				wait.until(ExpectedConditions.elementToBeClickable(nextNavigationArrow));
				nextNavigationArrow.click();
			
				isExpectedAdvancedOrderIdsVisible(listOfOrderId );
			}
			else
				
			{
				test.log(LogStatus.INFO, "Pagnation Next Arrow is not clickabke!!!! ");
				//--j; 
			}
			
	
	}
		
		

	
	
	
	
	public boolean isExpectedFarmerNameVisible()
	{
		
	
	
		return false;
	}
	

	public boolean isExpectedCODAmountVisible()
	{
		
	
	
		return false;
	}
	
	public void searchAndOpenOrderByOrderId(String OrderId)
	{
		
		int noOfordersCount=driver.findElements(By.xpath(CRMConstants.NO_OF_ORDERS_ON_GRID)).size();
		for(int i=1;i<=noOfordersCount;i++)
		{
			String getOrderId=driver.findElement(By.xpath(CRMConstants.SALES_ORDER_ID_PART1+i+CRMConstants.SALES_ORDER_ID_PART2)).getText();
			if(getOrderId.equals(OrderId))
			{
				takeScreenShot();
				String orderIdXpath=CRMConstants.SALES_ORDER_ID_PART1+i+CRMConstants.SALES_ORDER_ID_PART2;
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(orderIdXpath)));
				driver.findElement(By.xpath(orderIdXpath)).click();
				waitForPageLoad();
				takeScreenShot();
				test.log(LogStatus.INFO, "Order Details Page with Order Id : "+ OrderId+" has been opened Successfully!");
				break;
				
			}
		
		}
	}
	
	
}
