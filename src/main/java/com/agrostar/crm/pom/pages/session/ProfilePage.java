/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.pages.session;

import org.openqa.selenium.WebDriver;

import com.agrostar.crm.pom.base.BasePage;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ProfilePage extends BasePage{

	
	public ProfilePage(WebDriver driver,ExtentTest test){
		super(driver,test);
	}
	
	
	public void verifyProfile() {
		test.log(LogStatus.INFO, " Verifying profile");
	}


}
