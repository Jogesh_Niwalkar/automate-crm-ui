package com.agrostar.crm.pom.base;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.agrostar.crm.pom.utility.CRMConstants;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BasePage {

	public WebDriver driver;
	//public TopMenu menu;
	public ExtentTest test;
	public WebDriverWait wait;
	public String getServerResponseInToasterMessage;
	public String getServerResponseInToasterTitle;
	
	public BasePage(){}
	
	public BasePage(WebDriver driver,ExtentTest test){
		this.driver=driver;
		this.test=test;
	//	menu = new TopMenu(driver, test);
		PageFactory.initElements(driver, test);
		wait=new WebDriverWait(driver, 10);
		//WebDriverWait wait;
	}
	
	public String verifyTitle(String expTitle){
		test.log(LogStatus.INFO, "Verifying the title " + expTitle);
		// webdriver code
		return "";
	}
	
	public String verifyText(String locator,String expText){
		return "";
	}
	
	
	public void switch_To_Active_Element()
	{
		driver.switchTo().activeElement();

	}
	
	public boolean isElementPresent(String locator){
		test.log(LogStatus.INFO, "Trying to find element -> "+locator);
		int s = driver.findElements(By.xpath(locator)).size();
		if(s==0){
			test.log(LogStatus.INFO, "Element not found");
			return false;
		}
		else{
			test.log(LogStatus.INFO, "Element found");
			return true;
		}
			
	}
	
	
	/*public TopMenu getMenu(){
		return menu;
	}*/
	
	public void takeScreenShot(){
		Date d=new Date();
		String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
		String filePath=CRMConstants.REPORTS_PATH+"screenshots//"+screenshotFile;
		// store screenshot in that file
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(scrFile, new File(filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO,test.addScreenCapture(filePath));
	}
	
	public void reportFailure(String failureMessage){
		test.log(LogStatus.FAIL, failureMessage);
		takeScreenShot();
		Assert.fail(failureMessage);
	}
	
	public void waitForPageLoad()
	{
		//wait =new WebDriverWait(driver, 10);
		test.log(LogStatus.INFO, "Page Loading....");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(CRMConstants.PAGE_LOADING_IMAGE)));
		
	
		
	}
	
	public void waitForToastNotificationToBeDisappeared()
	{
		//wait =new WebDriverWait(driver, 10);
		test.log(LogStatus.INFO, "Waiting For Toast Notification to be disappeared ");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(CRMConstants.TOAST_CONTAINER)));
		
	
		
	}
	public void scrollPageUsingJavaScriptExecutor(int dimension1,int dimension2)
	{	
	
		((org.openqa.selenium.JavascriptExecutor) driver).executeScript("scroll("+dimension1+","+dimension2+")");	
	
	
	}
	public void waitUntilProductsListLoaded()
	{
		//wait =new WebDriverWait(driver, 10);
		test.log(LogStatus.INFO, "Product List Loading....");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CRMConstants.PRODUCTS_IN_TABLE)));
		
	
		
	}
	
	public void isElementClickable()
	{
		
	}
	
	public String getServerResponseInToasterBothTitleNMessage()
	{	
		boolean isAnyServerResponseInToaster=isElementPresent(CRMConstants.TOASTER_MESSAGE);

		if (isAnyServerResponseInToaster)
		{	
		//	getServerResponseInToasterMessage=toasterMessage.getText();
			getServerResponseInToasterMessage=driver.findElement(By.xpath(CRMConstants.TOASTER_MESSAGE)).getText();
			getServerResponseInToasterTitle=driver.findElement(By.xpath(CRMConstants.TOASTER_TITLE)).getText();
			
			takeScreenShot();	
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(CRMConstants.TOASTER_MESSAGE)));
			return getServerResponseInToasterTitle+" "+getServerResponseInToasterMessage;

		}
		return "";


	}
	public String getServerResponseInToaster()
	{	
		boolean isAnyServerResponseInToaster=isElementPresent(CRMConstants.TOASTER_MESSAGE);

		if (isAnyServerResponseInToaster)
		{	
		//	getServerResponseInToasterMessage=toasterMessage.getText();
			getServerResponseInToasterMessage=driver.findElement(By.xpath(CRMConstants.TOASTER_MESSAGE)).getText();
		//	getServerResponseInToasterTitle=driver.findElement(By.xpath(CRMConstants.TOASTER_TITLE)).getText();
			
			takeScreenShot();	
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(CRMConstants.TOASTER_MESSAGE)));
			return getServerResponseInToasterMessage;

		}
		return "";


	}
	
	public void selectOptionWithText(String textToSelect) {
		try {
			//WebElement autoOptions = villageInputBox;
			WebElement autoOptions = driver.findElement(By.xpath(CRMConstants.VILLAGE_INPUTBOX));
			wait=new WebDriverWait(driver, 5);

			wait.until(ExpectedConditions.visibilityOf(autoOptions));

			List<WebElement> optionsToSelect = autoOptions.findElements(By.xpath("//a[@class='ng-binding']"));

			for(WebElement option : optionsToSelect)

			{
				if(option.getText().equals(textToSelect)) 
				{
					test.log(LogStatus.INFO, "Trying to select: "+textToSelect);
					System.out.println("Trying to select: "+textToSelect);
					option.click();
					break;
				}
			}

		} catch (Exception e) {
			System.out.println(e.getStackTrace());
		}

	}

	public void selectOptionWithIndex(int indexToSelect) {

		WebElement autoOptions = driver.findElement(By.xpath(CRMConstants.VILLAGE_INPUTBOX));
		//WebElement autoOptions = driver.findElement(By.xpath("//input[@placeholder='Village']"));
	
		wait.until(ExpectedConditions.visibilityOf(autoOptions));

		List<WebElement> optionsToSelect = autoOptions.findElements(By.xpath("//a[@class='ng-binding']"));


		if(indexToSelect<=optionsToSelect.size()) 
		{
			test.log(LogStatus.INFO, "Trying to select based on index: "+ indexToSelect);
			//System.out.println("Trying to select based on index: "+indexToSelect);
			//wait.until(ExpectedConditions.visibilityOfAllElements(optionsToSelect));
		try {
			Thread.sleep(2500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			optionsToSelect.get(indexToSelect-1).click();

		}
	} 	
	
}
