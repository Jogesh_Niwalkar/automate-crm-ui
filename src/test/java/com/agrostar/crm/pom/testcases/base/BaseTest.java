package com.agrostar.crm.pom.testcases.base;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;

import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.ExtentManager;
import com.agrostar.crm.pom.utility.Xls_Reader;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class BaseTest {

	public ExtentReports extentReport = ExtentManager.getInstance();
	public ExtentTest test;
	public WebDriver driver;
	public Xls_Reader xls_reader=new Xls_Reader(CRMConstants.DATA_XLS_PATH);	



	public void init(String browserType) {
		if(!CRMConstants.GRID_RUN)
		{
			if (browserType.equals("MozillaFireFox"))

				driver = new FirefoxDriver();

			else if (browserType.equals("GoogleChrome")) {

				System.setProperty("webdriver.chrome.driver", CRMConstants.CHROME_DRIVER_EXE);
				driver = new ChromeDriver();
			}
		}
		else
		{
			// grid
			DesiredCapabilities cap=null;
			if(browserType.equals("GoogleChrome"))
			{
				
				cap = DesiredCapabilities.chrome();
				 cap.setBrowserName("GoogleChrome");
				 cap.setPlatform(org.openqa.selenium.Platform.LINUX);
				
			}
			else if(browserType.equals("MozillaFireFox"))
			{
				cap = DesiredCapabilities.firefox();
				cap.setBrowserName("MozillaFireFox");
				cap.setJavascriptEnabled(true);
				cap.setPlatform(org.openqa.selenium.Platform.LINUX);
				
			}
			
			try 
			{
				driver = new RemoteWebDriver(new URL("http://localhost:4444/grid/hub"), cap);
			} 
			catch (Exception e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		//driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		test.log(LogStatus.INFO, "Browser: "+ browserType + " opened successfully ");

	}

	public void reportFailure(String failureMessage){
		test.log(LogStatus.FAIL, failureMessage);
		takeScreenShot();
		Assert.fail(failureMessage);
	}
	
	public void reportScriptError(String failureMessage)
	{
		test.log(LogStatus.ERROR, failureMessage);
		takeScreenShot();
		Assert.fail(failureMessage);
	}
	
	public boolean isElementPresent(String locator){
		test.log(LogStatus.INFO, "Trying to find element -> "+locator);
		int s = driver.findElements(By.xpath(locator)).size();
		if(s==0){
			test.log(LogStatus.INFO, "Element not found");
			return false;
		}
		else{
			test.log(LogStatus.INFO, "Element found");
			return true;
		}
			
	}

	public void takeScreenShot(){

		Date d=new Date();
		String screenshotFile=d.toString().replace(":", "_").replace(" ", "_")+".png";
		String filePath=CRMConstants.REPORTS_PATH+"screenshots//"+screenshotFile;
		// store screenshot in that file
		File scrFile = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

		try {
			FileUtils.copyFile(scrFile, new File(filePath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.log(LogStatus.INFO,test.addScreenCapture(filePath));
	}}
