/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.AdminPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class CreateAndThenDisableOfferAndVerifyOffersGrid extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Create an Offer
	 */

	/*
	 * Create And Then Disable Offer And Verify Offers Grid 1. Navigate to Admin
	 * Tab 2. Verify the Offer Tab is selected 3. Click on New Button 4. Enter
	 * the Offer Number 5. Enter the Offer Description 6. Set the Validity for
	 * Offer 7. Enter the value which denotes Number of times the coupon will
	 * get used 8. Select Entitlements - what kind of Cashback it is 9. Set the
	 * discount in Percentage/Values and Description 10.Select the Sources to
	 * whom Offer is applicable 11.Select the Products to which Offer is applied
	 * 12.Click on Create Offer button 13.Verify the Offer is created or not !
	 * 
	 */

	String testCaseName = "CreateAndThenDisableOfferAndVerifyOffersGrid";

	@Test(dataProvider = "getData")
	public void Create_And_Then_Disable_Offer_And_Verify_Offers_Grid_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Create_And_Then_Disable_Offer_And_Verify_Offers_Grid_Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {
			test.log(LogStatus.INFO, "Starting Create_And_Then_Disable_Offer_And_Verify_Offers_Grid_Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object returnObject = homePage.goToAdminPage(data.get("Username"));

				if (returnObject instanceof AdminPage) {

					AdminPage adminPage = (AdminPage) returnObject;
					boolean isOfferSideMenuSelected = adminPage.isOfferSideMenuSelectedByDefault();

					if (isOfferSideMenuSelected) {
						test.log(LogStatus.INFO, "Offer Side Menu is selected : Verified !! ");
						adminPage.createNewOffer_Button();
						adminPage.waitForPageLoad();
						String offerPageTitle = adminPage.getOfferPage_Title();
						if (offerPageTitle.equals("Offers")) {
							test.log(LogStatus.PASS, "Title  verified : Showing as expected i.e.: 'Offers' ");
						} else {
							test.log(LogStatus.FAIL, "Wrong title is showing on Offers Page i.e.: " + offerPageTitle);
						}
						adminPage.offerName_InputBox(data.get("OfferName"));
						adminPage.offerDescription_InputBox(data.get("OfferDescription"));
						adminPage.offerValidity_StartDate(data.get("OfferName"));
						adminPage.offerValidity_EndDate();
						adminPage.offerAppliesToMaxOrders(data.get("MaxNoOfUsages"));
						adminPage.offerType_Dropdown(data.get("Entitlements_OfferType"));
						adminPage.offerPercentDiscountValue_InputBox(data.get("PercentDiscountValue"));
						adminPage.offerPercentDiscountDescription_InputBox(data.get("PercentDiscountDescription"));
						adminPage.selectSourcesForOffer_Link();
						driver.switchTo().activeElement();
						adminPage.selectSourcesForOffer_CheckBox();
						adminPage.selectSourcesForOffer_SubmitButton();
						adminPage.SelectProductsForOffer_Link();
						driver.switchTo().activeElement();
						adminPage.searchProductsByNameOrSKU_InputBox(data.get("Products"));
						adminPage.selectProductFromProductListToApplyOffer(data.get("Products"),
								data.get("ProductSKUCode"));
						adminPage.selectProductsForOffer_SubmitButton();
						adminPage.createOffer_Button();

						String responseFromServer = adminPage.getServerResponseInToaster();

						if (responseFromServer.equals(data.get("OfferName"))) {

							test.log(LogStatus.PASS,
									"Offer : " + data.get("OfferName") + " has been created Successfully");
							test.log(LogStatus.INFO,
									"Now verifying the Offer Grid is showing the created Offer or not ");
							boolean IsOfferAvailableOnOfferGrid = adminPage
									.IsOfferAvailableOnGrid(data.get("OfferName"), data.get("OfferDescription"));

							if (IsOfferAvailableOnOfferGrid) {
								test.log(LogStatus.PASS, "Offer : " + data.get("OfferName")
										+ " is showing as expected on Offer Grid with defined validity ");

								test.log(LogStatus.INFO,
										"Now Disabling the newly created Offer i.e. " + data.get("OfferName"));

								boolean isOfferDisabledSuccessfully = adminPage.disableOffer(data.get("OfferName"),
										data.get("OfferDescription"));

								if (isOfferDisabledSuccessfully) {
									test.log(LogStatus.PASS, "Offer Name i.e. " + data.get("OfferName")
											+ " has been disabled successfully ");
									test.log(LogStatus.INFO, "Now verifying that Offer Name i.e. "
											+ data.get("OfferName") + " should not be visibled on Offers Grid ");
									boolean isOfferAvailableOnOfferGrid = adminPage
											.isOfferAvailableOnGridAfterDisabling(data.get("OfferName"));
									if (!isOfferAvailableOnOfferGrid) {

										test.log(LogStatus.PASS, "Offer : " + data.get("OfferName")
												+ " IS NOT showing as expected on offer Grid  because it is Disabled");
										takeScreenShot();
									} else {
										reportFailure("Offer IS still visible on Coupon Grid though it is disabled");
									}

								} else {
									reportFailure("Failure : unable to disable to Offer i.e. " + data.get("OfferName"));
								}

							} else {
								reportFailure("Failed to show Offer : " + data.get("OfferName") + " on Offer Grid");
							}

						} else {
							reportFailure("Sorry ..Unable to create an Offer with reason stated as " + " ' "
									+ responseFromServer + " ' ");
						}
					}

				}

			}
		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}

	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

}
