/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.OrderDetailsPage;
import com.agrostar.crm.pom.pages.session.SearchOrderPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.ExtentTestInterruptedException;
import com.relevantcodes.extentreports.LogStatus;

public class SearchAnOrderByOrderId extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Search An Order
	 */

	/*
	 * Search An Order By Order Id 1. Navigate to Farmer Tab 2. Search Farmer
	 * Profile By Name 3. Enter Name of Farmer and Click on Search Button 4.
	 * Select required Farmer Profile from the Farmers Grid 5. Click on Place An
	 * Order Button 6. Click on Add to Cart Button 7. Click on Checkout Button
	 * 8. Click on Create Order button to Create an Order 9. Save an Order Id
	 * 10. Logout from Farmer Profile 10. Go to the Search Order Page 11. verify
	 * user is able to Search an Order by order Id
	 */

	String testCaseName = "SearchAnOrderByOrderId";

	@Test(dataProvider = "getData")
	public void search_An_Order_By_Order_Id_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("SearchAnOrderByOrderId Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}
		try {

			test.log(LogStatus.INFO, "Starting Search_An_Order_By_Order_Id Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToSearchOrderPage();

				if (pageObj instanceof SearchOrderPage) {
					test.log(LogStatus.INFO, "User : " + data.get("Username") + " is permitted to search an Order");
					test.log(LogStatus.INFO, "Navigating to Farmer Tab to Place An Order");

					HomePage homePageObject = new HomePage(driver, test);
					PageFactory.initElements(driver, homePageObject);

					Object receivedObject = homePage.goToFarmerSearchPage();

					if (receivedObject instanceof FarmerSearchPage) {

						FarmerSearchPage farmerSearchPage = (FarmerSearchPage) receivedObject;
						Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));

						// FarmerSearchPage searchOrderPage=(FarmerSearchPage)
						// pageObj ;
						// Object
						// returnObject=searchOrderPage.(data.get("FarmerName"));
						if (returnObject instanceof FarmerProfilePage) {
							FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

							farmerProfilePage.PlaceAnOrder_Button();
							farmerProfilePage.waitForPageLoad();
							farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Products"));
							farmerProfilePage.waitUntilProductsListLoaded();
							String returnedProductName = farmerProfilePage
									.selectProductFromProductList(data.get("Products"));

							if (returnedProductName.equalsIgnoreCase(data.get("Products"))) {
								farmerProfilePage.addToCart_Button(returnedProductName);
								String ProductAddedToTheCart = farmerProfilePage
										.getServerResponseInToasterBothTitleNMessage();
								farmerProfilePage.waitForToastNotificationToBeDisappeared();

								String ExpectedMessageInToaster = "Added to Cart " + data.get("Products");
								if (ProductAddedToTheCart.equalsIgnoreCase(ExpectedMessageInToaster)) {
									test.log(LogStatus.INFO,
											"Product : " + data.get("Products")
													+ "has been added successsfully in cart with response in Toaster: i.e. "
													+ ProductAddedToTheCart);

									farmerProfilePage.checkOut_Button();
									farmerProfilePage.waitForPageLoad();
									test.log(LogStatus.INFO,
											"Now Verifying the Products added to bag is showing correct Or Not... ");
									OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
									PageFactory.initElements(driver, orderCheckOutPage);
									String returnedProductNameFromCheckoutPage = orderCheckOutPage
											.productsIn_MyBag(data.get("Products"));

									if (returnedProductNameFromCheckoutPage.equalsIgnoreCase(data.get("Products"))) {
										test.log(LogStatus.INFO, "Product : " + data.get("Products")
												+ " is able to see in My Bag successfully");

										test.log(LogStatus.INFO, "Now Placing an Order with products available in Bag");

										orderCheckOutPage.createOrder_Button();
										orderCheckOutPage.waitForPageLoad();
										orderCheckOutPage.switch_To_Active_Element();
										String OrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
												data.get("OrderSuccess_ContentMessage"),
												data.get("OrderSuccess_ContentDetails"));

										if (!OrderId.isEmpty()) {
											test.log(LogStatus.INFO,
													"Order has been placed successfully with Order Id : " + OrderId);

											try {

												Thread.sleep(1000);
											} catch (InterruptedException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											orderCheckOutPage.closeModalDialog();
											orderCheckOutPage.waitForPageLoad();

											test.log(LogStatus.INFO, "Logging out from Farmer Profile");
											farmerProfilePage.farmerLogOut();
											farmerSearchPage.waitForPageLoad();

											test.log(LogStatus.INFO, "Now searching an order by Order Id : " + OrderId
													+ "from Search Order Tab");

											homePageObject.goToSearchOrderPage();
											// homePageObject.waitForPageLoad();

											SearchOrderPage searchOrderPage = new SearchOrderPage(driver, test);
											PageFactory.initElements(driver, searchOrderPage);
											searchOrderPage.searchAndSelectOrderByOrderId(OrderId);
											OrderDetailsPage orderDetailsPage=new OrderDetailsPage(driver, test);
											PageFactory.initElements(driver, orderDetailsPage);
											boolean isFarmerLogOutButtonVisible=orderDetailsPage.isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);
											
											if(isFarmerLogOutButtonVisible)
											{
												String orderId=orderDetailsPage.getOrderIdFromOrderDetailsPage();
												
												if(orderId.equals(OrderId))		
												{
													test.log(LogStatus.PASS, "User is able to search an Order by Order Id : "+ OrderId);
													takeScreenShot();
													
												}
												else
												{
													reportFailure("Failure : Unable to find expected Order Id : "+OrderId +"  on Order Details Page  ");
													
												}
											}
											else
											{
												reportFailure("Failure : Unable to Search Order By Order Id  ");
												
											}	
												
											//	searchOrderPage.selectOrderByOrderId(OrderId);

										} else {
											reportFailure("Order Id is not available Order Confirmation Modal Dialog");
										}

										// orderCheckOutPage.

									} else {
										reportFailure("Unable to find Product in My Bag with reason : "
												+ returnedProductNameFromCheckoutPage);
									}

								} else {
									reportFailure("Unable to add product :" + data.get("Products")
											+ " in cart with response :" + ProductAddedToTheCart);
								}
							} else {
								reportFailure("Failure : Unable to find the Product : " + data.get("Products")
										+ "in product List");
							}

						} else if (returnObject instanceof HomePage) {
							reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
						}

						// homePage.logOut();

					} else if (pageObj instanceof HomePage) {
						reportFailure("Failure : Unable to navigate to the Search Order Page...May be Access Issue  ");
					}

				}
			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
