/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.OrderDetailsPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class PlaceAndThenCancelOrder extends BaseTest {

	String testCaseName = "PlaceAndThenCancelOrder";

	@Test(dataProvider = "getData")
	public void Place_And_Then_Cancel_Order_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("PlaceAndThenCancelOrder Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {
			test.log(LogStatus.INFO, "Starting Place_And_Cancel_Order Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						farmerProfilePage.PlaceAnOrder_Button();
						farmerProfilePage.waitForPageLoad();
						farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Products"));
						farmerProfilePage.waitForPageLoad();
						farmerProfilePage.waitUntilProductsListLoaded();
						String returnedProductName = farmerProfilePage
								.selectProductFromProductList(data.get("Products"));

						if (returnedProductName.equalsIgnoreCase(data.get("Products"))) {
							farmerProfilePage.addToCart_Button(returnedProductName);
							String ProductAddedToTheCart = farmerProfilePage
									.getServerResponseInToasterBothTitleNMessage();
							farmerProfilePage.waitForPageLoad();

							String ExpectedMessageInToaster = "Added to Cart " + data.get("Products");
							if (ProductAddedToTheCart.equalsIgnoreCase(ExpectedMessageInToaster)) {
								test.log(LogStatus.INFO,
										"Product : " + data.get("Products")
												+ "has been added successsfully in cart with response in Toaster: i.e. "
												+ ProductAddedToTheCart);

								farmerProfilePage.checkOut_Button();
								farmerProfilePage.waitForPageLoad();
								test.log(LogStatus.INFO,
										"Now Verifying the Products added to bag is showing correct Or Not... ");
								OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
								PageFactory.initElements(driver, orderCheckOutPage);
								String returnedProductNameFromCheckoutPage = orderCheckOutPage
										.productsIn_MyBag(data.get("Products"));

								if (returnedProductNameFromCheckoutPage.equalsIgnoreCase(data.get("Products"))) {
									test.log(LogStatus.INFO, "Product : " + data.get("Products")
											+ " is able to see in My Bag successfully");

									test.log(LogStatus.INFO, "Now Placing an Order with products available in Bag");

									orderCheckOutPage.createOrder_Button();
									orderCheckOutPage.waitForPageLoad();
									driver.switchTo().activeElement();
									String OrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
											data.get("OrderSuccess_ContentMessage"),
											data.get("OrderSuccess_ContentDetails"));
									try {
										Thread.sleep(1000);
									} catch (InterruptedException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									orderCheckOutPage.closeModalDialog();
									orderCheckOutPage.waitForPageLoad();
									if (!OrderId.isEmpty()) {
										test.log(LogStatus.INFO,
												"Order has been placed successfully with Order Id : " + OrderId);

										orderCheckOutPage.scrollPageUsingJavaScriptExecutor(0, 1000);

										test.log(LogStatus.INFO, "Opening Order Details Page for Order Id: " + OrderId);

										String ActualOrderId;
										boolean isOrderHistoryTabSelected = isElementPresent(
												CRMConstants.IS_ORDER_HISTORY_TAB_SELECTED);
										if (isOrderHistoryTabSelected) {

											ActualOrderId = farmerProfilePage.openPlacedOrder(OrderId);

										} else {
											farmerProfilePage.orderHistory_Tab();
											ActualOrderId = farmerProfilePage.openPlacedOrder(OrderId);

										}

										farmerProfilePage.waitForPageLoad();
										OrderDetailsPage orderDetailsPage = new OrderDetailsPage(driver, test);
										PageFactory.initElements(driver, orderDetailsPage);

										test.log(LogStatus.INFO,
												"Verifying the correct Order with order id is opened..");

										String OrderIdAvailableOnOrderDetailsPage = orderDetailsPage
												.getOrderIdFromOrderDetailsPage();

										if (ActualOrderId.equals(OrderIdAvailableOnOrderDetailsPage)) {
											test.log(LogStatus.INFO,
													"Order Details Page with Order Id :"
															+ OrderIdAvailableOnOrderDetailsPage
															+ " has been opened successfully");

											test.log(LogStatus.INFO,
													"Now Cancelling the placed Order with Order Id : " + ActualOrderId);

											orderDetailsPage.cancelOrder_button();
											orderDetailsPage.waitForPageLoad();
											String getServerResponse = orderDetailsPage.getServerResponseInToaster();

											String expectedOrderCancelledSuccessMessage = data
													.get("OrderCancelledSuccessMessage") + " " + ActualOrderId;
											if (getServerResponse.equals(expectedOrderCancelledSuccessMessage)) {
												test.log(LogStatus.PASS,
														"Order With Order Id :" + ActualOrderId
																+ " has been cancelled Successfully with server response "
																+ getServerResponse);
											} else {
												reportFailure("Unable to Cancel Order with Order Id :" + ActualOrderId
														+ " with server response " + getServerResponse);
											}

											test.log(LogStatus.INFO,
													"Now Verifying the Order Status of the order whether it is cancelled or not");

											String orderStatus = orderDetailsPage.getOrderStatusFromOrderDetailsPage();
											if (orderStatus.equals(data.get("OrderStatus"))) {
												test.log(LogStatus.PASS,
														"Order Status on Order Details Page is showing as expected i.e. "
																+ orderStatus);

											} else {
												reportFailure(
														"Failure : Order Status on Order Details Page is  showing "
																+ orderStatus + " which is incorrect");

											}

											orderDetailsPage.farmerProfile_Button();
											farmerProfilePage.waitForPageLoad();
											test.log(LogStatus.INFO, "Verifying Order Status on Farmer Profile Page");
											farmerProfilePage.scrollPageUsingJavaScriptExecutor(0, 900);
											boolean isOrderCancelled = farmerProfilePage
													.verifyCancelledOrderOnOrderHistoryGrid(ActualOrderId,
															data.get("OrderStatus"));

											if (isOrderCancelled) {
												test.log(LogStatus.PASS,
														"Order Status on Farmer Profile Page is showing as expected i.e. "
																+ data.get("OrderStatus"));

											} else {
												reportFailure(
														"Failure : Order Status on Farmer Profile Page is not showing as expected ");
											}

										} else {
											reportFailure("Failure : Incorrect Order Details Page is displaying");
										}

									}

									else {
										reportFailure(
												"Place Order Failed ! Unable to receive created Order Id From Order Confirmation Modal Dialog ");
									}

									farmerProfilePage.farmerLogout_Button();
									farmerProfilePage.orderPlaced_Button();
									farmerProfilePage.tagCallLogoutFarmerForOrderPlacedSelection_Button();

									farmerSearchPage.waitForPageLoad();

									homePage.logOut();
									// orderCheckOutPage.

								} else {
									reportFailure("Unable to find Product in My Bag with reason : "
											+ returnedProductNameFromCheckoutPage);
								}

							} else {
								reportFailure("Unable to add product :" + data.get("Products")
										+ " in cart with response :" + ProductAddedToTheCart);
							}
						} else {
							reportFailure("Failure : Unable to find the Product : " + data.get("Products")
									+ "in product List");
						}

					} else if (returnObject instanceof HomePage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

					// homePage.logOut();

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
