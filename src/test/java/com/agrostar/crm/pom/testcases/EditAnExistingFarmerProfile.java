/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class EditAnExistingFarmerProfile extends BaseTest {

	String testCaseName = "EditAnExistingFarmerProfile";

	@Test(dataProvider = "getData")
	public void Edit_An_Existing_Farmer_Profile_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("EditAnExistingFarmerProfile Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Edit_An_Existing_Farmer_Profile Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			// loginPage.takeScreenShot();
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {

				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			} 
			else if (page instanceof HomePage) {

				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object pageObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));

					if (pageObject instanceof FarmerProfilePage) {
						
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) pageObject;
						
						test.log(LogStatus.INFO, " Entered Phone Number: " + data.get("PhoneNumber")
								+ " is associated with Existing Farmer Profile ");

						test.log(LogStatus.INFO,
								"Verifying the Farmer Name and Phone Number before the updating it...");
						
						String farmerName = farmerProfilePage.getFarmerNameFromFarmerProfile();
						
						String PhoneNumber = farmerProfilePage.getFarmersPhoneNumber();
					
						if (PhoneNumber.contentEquals(data.get("PhoneNumber")) && farmerName.contentEquals(data.get("FirstName") + " " + data.get("MiddleName") + " " + data.get("LastName"))) {
							
							test.log(LogStatus.PASS, " Farmer Profile Verified....");
						
							takeScreenShot();
							
							String existingFarmersAddress = farmerProfilePage.getAddressFromFarmerProfile();
						
							test.log(LogStatus.INFO, "Clicking on Edit Profile Link to update the Farmer's Profile");
						
							farmerProfilePage.editProfile_Link();
						
							farmerProfilePage.switch_To_Active_Element();

							test.log(LogStatus.INFO, "Adding a new Smartphone Number " + data.get("NewPhoneNumber"));
							
							farmerSearchPage.phoneNumber_InputBox(data.get("NewPhoneNumber"));
							farmerSearchPage.selectLanguage_Dropdown(data.get("Language"));
							farmerSearchPage.smartPhoneAvailable_Checkbox();
							farmerSearchPage.addPhoneNumber_Button();
							String serverResponse=farmerSearchPage.getServerResponseInToaster().trim();
							if(!serverResponse.isEmpty())
							{
								reportFailure("Faillure Occued with Server Response :"+serverResponse);
							}
							
							Thread.sleep(2000);
							int availablePhoneNumbersCount = farmerSearchPage.getPhoneNumbersCountFromProfile();

							boolean isNewlyAddedPhoneNumberVisible = farmerSearchPage.isUpdatedPhoneNumberVisible(
									availablePhoneNumbersCount, data.get("NewPhoneNumber"));

							if (isNewlyAddedPhoneNumberVisible) {

								test.log(LogStatus.PASS, "Alternate Phone Number :" + data.get("NewPhoneNumber")
										+ " has been added on Farmer Profile Modal Dialog");

								takeScreenShot();
							
								test.log(LogStatus.INFO,"Now Adding the New Address to the Farmer Prfoile " );
								 
								farmerSearchPage.clearVillage_InputBox();
								farmerSearchPage.village_InputBox(data.get( "NewVillage"));
								farmerSearchPage.selectOptionWithIndex(1);
								farmerSearchPage.clearAddress_InputBox();
								farmerSearchPage.address_InputBox(data.get("NewAddress"));
								farmerSearchPage.submitFarmerDetails_Button();
								String getServerResponse = farmerSearchPage.getServerResponseInToaster().trim();
								farmerSearchPage.waitForToastNotificationToBeDisappeared();
								String expectedSuccessMessage = getServerResponse.replace(" ", "_");
								int indexOfBlankSpace = expectedSuccessMessage.indexOf("_");
								String getFinalExpectedServerResponse = getServerResponse.substring(indexOfBlankSpace)
										.trim();

								if (getFinalExpectedServerResponse.equals(data.get("ProfileEdited_SuccessMessage"))) 
								{
									test.log(LogStatus.PASS,
											"Farmer Profile associated with Phone Number :" + data.get("PhoneNumber")
													+ " has been edited successfully with Server Resposne: "
													+ getServerResponse);
								} 
								else 
								{
									reportFailure("Failed : Unable to edit the Farmer Profile with server response : "
											+ getServerResponse);

								}

								String getUpdatedAddress = farmerProfilePage.getAddressFromFarmerProfile();
								String getAlternatePhoneNumber = farmerProfilePage
										.getAlternatePhoneNumberFromFarmerProfilePage(data.get("NewPhoneNumber"));

								if (getUpdatedAddress.contains(data.get("NewAddress"))
										&& getAlternatePhoneNumber.contains(data.get("NewPhoneNumber")))
								{
									test.log(LogStatus.PASS, "Farmer Profile:" + data.get("FirstName") + "  "
											+ data.get("LastName")
											+ " has been edited/updated . updated PhoneNumber :"+ getUpdatedAddress +"and address : "+ getUpdatedAddress+"  is showing correctly on Profile Page !! ");
									takeScreenShot();
								}
								else 
								{
									reportFailure("Updated address and New Phone Number i.e " + getUpdatedAddress + " "
											+ getAlternatePhoneNumber
											+ " is not showing correctly on Farmer Profile Page");

								}
								
								test.log(LogStatus.INFO, "Now removing the New Phone Number and adding Old address to make test case dyanamic ");
							
								farmerProfilePage.editProfile_Link();
								
								farmerProfilePage.switch_To_Active_Element();

								test.log(LogStatus.INFO, "Removing Phone Number...");
								
								boolean isNewPhoneNumberRemoved =farmerSearchPage.removePhoneNumber(data.get("NewPhoneNumber"));
								
								
								test.log(LogStatus.INFO,"Now adding the Old Address to the Farmer Prfoile " );
								 
								farmerSearchPage.clearVillage_InputBox();
								farmerSearchPage.village_InputBox(data.get("Village"));
								farmerSearchPage.selectOptionWithIndex(1);
								farmerSearchPage.clearAddress_InputBox();
								farmerSearchPage.address_InputBox(data.get("Address"));
								farmerSearchPage.submitFarmerDetails_Button();
								String getServerResponse1 = farmerSearchPage.getServerResponseInToaster().trim();
								farmerSearchPage.waitForToastNotificationToBeDisappeared();
								String expectedSuccessMessage1 = getServerResponse1.replace(" ", "_");
								int indexOfBlankSpace1 = expectedSuccessMessage1.indexOf("_");
								String getFinalExpectedServerResponse1 = getServerResponse1.substring(indexOfBlankSpace1)
										.trim();

								if (getFinalExpectedServerResponse1.equals(data.get("ProfileEdited_SuccessMessage"))&&isNewPhoneNumberRemoved) 
								{
									test.log(LogStatus.INFO," New Phone number is removed and profile ss updated with Old Data.." );
								} 
								else 
								{
									test.log(LogStatus.WARNING,"Profile is not updated with Old Data " );
										

								}
								
								farmerProfilePage.farmerLogOut();
								farmerProfilePage.waitForToastNotificationToBeDisappeared();
								homePage.logOut();

							} 
							else 
							{
								reportFailure("Unable to find New Phone Number :" + data.get("NewPhoneNumber")
										+ " on Farmer Profile Modal");
							}

						} 
						else 
						{
							reportFailure("Required Phone Number and Farmer Name i.e  " + data.get("PhoneNumber")
									+ " , " + data.get("FirstName") + " " + data.get("MiddleName") + " "
									+ data.get("LasttName") + "IS NOT showing on Farmer Profile  ");
						}

					} 
					else if (pageObject instanceof FarmerSearchPage) 
					{
						reportFailure(" Entered Phone Number: " + data.get("PhoneNumber")
								+ " IS NOT associated with any Existing Farmer Profile . It may be New Number,Please see Extent reports");
					}

				} 
				else if (pageObj instanceof HomePage) 
				{
					reportFailure("Unable to navigate to the Farmer Search Page...!!!");
				}
			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}
}
