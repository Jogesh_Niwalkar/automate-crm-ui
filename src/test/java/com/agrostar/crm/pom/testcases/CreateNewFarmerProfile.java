/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class CreateNewFarmerProfile extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In 
	 * //2. Check logged in user has access to Create New Farmer Profile
	 */

	/*
	 * create New Farmer Profile
	 * 1. Navigate to Farmer Tab 
	 * 2. Enter the new Phone Number to create New Farmer Profile 
	 * 3. Click on Search Button 
	 * 4. Verify the Farmer Profile Window 
	 * 5. Enter all the field 
	 * 6. Click on Submit Button 
	 * 7.Verify the the CSR is landing on Farmer profile Page 
	 * 8. //Verify the Farmer Profile Content as CSR entered - UI Dependancy - Need to check
	 */

	String testCaseName = "CreateNewFarmerProfile";

	@Test(dataProvider = "getData")
	public void Create_New_Farmer_Profile_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("CreateNewFarmerProfile Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {
			
			test.log(LogStatus.INFO, "Starting Create_New_Farmer_Profile Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) 
			{
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			} 
			else if (page instanceof HomePage) 
			{

				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) 
				{

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;

					Object pageObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));

					if (pageObject instanceof FarmerProfilePage) 
					{
						test.log(LogStatus.INFO, " Entered Phone Number: " + data.get("PhoneNumber")+ " is associated with Existing Farmer Profile ");
						((FarmerProfilePage) pageObject).waitForPageLoad();
						((FarmerProfilePage) pageObject).takeScreenShot();
						((FarmerProfilePage) pageObject).farmerLogOut();
						// test.log(LogStatus.FAIL, "Test Failed."+" We can not
						// create new Farmer Profile because the Entered number
						// is associated with existing Farmer Profile");
						reportFailure("Test Failed."+ " We can not create new Farmer Profile because the Entered number is associated with existing Farmer Profile");

						driver.quit();
					} 
					else if (pageObject instanceof FarmerSearchPage) 
					{
						FarmerSearchPage farmerSearchPageObj2 = (FarmerSearchPage) pageObject;
						farmerSearchPageObj2.switch_To_New_Farmer_Form();
						farmerSearchPageObj2.selectLanguage_Dropdown(data.get("Language"));
						farmerSearchPageObj2.smartPhoneAvailable_Checkbox();
						farmerSearchPageObj2.firstName_InputBox(data.get("FirstName"));
						farmerSearchPageObj2.middleName_InputBox(data.get("MiddleName"));
						farmerSearchPageObj2.LastName_InputBox(data.get("LastName"));
						farmerSearchPageObj2.selectFarmerType_Dropdown(data.get("FarmerType"));
						farmerSearchPageObj2.farmersAge_InputBox(data.get("Age"));
						farmerSearchPageObj2.description_InputBox(data.get("Description"));
						farmerSearchPageObj2.selectIrrigationSource_Dropdown(data.get("IrrigationSource"));
						farmerSearchPageObj2.selectIrrigationType_Dropdown(data.get("IrrigationType"));
						farmerSearchPageObj2.crop_InputBox(data.get("Crop1"));
						farmerSearchPageObj2.crop_InputBox(data.get("Crop2"));
						farmerSearchPageObj2.crop_InputBox(data.get("Crop3"));
						farmerSearchPageObj2.selectHeardAboutAgroStar_Dropdown(data.get("HeardAboutAgroStar"));
						farmerSearchPageObj2.landHolding_InputBox(data.get("LandHolding"));
						farmerSearchPageObj2.selectLandHoldingUnit_Dropdown(data.get("LandHoldingUnits"));
						farmerSearchPageObj2.village_InputBox(data.get("Village"));
						farmerSearchPageObj2.selectOptionWithIndex(1);
						farmerSearchPageObj2.address_InputBox(data.get("Address"));
						farmerSearchPageObj2.submitFarmerDetails_Button();

						String getServerResponse = farmerSearchPage.getServerResponseInToaster();

						if (getServerResponse.contains(data.get("ProfileCreated_SuccessMessage")))
						{
							test.log(LogStatus.PASS,
									"Farmer Profile with Name : " + data.get("FirstName") + " " + data.get("MiddleName")
											+ " " + data.get("LastName") + " has been created Successfully");
							takeScreenShot();
							
							FarmerProfilePage farmerProfilePage=new FarmerProfilePage(driver, test);
							PageFactory.initElements(driver, farmerProfilePage);
							farmerProfilePage.farmerLogOut();
							farmerProfilePage.waitForToastNotificationToBeDisappeared();
							homePage.logOut();
						} 
						else 
						{
							reportFailure("Unable to create a New Farmer Profile with Server Respose: " + getServerResponse);

						}

					}

				} 
				else if (pageObj instanceof HomePage) 
				{
					reportFailure("Failure : Unable to click on Farmer Tab");
					
				}

			}

		} 
		catch (Throwable t) 
		{
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}

	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}
}
