
/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class AddProductToCartFromProductDetailsPage extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Place an Order
	 */

	/*
	 * Add Product To Cart From Product Details Page 1. Navigate to Farmer Tab
	 * 2. Search Farmer Profile By Number 3. Enter Number of Farmer and Click on
	 * Search Button 4. Select required Farmer Profile from the Farmers Grid 5.
	 * Click on Place An Order Button 6. Click on Add to Cart Button 7. Click on
	 * Checkout Button 8. Click on Create Order button to Create an Order 9.
	 * Verify Order Details
	 */

	String testCaseName = "AddProductToCartFromProductDetailsPage";

	@Test(dataProvider = "getData")
	public void Add_Product_To_Cart_From_Product_Details_Page_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Add_Product_To_Cart_From_Product_Details_Page Test ");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Add_Product_To_Cart_From_Product_Details_Page Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						farmerProfilePage.PlaceAnOrder_Button();
						farmerProfilePage.waitForPageLoad();
						farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Products"));
						farmerProfilePage.waitUntilProductsListLoaded();
						String returnedProductName = farmerProfilePage
								.selectProductFromProductList(data.get("Products"));

						if (returnedProductName.equalsIgnoreCase(data.get("Products"))) {
							farmerProfilePage.selectProductFromProductNameLink(data.get("Products"));
							Thread.sleep(2000);
							farmerProfilePage.switch_To_Active_Element();

							String ProductName = farmerProfilePage.getProductNameFromProductDetailsModal();

							if (ProductName.equalsIgnoreCase(returnedProductName)) {
								test.log(LogStatus.INFO, "Product : " + ProductName
										+ " is showing as expected on Product Details Modal Dailog");

								test.log(LogStatus.INFO, "Adding Product : " + ProductName + " to the Cart");

								farmerProfilePage.productDetailsModal_addToCart_Button();
								// farmerProfilePage.waitForPageLoad();
								String ProductAddedToTheCart = farmerProfilePage.getServerResponseInToaster();
								// farmerProfilePage.waitForPageLoad();

								String ExpectedMessageInToaster = "1 " + data.get("Products") + " Added to Cart";
								if (ProductAddedToTheCart.equalsIgnoreCase(ExpectedMessageInToaster)) {
									test.log(LogStatus.INFO,
											"Product : " + data.get("Products")
													+ "has been added successsfully in cart with response in Toaster: i.e. "
													+ ProductAddedToTheCart);

									farmerProfilePage.checkOut_Button();
									farmerProfilePage.waitForPageLoad();
									test.log(LogStatus.INFO,
											"Now Verifying the Products added to bag is showing correct Or Not... ");
									OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
									PageFactory.initElements(driver, orderCheckOutPage);
									String returnedProductNameFromCheckoutPage = orderCheckOutPage
											.productsIn_MyBag(data.get("Products"));

									if (returnedProductNameFromCheckoutPage.equalsIgnoreCase(data.get("Products"))) {
										test.log(LogStatus.INFO, "Product : " + data.get("Products")
												+ " is able to see in My Bag successfully");
										test.log(LogStatus.PASS,
												"User : " + data.get("Username") + " is able to add Product : "
														+ ProductName
														+ " into the cart successfully from Product Details Page ");
										orderCheckOutPage.waitForPageLoad();
										farmerProfilePage.farmerLogOut();
										farmerSearchPage.waitForPageLoad();

										homePage.logOut();
										// orderCheckOutPage.

									} else {
										reportFailure("Unable to find Product in My Bag with reason : "
												+ returnedProductNameFromCheckoutPage);
									}

								} else {
									reportFailure("Unable to add product :" + data.get("Products")
											+ " in cart with response :" + ProductAddedToTheCart);
								}

							} else {
								test.log(LogStatus.FAIL, "Product : " + ProductName
										+ " is NOT showing as expected on Product Details Modal Dailog");
								takeScreenShot();
							}

						} else {
							reportFailure("Failure : Unable to find the Product : " + data.get("Products")
									+ "in product List");
						}

					} else if (returnObject instanceof FarmerSearchPage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

					// homePage.logOut();

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
