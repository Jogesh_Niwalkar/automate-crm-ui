/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.ArrayList;
import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.PerformancePage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class VerifyAdvancedOrderGridByPlacingMultipleFutureOrders extends BaseTest {

	ArrayList<String> listOfOrderId = new ArrayList<String>();
	int orderIdIndexingForList = 0;

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Place an Order
	 */

	/*
	 * Verify Todays Sales Grid By Placing Multiple Orders 1. Navigate to Farmer
	 * Tab 2. Search Farmer Profile By Number 3. Enter Number of Farmer and
	 * Click on Search Button 4. Select required Farmer Profile from the Farmers
	 * Grid 5. Click on Place An Order Button 6. Click on Add to Cart Button 7.
	 * Click on Checkout Button 8. Click on Create Order button to place an
	 * Order 9. place multiple orders using same process 10. logout from Farmer
	 * Profile 11. Go to Performance Page 12. verify Placed Order are showing on
	 * Todays sales Grid
	 */

	String testCaseName = "VerifyAdvancedOrderGridByPlacingMultipleFutureOrders";

	@Test(dataProvider = "getData")
	public void Verify_Todays_Sales_Grid_By_Placing_Multiple_Orders_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Verify_Advanced_Order_Grid_By_Placing_Multiple_Future_Orders_Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Verify_Advanced_Order_Grid_By_Placing_Multiple_Future_Orders Test ");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						String orderQuantity = data.get("NumberOfOrders");
						int noOfOrdersToBePlaced = Integer.parseInt(orderQuantity);

						for (int i = 1; i <= noOfOrdersToBePlaced; i++) {

							farmerProfilePage.PlaceAnOrder_Button();
							farmerProfilePage.waitForPageLoad();
							farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Products"));
							farmerProfilePage.waitUntilProductsListLoaded();
							String returnedProductName = farmerProfilePage
									.selectProductFromProductList(data.get("Products"));

							if (returnedProductName.equalsIgnoreCase(data.get("Products"))) {
								Thread.sleep(2000);
								farmerProfilePage.addToCart_Button(returnedProductName);
								// farmerProfilePage.waitForPageLoad();
								String ProductAddedToTheCart = farmerProfilePage
										.getServerResponseInToasterBothTitleNMessage();
								// farmerProfilePage.waitForPageLoad();

								String ExpectedMessageInToaster = "Added to Cart " + data.get("Products");
								if (ProductAddedToTheCart.equalsIgnoreCase(ExpectedMessageInToaster)) {
									test.log(LogStatus.INFO,
											"Product : " + data.get("Products")
													+ "has been added successsfully in cart with response in Toaster: i.e. "
													+ ProductAddedToTheCart);

									farmerProfilePage.checkOut_Button();
									farmerProfilePage.waitForPageLoad();
									test.log(LogStatus.INFO,
											"Now Verifying the Products added to bag is showing correct Or Not... ");
									OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
									PageFactory.initElements(driver, orderCheckOutPage);
									String returnedProductNameFromCheckoutPage = orderCheckOutPage
											.productsIn_MyBag(data.get("Products"));

									if (returnedProductNameFromCheckoutPage.equalsIgnoreCase(data.get("Products"))) {
										test.log(LogStatus.INFO, "Product : " + data.get("Products")
												+ " is able to see in My Bag successfully");

										test.log(LogStatus.INFO, "Now Placing an Order with products available in Bag");

										orderCheckOutPage.scrollPageUsingJavaScriptExecutor(0, 500);

										test.log(LogStatus.INFO,
												"Now Placing a advanced Order with products available in Bag");

										orderCheckOutPage.advanceOrderDate_InputBox();
										orderCheckOutPage.scrollPageUsingJavaScriptExecutor(0, 400);
										int orderDate = orderCheckOutPage.advancedOrderDate();

										if (!(orderDate == 0)) {
											test.log(LogStatus.INFO,
													"Selecting Date:" + orderDate + " for an advanced Order..!  ");
											takeScreenShot();

										} else {
											reportFailure(
													"Unable to select Expected Advance Order...Please see Logs.  received  Date is : "
															+ orderDate);
										}
										orderCheckOutPage.selectSource_Inputbox();
										orderCheckOutPage.selectSource_Dropdown(data.get("Source"));
										orderCheckOutPage.internalNote_Inputbox(data.get("InternalNote"));
										orderCheckOutPage.createOrder_Button();
										orderCheckOutPage.waitForPageLoad();
										driver.switchTo().activeElement();
										String OrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
												data.get("OrderSuccess_ContentMessage"),
												data.get("OrderSuccess_ContentDetails"));

										if (!OrderId.isEmpty()) {
											test.log(LogStatus.PASS,
													"Order has been placed successfully with Order Id : " + OrderId);
										} else {
											reportFailure(
													"Place Order Failed ! Unable to receive created Order Id From Order Confirmation Modal Dialog ");
										}

										try {

											Thread.sleep(1000);
										} catch (InterruptedException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										orderCheckOutPage.closeModalDialog();
										orderCheckOutPage.waitForPageLoad();

										listOfOrderId.add(OrderId);

									} else {
										reportFailure("Unable to find Product in My Bag with reason : "
												+ returnedProductNameFromCheckoutPage);
									}

								} else {
									reportFailure("Unable to add product :" + data.get("Products")
											+ " in cart with response :" + ProductAddedToTheCart);
								}
							} else {
								reportFailure("Failure : Unable to find the Product : " + data.get("Products")
										+ "in product List");
							}

						}

						farmerProfilePage.farmerLogOut();
						farmerSearchPage.waitForPageLoad();
						farmerSearchPage.goToPerformancePageFromFarmerSearchPage();
						farmerSearchPage.waitForPageLoad();
						PerformancePage performancePage = new PerformancePage(driver, test);
						PageFactory.initElements(driver, performancePage);

						test.log(LogStatus.INFO, "Clicking on Advanced Order tab");
						performancePage.advancedOrderTab_Click();
						performancePage.waitForPageLoad();
						test.log(LogStatus.INFO,
								" Now verifying all the placed advanced Orders on Advanced Order Tab... ");
						performancePage.isExpectedAdvancedOrderIdsVisible(listOfOrderId);

						homePage.logOut();

					} else if (returnObject instanceof HomePage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

					// homePage.logOut();

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	public void storeOrderIdToVerify(String OrderId) {

		ArrayList<String> listOfOrderId = new ArrayList<String>();
		listOfOrderId.add(OrderId);

	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
