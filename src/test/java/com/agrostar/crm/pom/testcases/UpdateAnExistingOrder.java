/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.OrderDetailsPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class UpdateAnExistingOrder extends BaseTest {

	/*
	 * Pre Conditions 
	 * 1. Check User has access to Log In 
	 * //2. Check logged in user has access to Place an Order
	 */

	/*
	 * UpdateAnExistingOrder
	 * 1. Navigate to Farmer Tab 
	 * 2. Search Farmer Profile By
	 * Number 
	 * 3. Enter Number of Farmer and Click on Search Button
	 *  4. Select
	 * required Farmer Profile from the Farmers Grid 
	 * 5. Click on Place An Order
	 * Button
	 *  6. Click on Add to Cart Button 
	 *  7. Click on Checkout Button 
	 *  8. Click on Create Order button to Create an Order 
	 *  9. Verify Order Details
	 *  10. Now Open placed Order
	 * 11 . Click on Edit  Order button
	 * 12. Add more Product
	 * 13 Verify added Products in bag
	 * 14. And lcick on Update Order Button
	 * 15. And Verify updated Order
	 **/

	String testCaseName = "UpdateAnExistingOrder";

	@Test(dataProvider = "getData")
	public void Update_An_Existing_Order_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("UpdateAnExistingOrder Test ");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Update_An_Existing_Order Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage)
			{
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} 
			else if (page instanceof HomePage) 
			{
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage)
				{

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage)
					{
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						farmerProfilePage.PlaceAnOrder_Button();
						farmerProfilePage.waitForPageLoad();
						farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Product1"));
						farmerProfilePage.waitUntilProductsListLoaded();
						String returnedProductName = farmerProfilePage
								.selectProductFromProductList(data.get("Product1"));

						if (returnedProductName.equalsIgnoreCase(data.get("Product1")))
						{
							Thread.sleep(2000);
							farmerProfilePage.addToCart_Button(returnedProductName);
							// farmerProfilePage.waitForPageLoad();
							String ProductAddedToTheCart = farmerProfilePage
									.getServerResponseInToasterBothTitleNMessage();
							// farmerProfilePage.waitForPageLoad();

							String ExpectedMessageInToaster = "Added to Cart " + data.get("Product1");
							if (ProductAddedToTheCart.equalsIgnoreCase(ExpectedMessageInToaster))
							{
								test.log(LogStatus.INFO,
										"Product : " + data.get("Product1")
												+ "has been added successsfully in cart with response in Toaster: i.e. "
												+ ProductAddedToTheCart);

								farmerProfilePage.checkOut_Button();
								farmerProfilePage.waitForPageLoad();
								test.log(LogStatus.INFO,
										"Now Verifying the Products added to bag is showing correct Or Not... ");
								OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
								PageFactory.initElements(driver, orderCheckOutPage);
								String returnedProductNameFromCheckoutPage = orderCheckOutPage
										.productsIn_MyBag(data.get("Product1"));

								if (returnedProductNameFromCheckoutPage.equalsIgnoreCase(data.get("Product1")))
								{
									test.log(LogStatus.INFO, "Product : " + data.get("Product1")
											+ " is able to see in My Bag successfully");

									test.log(LogStatus.INFO, "Now Placing an Order with products available in Bag");

									orderCheckOutPage.createOrder_Button();
									orderCheckOutPage.waitForPageLoad();
									driver.switchTo().activeElement();
									String OrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
											data.get("OrderSuccess_ContentMessage"),
											data.get("OrderSuccess_ContentDetails"));

									if (!OrderId.isEmpty())
									{

										test.log(LogStatus.PASS,
												"Order has been placed successfully with Order Id : " + OrderId);

									} 
									else 
									{
										reportFailure(
												"Place Order Failed ! Unable to receive created Order Id From Order Confirmation Modal Dialog ");
									}

									try 
									{

										Thread.sleep(1000);
									} 
									catch (InterruptedException e) 
									{
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									orderCheckOutPage.closeModalDialog();
									// orderCheckOutPage.waitForPageLoad();

									test.log(LogStatus.INFO, "Opening the existing Order to update...");
									farmerProfilePage.scrollPageUsingJavaScriptExecutor(0, 1200);
									farmerProfilePage.openPlacedOrder(OrderId);

									OrderDetailsPage orderDetailsPage = new OrderDetailsPage(driver, test);
									PageFactory.initElements(driver, orderDetailsPage);
									test.log(LogStatus.INFO, "Clicking on Edit Order button to edit order...");
									takeScreenShot();
									Thread.sleep(2000);
									orderDetailsPage.editOrder_button();
									orderDetailsPage.waitForPageLoad();
									Thread.sleep(3000);
									test.log(LogStatus.INFO,
											"Clicking on add more products button to add more products...");
									takeScreenShot();
									orderCheckOutPage.addMoreProducts_Button();
									orderDetailsPage.waitForPageLoad();
									test.log(LogStatus.INFO, "Adding Product: " + data.get("Product2") + " in cart");
									takeScreenShot();
									farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Product2"));
									farmerProfilePage.waitUntilProductsListLoaded();
									String NameOfProduct2 = farmerProfilePage
											.selectProductFromProductList(data.get("Product2"));
									if (NameOfProduct2.equalsIgnoreCase(data.get("Product2")))
									{
										Thread.sleep(2000);
										farmerProfilePage.addToCart_Button(NameOfProduct2);
										// farmerProfilePage.waitForPageLoad();
										String product2AddedServerResponse = farmerProfilePage
												.getServerResponseInToasterBothTitleNMessage();
										// farmerProfilePage.waitForPageLoad();

										String ExpectedMessageInToasterForProduct2 = "Added to Cart "
												+ data.get("Product2");

										if (product2AddedServerResponse
												.equalsIgnoreCase(ExpectedMessageInToasterForProduct2))
										{
											takeScreenShot();
											test.log(LogStatus.INFO, "Product : " + data.get("Product2")
													+ "has been added successsfully to the Cart");
										} 
										else
										{
											reportFailure("Unable to add product :" + data.get("Product2")
													+ " in cart with response :" + product2AddedServerResponse);
										}
									}
									else
									{
										reportFailure("Failure : Unable to find the Product : " + data.get("Product2")
												+ "in product List");
									}
									// farmerProfilePage.clear_searchProductbyNameOrSKU_InputBox();

									farmerProfilePage.clear_searchProductbyNameOrSKU_InputBox();
									test.log(LogStatus.INFO, "Adding Product: " + data.get("Product3") + " in cart");

									farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Product3"));
									farmerProfilePage.waitUntilProductsListLoaded();
									String NameOfProduct3 = farmerProfilePage
											.selectProductFromProductList(data.get("Product3"));
									if (NameOfProduct3.equalsIgnoreCase(data.get("Product3")))
									{
										Thread.sleep(2000);
										farmerProfilePage.addToCart_Button(NameOfProduct3);
										// farmerProfilePage.waitForPageLoad();
										String product3AddedServerResponse = farmerProfilePage
												.getServerResponseInToasterBothTitleNMessage();
										// farmerProfilePage.waitForPageLoad();

										String ExpectedMessageInToasterForProduct3 = "Added to Cart "
												+ data.get("Product3");

										if (product3AddedServerResponse
												.equalsIgnoreCase(ExpectedMessageInToasterForProduct3))
										{
											takeScreenShot();
											test.log(LogStatus.INFO, "Product : " + data.get("Product3")
													+ "has been added successsfully to the Cart");
										}
										else
										{
											reportFailure("Unable to add product :" + data.get("Product3")
													+ " in cart with response :" + product3AddedServerResponse);
										}
									} 
									else
									{
										reportFailure("Failure : Unable to find the Product : " + data.get("Product3")
												+ "in product List");
									}

									farmerProfilePage.checkOut_Button();
									farmerProfilePage.waitForPageLoad();
									test.log(LogStatus.INFO,
											"Now Verifying the Products added to bag is showing correct Or Not... ");

									String Product1_Name = orderCheckOutPage.productsIn_MyBag(data.get("Product1"));
									String Product2_Name = orderCheckOutPage.productsIn_MyBag(data.get("Product2"));
									String Product3_Name = orderCheckOutPage.productsIn_MyBag(data.get("Product3"));

									if (Product1_Name.equalsIgnoreCase(data.get("Product1"))
											&& Product2_Name.equalsIgnoreCase(data.get("Product2"))
											&& Product3_Name.equalsIgnoreCase(data.get("Product3")))
									{
										test.log(LogStatus.INFO,
												"Products : " + data.get("Product1") + " , " + data.get("Product2")
														+ " , " + data.get("Product3") + " are available in My Bag ");
										takeScreenShot();
										test.log(LogStatus.INFO,
												"Now updating an Order with products available in Bag");

										orderCheckOutPage.updateAnOrder_Button();
										orderCheckOutPage.waitForPageLoad();
										orderCheckOutPage.switch_To_Active_Element();
										String updatedOrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
												data.get("OrderSuccess_ContentMessage"),
												data.get("EditedOrderSuccess_ContentDetails"));
										if (!updatedOrderId.isEmpty())
										{

											test.log(LogStatus.PASS,
													"Order has been updated Successfully with Order Id : "
															+ updatedOrderId);

										}
										else
										{
											reportFailure(
													"Update Order Failed ! Unable to receive Updated Order Id From Order Confirmation Modal Dialog   ");
										}

										try
										{

											Thread.sleep(1000);
										}
										catch (InterruptedException e)
										{
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										orderCheckOutPage.closeModalDialog();
										orderCheckOutPage.waitForPageLoad();
										test.log(LogStatus.INFO,
												"Now verifying the updated Order Status and details of the Order :"
														+ updatedOrderId + " on Order History Tab");
										test.log(LogStatus.INFO, "Clicking on Order History Tab");
										farmerProfilePage.scrollPageUsingJavaScriptExecutor(0, 1000);
										boolean isOrderHistoryTabSelected = isElementPresent(
												CRMConstants.IS_ORDER_HISTORY_TAB_SELECTED);
										if (!isOrderHistoryTabSelected)
										{
											farmerProfilePage.orderHistory_Tab();
										}

										boolean isUpdatedOrderStatusVerified = farmerProfilePage
												.verifyUpdatedOrderDetailsOnOrderHistoryGrid(updatedOrderId,
														data.get("UpdatedOrderStatus"), OrderId);
										if (isUpdatedOrderStatusVerified)
										{
											test.log(LogStatus.PASS,
													"Updated Order Status for Order Id " + updatedOrderId
															+ "is showing as expected on Order History Tab !!");
											test.log(LogStatus.PASS,
													"Also the Order Status of the older Order: " + OrderId
															+ " has been changed to " + data.get("UpdatedOrderStatus")
															+ updatedOrderId + " as expected !!!");
											takeScreenShot();
										} 
										else
										{
											reportFailure(
													"Updated Order Status IS NOT showing as expected on Order History Tab !!");
										}

									} 
									else
									{
										reportFailure("Failure :Unable to find Expected Products" + data.get("Product2")
												+ " ," + data.get("Product3") + " in My Bag ");

									}

								}
								else
								{
									reportFailure("Failure :Unable to find Expected Product " + data.get("Product1")
											+ " in My Bag ");
								}

							} 
							else
							{
								reportFailure("Unable to add product :" + data.get("Product1")
										+ " in cart with response :" + ProductAddedToTheCart);
							}

						}
						else
						{
							reportFailure("Failure : Unable to find the Product : " + data.get("Product1")
									+ "in product List");
						}

					}
					else if (returnObject instanceof HomePage)
					{
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

					// homePage.logOut();

				}
				else if (pageObj instanceof HomePage)
				{
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} 
		catch (Exception t)
		{
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit()
	{
		if (extentReport != null)
		{
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData()
	{
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
