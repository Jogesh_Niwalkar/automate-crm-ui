/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.WalletPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class AddAgroStarPointsToCustomersWalletUsingAddAndRemoveLink extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Place an Order
	 */

	/*
	 * Placing An Order 1. Navigate to Farmer Tab 2. Search Farmer Profile By
	 * Number 3. Enter Number of Farmer and Click on Search Button 4. Select
	 * required Farmer Profile from the Farmers Grid 5. Click on Place An Order
	 * Button 6. Click on Add to Cart Button 7. Click on Checkout Button 8.
	 * Click on Create Order button to Create an Order 9. Verify Order Details
	 */
	int initialAgroStarPointsInWallet;
	int finalAgroStarPointsInWallet;

	String testCaseName = "AddAgroStarPointsToCustomersWalletUsingAddAndRemoveLink";

	@Test(dataProvider = "getData")
	public void Add_AgroStar_Points_To_Customers_Wallet_Using_Add_And_Remove_Link_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Add_AgroStar_Points_To_Customers_Wallet_Using_Add_And_Remove_Link_Test ");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Add_AgroStar_Points_To_Customers_Wallet_Using_Add_And_Remove_Link Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {

						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;
						String agrostarPointsInWallet = farmerProfilePage.agrostarPointsInWallet();

						String totalAgroStarPointsInWallet = agrostarPointsInWallet.replace(" ", "-");
						int value = totalAgroStarPointsInWallet.indexOf("-");
						totalAgroStarPointsInWallet = agrostarPointsInWallet.substring(0, value);
						initialAgroStarPointsInWallet = Integer.parseInt(totalAgroStarPointsInWallet);

						test.log(LogStatus.INFO,
								"Initial AgroStar Points in wallet is : " + initialAgroStarPointsInWallet);
						takeScreenShot();
						test.log(LogStatus.INFO,
								"Clicking on Usable Points - > Add/Remove Link to open Wallet page... ");

						farmerProfilePage.addAgroStarPointsToWallet_Link();
						farmerProfilePage.waitForPageLoad();
						takeScreenShot();

						WalletPage walletPage = new WalletPage(driver, test);
						PageFactory.initElements(driver, walletPage);
						Object getObject = walletPage.isNavigatedToWalletPage();

						if (getObject instanceof WalletPage) {
							walletPage = (WalletPage) getObject;

							test.log(LogStatus.INFO, "Adding AgroStar Points into the Customers wallet ... ");
							takeScreenShot();

							test.log(LogStatus.INFO, "Selecting the Cash Type - AgroStar Points from Dropdown... ");
							walletPage.CashTypeDropdown_Click();
							walletPage.selectValueFromCashType_Dropdown(data.get("CashType"));

							test.log(LogStatus.INFO,
									"Selecting the Account Entry Type - Credit - Add Amount from Dropdown... ");
							walletPage.AccountEntryTypeDropdown_Click();
							walletPage.selectValueFromAccountEntryType_Dropdown(data.get("Accounting_Entry_Type"));

							test.log(LogStatus.INFO,
									" Crediting the amount:  " + data.get("Amount") + " to the Farmer's Wallet...");
							String amountAdded = walletPage.amount_InputBox(data.get("Amount"));

							test.log(LogStatus.INFO,
									" Selecting the transaction reason from Dropdown i.e. " + data.get("Reason"));
							walletPage.transactionReason_Click();
							walletPage.transactionReason_InputBox(data.get("Reason"));

							test.log(LogStatus.INFO, " Providing the remarks for the transaction");
							walletPage.remarks_InputBox(data.get("Remarks"));
							takeScreenShot();

							test.log(LogStatus.INFO, " Clicking on Update Wallet Button");
							walletPage.updateWalletButton();

							String serverResponse = walletPage.getServerResponseInToaster();

							if (serverResponse.equalsIgnoreCase(data.get("WalletUpdatedSuccessMessage"))) {

								test.log(LogStatus.PASS,
										"Wallet transaction has been completed successfully with server response : "
												+ serverResponse);
								takeScreenShot();
							} else {
								reportFailure(
										"Failure : Unable to get Success Messge for Wallet Update in toaster as expected ");
							}

							test.log(LogStatus.INFO, "Now verifying the updated AgroStar Points in wallet... ");
							test.log(LogStatus.INFO,
									"Initial AgroStar Points in wallet was " + initialAgroStarPointsInWallet);
							test.log(LogStatus.INFO,
									"Adding AgroStar Points in wallet is .: " + Integer.parseInt(amountAdded));
							int AmountAdded = Integer.parseInt(amountAdded);
							int totolAmount = AmountAdded + initialAgroStarPointsInWallet;
							test.log(LogStatus.INFO, "Expected AgroStar Points in wallet is Rs. " + totolAmount);

							test.log(LogStatus.INFO,
									"Now verifying the Total AgroStar Points amount on Farmer Profile Page ");

							test.log(LogStatus.INFO, "Clicking on Farmer Profile Button... ");
							walletPage.farmerProfile_Button();
							walletPage.waitForPageLoad();
							String expctedFinalAgroStarPointsInWallet = farmerProfilePage.agrostarPointsInWallet();

							totalAgroStarPointsInWallet = expctedFinalAgroStarPointsInWallet.replace(" ", "-");
							value = totalAgroStarPointsInWallet.indexOf("-");
							totalAgroStarPointsInWallet = totalAgroStarPointsInWallet.substring(0, value);
							finalAgroStarPointsInWallet = Integer.parseInt(totalAgroStarPointsInWallet);

							if (totolAmount == finalAgroStarPointsInWallet) {
								test.log(LogStatus.PASS, "Expected AgroStar Points In Wallet is " + totolAmount
										+ " which is showing correct on Farmer Profile Page as expected");
								takeScreenShot();
							} else {
								reportFailure("Failure : Expected AgroStar Points Amount in wallet is Rs. "
										+ totolAmount + " ," + "but showing " + finalAgroStarPointsInWallet
										+ " which is incorrect");
							}

						} else if (getObject instanceof FarmerProfilePage) {
							reportFailure("Failure : Unable to navigate to Wallet Page..");

						}

					} else if (returnObject instanceof FarmerSearchPage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

					// homePage.logOut();

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
