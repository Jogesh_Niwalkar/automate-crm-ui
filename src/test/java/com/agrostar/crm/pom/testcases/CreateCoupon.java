/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.AdminPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class CreateCoupon extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In 
	 * //2. Check logged in user has access to Create a Coupon
	 */

	/*
	 * Creating a Coupon
	 *  1. Navigate to Admin Tab 
	 *  2. Verify the Coupon Tab is selected 
	 * 3. Click on New Button
	 *  4. Enter the Coupon Code 
	 *  5. Enter the Coupon Description 
	 * 6. Set the Validity for Coupon
	 *  7. Enter the value which denotes Number of times the coupon will get used 
	 * 8. Select Entitlements - what kind of Cashback it is 
	 * 9. Set the discount in  Percentage/Values and Description 
	 * 10.Select the Sources to whom Coupon is applicable
	 * 11.Click on Create Coupon button
	 * 12.Verify the Coupon is created or not !
	 */

	String testCaseName = "CreateCoupon";

	@Test(dataProvider = "getData")
	public void Create_New_Coupon_Test(Hashtable<String, String> data) 
	{

		test = extentReport.startTest("Create_Coupon Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}
		try 
		{
			test.log(LogStatus.INFO, "Starting Create_Coupon Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) 
			{

				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			}
			else if (page instanceof HomePage) 
			{
				HomePage homePage = (HomePage) page;
				Object returnObject = homePage.goToAdminPage(data.get("Username"));
				
				if (returnObject instanceof AdminPage) 
				{

					AdminPage adminPage = (AdminPage) returnObject;
					adminPage.couponTab_Button();
					adminPage.waitForPageLoad();
					adminPage.createNewCoupon_Button();
					adminPage.waitForPageLoad();

					String CouponTitleOnPage = adminPage.getCouponPage_Title();
					if (CouponTitleOnPage.equals("Coupons")) 
					{
						test.log(LogStatus.PASS, "Title  verified : Showing as expected i.e.: 'Coupons' ");
					} 
					else 
					{
						test.log(LogStatus.FAIL, "Wrong title is showing on Offers Page i.e.: " + CouponTitleOnPage);
					}

					adminPage.couponCode_InputBox(data.get("CouponName"));
					adminPage.couponCodeDescription_InputBox(data.get("CouponDescription"));
					adminPage.couponValidity_StartDate(data.get("CouponName"));
					adminPage.couponValidity_EndDate();
					adminPage.couponAppliesToMaxOrders(data.get("MaxNoOfUsages"));
					adminPage.couponType_Dropdown(data.get("Entitlements_CouponType"));
					adminPage.PercentDiscountValue_InputBox(data.get("PercentDiscountValue"));
					adminPage.PercentDiscountDescription_InputBox(data.get("PercentDiscountDescription"));
					adminPage.selectSourcesForCoupon_Link();
					adminPage.switch_To_Active_Element();
					adminPage.selectSources_CheckBox();
				
					adminPage.selectSources_SubmitButton();
					adminPage.createCoupon_Button();
					String responseFromServer = adminPage.getServerResponseInToasterBothTitleNMessage();
					adminPage.waitForToastNotificationToBeDisappeared();

					if (responseFromServer.equals(data.get("CouponCreated_SuccessMessage")+" "+data.get("CouponName"))) 
					{
						test.log(LogStatus.PASS,
								"Offer : " + data.get("CouponName") + " has been created Successfully");
						
						test.log(LogStatus.INFO,
								"Now verifying the Coupon Grid is showing the created Coupon" + data.get("CouponName"));
						
						boolean IsCouponAvailableOnOfferGrid = adminPage.IsCouponAvailableOnGrid(data.get("CouponName"),
								data.get("CouponDescription"));

						if (IsCouponAvailableOnOfferGrid) 
						{
							test.log(LogStatus.PASS, "Coupon : " + data.get("CouponName")
									+ " is showing as expected on Coupon Grid with defined validity");
							homePage.logOut();

						} 
						else 
						{
							reportFailure("Failed to show Coupon : " + data.get("CouponName") + " on Coupon Grid");
						}

					} 
					else 
					{
						reportFailure("Sorry ..Unable to create an Coupon with reason stated as " + " ' "
								+ responseFromServer + " ' ");
					}

				} 
				else 
				{
					reportFailure("User can not access the Admin page...stopping the Test case ..Quitting driver...");

				}

			}

		} 
		catch (Exception t)
		{
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() 
	{
		if (extentReport != null) 
		{
			extentReport.endTest(test);
			extentReport.flush();
		}
		
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() 
	{
		return DataUtil.getData(xls_reader, testCaseName);
	}
}
