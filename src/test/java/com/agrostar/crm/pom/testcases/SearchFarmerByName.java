
/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class SearchFarmerByName extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Farmer
	 */

	/*
	 * Search Farmer By Name 1. Navigate to Farmer Tab 2. Search Farmer Profile
	 * By Name 3. Enter Name of Farmer and Click on Search Button 4. User should
	 * be able to Login to the Farmer Profile 5. Verify Farmer Name in Profile
	 */

	String testCaseName = "SearchFarmerByName";

	@Test(dataProvider = "getData")
	public void Search_Farmer_By_Name_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("SearchFarmerByName Test ");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Search_Farmer_By_Name Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByName(data.get("FarmerName"));

					if (returnObject instanceof FarmerProfilePage) {
						test.log(LogStatus.PASS, "Logged In User is able to search Farmer by Farmer Name ");
						takeScreenShot();
						test.log(LogStatus.PASS,
								"Also the User has been logged in successfully to the Farmer Profile ");

						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;
						boolean isExpectedFarmerNameAvailableOnPage = farmerProfilePage
								.isExpectedFarmerNameAvailableOnProfilePage(data.get("FarmerName"));
						if (isExpectedFarmerNameAvailableOnPage) {

							takeScreenShot();
							test.log(LogStatus.PASS, "Farmer's Name i.e." + data.get("FarmerName")
									+ " is SHOWING CORRECTLY on Farmer Profile Page");
						} else {
							reportFailure("Farmer's Name i.e." + data.get("FarmerName")
									+ " IS NOT showing correctly on Farmer Profile Page");
						}

					} else if (returnObject instanceof FarmerSearchPage) {
						FarmerSearchPage farmerSearchPage2 = (FarmerSearchPage) returnObject;

						boolean isFarmerNameSelected = farmerSearchPage2
								.isFarmerNameFromSearchFarmerGridSelected(data.get("FarmerName"));

						if (isFarmerNameSelected) {
							test.log(LogStatus.PASS, "Logged In User is able to search Farmer by Farmer Name ");
							test.log(LogStatus.PASS,
									"Also the User has been logged in successfully to the Farmer Profile ");
							FarmerProfilePage farmerProfilePage = new FarmerProfilePage(driver, test);
							PageFactory.initElements(driver, farmerProfilePage);
							boolean isExpectedFarmerNameAvailableOnPage = farmerProfilePage
									.isExpectedFarmerNameAvailableOnProfilePage(data.get("FarmerName"));
							if (isExpectedFarmerNameAvailableOnPage) {
								takeScreenShot();
								test.log(LogStatus.PASS, "Farmer's Name i.e." + data.get("FarmerName")
										+ " is SHOWING CORRECTLY on Farmer Profile Page");
							} else {
								reportFailure("Farmer's Name i.e." + data.get("FarmerName")
										+ " IS NOT showing correctly on Farmer Profile Page");
							}

						} else {
							reportFailure("There is no results for name " + data.get("FarmerName"));
						}

						// reportFailure( "Logged In User IS NOT able to search
						// Farmer by Phone Number... May Be the Phone Number is
						// INVALID...PLease refer the Screenshot above .");

					}

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
