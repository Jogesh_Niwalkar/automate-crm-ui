/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class AddNewBillingAddress extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to navigate Farmer Profile Page
	 */

	/*
	 * Add New Shipping Address 1. Navigate to Farmer Tab 2. Search Farmer
	 * Profile By Name 3. Enter Phone Number of Farmer and Click on Search
	 * Button 4. Click on Place An Order Button 5. Click on Add to Cart Button
	 * to add Product in cart 6. Click on Checkout Button 7. Click on Add New
	 * Address link to add Billing Address 8. Click on Submit Button 9. Verify
	 * Newly Added Billing Address
	 * 
	 */

	String testCaseName = "AddNewBillingAddress";

	@Test(dataProvider = "getData")
	public void addNewBillingAddressFromCheckoutPage_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("AddNewBillingAddress Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {
			test.log(LogStatus.INFO, "Starting Add_New_Billing_Address Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");
				// driver.quit();

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						farmerProfilePage.PlaceAnOrder_Button();
						farmerProfilePage.waitForPageLoad();
						farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Products"));
						farmerProfilePage.waitUntilProductsListLoaded();
						String returnedProductName = farmerProfilePage
								.selectProductFromProductList(data.get("Products"));

						if (returnedProductName.contains(data.get("Products"))) {
							farmerProfilePage.addToCart_Button(returnedProductName);

							String ProductAddedToTheCart = farmerProfilePage
									.getServerResponseInToasterBothTitleNMessage();
							farmerProfilePage.waitForPageLoad();

							String ExpectedMessageInToaster = "Added to Cart " + data.get("Products");
							if (ProductAddedToTheCart.contains(ExpectedMessageInToaster)) {
								test.log(LogStatus.INFO,
										"Product : " + data.get("Products")
												+ "has been added successsfully in cart with response in Toaster: i.e. "
												+ ProductAddedToTheCart);

								farmerProfilePage.checkOut_Button();
								farmerProfilePage.waitForPageLoad();
								test.log(LogStatus.INFO, "Now Adding the New Billing Address to the Profile ");

								OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
								PageFactory.initElements(driver, orderCheckOutPage);
								orderCheckOutPage.addNewBillingAddress_link();
								driver.switchTo().activeElement();
								orderCheckOutPage.village_InputBox(data.get("Village"));
								orderCheckOutPage.selectOptionWithIndex(1);

								orderCheckOutPage.address_InputBox(data.get("BillingAddress"));
								// Boolean
								// isPO_Tah_Dis_StateAvailable=orderCheckOutPage.retrivePO_PinCode_Taluka_District_State();
								orderCheckOutPage.waitForPageLoad();
								orderCheckOutPage.submitBillingNewAddress_Button();
								orderCheckOutPage.waitForPageLoad();

								String getServerResponse = orderCheckOutPage.getServerResponseInToaster();
								orderCheckOutPage.waitForPageLoad();
								orderCheckOutPage.billingAddress_viewMore_link();

								if (getServerResponse.equals(data.get("ExpectedSuccessMSGForBillingAddress"))) {
									test.log(LogStatus.PASS,
											"Billing Address has been added successfully with Server Response : "
													+ getServerResponse);
								} else {
									reportFailure("Failed to add Billing Address with Server Response : i.e."
											+ getServerResponse);
								}

								test.log(LogStatus.INFO,
										"Now verifying the Address is showing on Check Out page or not... ");

								boolean isAddressVerified = orderCheckOutPage
										.verifyCompleteBillingAddressOnCheckoutPage(data.get("Village"),
												data.get("BillingAddress"));

								if (isAddressVerified) {
									test.log(LogStatus.PASS,
											"Newly Added Billing address i.e." + data.get("BillingAddress")
													+ " is showing on correctly on Check Out Page");
								} else {
									reportFailure("Newly Added Billing address i.e." + data.get("BillingAddress")
											+ " is NOT showing on correctly on Check Out Page");
								}

								test.log(LogStatus.INFO, "now Removing the Product:  " + data.get("Products")
										+ "from the Bag by clicking on Empty Bag Button");

								orderCheckOutPage.emptyBag_Button();
								orderCheckOutPage.waitForPageLoad();

								test.log(LogStatus.INFO, "Cart is EMPTY NOW.");
								farmerProfilePage.farmerLogOut();
								farmerSearchPage.waitForPageLoad();
								homePage.logOut();

							} else {
								reportFailure("Failure : Unable to add Product to the Cart with server Response:"
										+ ProductAddedToTheCart);
							}

						} else {
							reportFailure("Failure : Unable to find the Product : " + data.get("Products")
									+ " in product List");

						}

					}

					else if (returnObject instanceof FarmerSearchPage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

				}

				else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}

	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

}
