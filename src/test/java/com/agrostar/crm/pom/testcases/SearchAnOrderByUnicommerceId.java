/** 
 * Copyright (C) 2016 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */
package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderDetailsPage;
import com.agrostar.crm.pom.pages.session.SearchOrderPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;
public class SearchAnOrderByUnicommerceId extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Search An Order
	 */

	/*
	 * Search An Order By Unicommerce Id 
	 *  1. Navigate to Search Order Tab
	 *  2. Search Order By Unicommerce Id
	 *  Verify User is able to search order by Unicommerse Id
	 */

	String testCaseName = "SearchAnOrderByUnicommerceId";

	@Test(dataProvider = "getData")
	public void Search_An_Order_By_Unicommerse_Id_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Search_An_Order_By_Unicommerse_Id_Test");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) 
		{
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}
		try 
		{

			test.log(LogStatus.INFO, "Starting Search_An_Order_By_Unicommerse_Id Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage)
			{
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason: " + getServerResponse + " . Quitting Driver");

			}
			else if (page instanceof HomePage)
			{
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToSearchOrderPage();

				if (pageObj instanceof SearchOrderPage)
				{
					test.log(LogStatus.INFO, "User : " + data.get("Username") + " is permitted to search an Order");
					test.log(LogStatus.INFO, "Navigating to Search Order Tab to search An Order by unicommerse Id");

					SearchOrderPage searchOrderPage = ( SearchOrderPage) pageObj;
					searchOrderPage.searchAndSelectOrderByUnicommerceId(data.get("UnicommerceId"));
					OrderDetailsPage orderDetailsPage=new OrderDetailsPage(driver, test);
					PageFactory.initElements(driver, orderDetailsPage);
					boolean isFarmerLogOutButtonVisible=orderDetailsPage.isElementPresent(CRMConstants.FARMER_LOGOUT_BUTTON);
					
					if(isFarmerLogOutButtonVisible)
					{
						String unicommerseId=orderDetailsPage.getUnicommerseIdFromOrderDetailsPage();
						
						if(unicommerseId.equals(data.get("UnicommerceId")))		
						{
							test.log(LogStatus.PASS, "User is able to search an Order by UniCommerse Id : "+ data.get("UnicommerceId"));
							takeScreenShot();
							
						}
						else
						{
							reportFailure("Failure : Unable to find expected Unicommerse Id on Order Details Page  ");
							
						}
					}
					else
					{
						reportFailure("Failure : Unable to Search Order By Unicommerse Id  ");
						
					}
					


				} 
				else if (pageObj instanceof HomePage)
				{
						reportFailure("Failure : Unable to navigate to the Search Order Page...May be Access Issue  ");
				}

			}
			

		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
