
/** 
 * Copyright (C) 2017 Agrostar.in 
 * All right reserved.
 * project_name : CRM AgroStar- Automation
 * ${time}
 * ${year}$
 * Author : Jogesh Niwalkar
 */

package com.agrostar.crm.pom.testcases;

import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.agrostar.crm.pom.pages.LaunchPage;
import com.agrostar.crm.pom.pages.LoginPage;
import com.agrostar.crm.pom.pages.session.FarmerProfilePage;
import com.agrostar.crm.pom.pages.session.FarmerSearchPage;
import com.agrostar.crm.pom.pages.session.HomePage;
import com.agrostar.crm.pom.pages.session.OrderCheckOutPage;
import com.agrostar.crm.pom.pages.session.OrderDetailsPage;
import com.agrostar.crm.pom.testcases.base.BaseTest;
import com.agrostar.crm.pom.utility.CRMConstants;
import com.agrostar.crm.pom.utility.DataUtil;
import com.relevantcodes.extentreports.LogStatus;

public class VerifyTicketHistoryGridByCreatingMultipleTickets extends BaseTest {

	/*
	 * Pre Conditions 1. Check User has access to Log In //2. Check logged in
	 * user has access to Create New Ticket For Placed Order
	 */

	/*
	 * Create_New_Ticket_For_PlacedOrder_Test 1. Navigate to Farmer Tab 2.
	 * Search Farmer Profile By Number 3. Enter Number of Farmer and Click on
	 * Search Button 4. Select required Farmer Profile from the Farmers Grid 5.
	 * Click on Place An Order Button 6. Click on Add to Cart Button 7. Click on
	 * Checkout Button 8. Click on Create Order button to Create an Order 9.
	 * Logout For Farmer profile 10. Click on Performance Page 11. Search And
	 * Open required Order by clicking on Order Id 12. Open Order Details Page
	 * 13. Click on create an Order Ticket button 14. Select an Order Issue from
	 * Dropdown 15. select Reason from Dropdown 14. ENter remarks Message 15.
	 * Click on Submit Button 16. Verify that Order Ticket has been created
	 * Successfully 17. Verify Order ticket id is showing on Order Details page
	 * as Expected
	 */

	String getTicketIdForProduct_1;
	String getTicketIdForProduct_2;
	String getTicketIdForProduct_3;

	String testCaseName = "VerifyTicketHistoryGridByCreatingMultipleTickets";

	@Test(dataProvider = "getData")
	public void Verify_Ticket_History_Grid_By_Creating_Multiple_Tickets_Test(Hashtable<String, String> data) {

		test = extentReport.startTest("Verify_Ticket_History_Grid_By_Creating_Multiple_Tickets Test ");
		System.out.println("TestCase Name :" + testCaseName);

		if (!DataUtil.isTestExecutable(xls_reader, testCaseName) || data.get(CRMConstants.RUNMODE_COL).equals("N")) {
			test.log(LogStatus.SKIP, "Skipping the test as Runmode is N");
			throw new SkipException("Skipping the test as Runmode is N");
		}

		try {

			test.log(LogStatus.INFO, "Starting Verify_Ticket_History_Grid_By_Creating_Multiple_Tickets Test");
			test.log(LogStatus.INFO, "Opening browser..." + (data.get("Browser")));
			init(data.get("Browser"));

			LaunchPage launchPage = new LaunchPage(driver, test);
			PageFactory.initElements(driver, launchPage);

			LoginPage loginPage = launchPage.gotoLoginPage();
			loginPage.takeScreenShot();
			test.log(LogStatus.INFO, "Logging in");
			Object page = loginPage.doLogin(data.get("Username"), data.get("Password"));

			if (page instanceof LoginPage) {
				String getServerResponse = loginPage.getServerResponseInToaster();
				reportFailure("CRM Login failed with reason : " + getServerResponse + " . Quitting Driver");

			} else if (page instanceof HomePage) {
				HomePage homePage = (HomePage) page;
				Object pageObj = homePage.goToFarmerSearchPage();

				if (pageObj instanceof FarmerSearchPage) {

					FarmerSearchPage farmerSearchPage = (FarmerSearchPage) pageObj;
					Object returnObject = farmerSearchPage.farmerSearchByPhoneNumber(data.get("PhoneNumber"));
					if (returnObject instanceof FarmerProfilePage) {
						FarmerProfilePage farmerProfilePage = (FarmerProfilePage) returnObject;

						farmerProfilePage.PlaceAnOrder_Button();

						String noOfProductsForOrder = data.get("NoOfProductsForOrder");
						int noOfProducts = Integer.parseInt(noOfProductsForOrder);
						String returnedProductName;
						for (int i = 1; i <= noOfProducts; i++) {
							farmerProfilePage.clear_searchProductbyNameOrSKU_InputBox();
							farmerProfilePage.waitForPageLoad();
							farmerProfilePage.searchProductbyNameOrSKU_InputBox(data.get("Product" + "_" + i));
							farmerProfilePage.waitUntilProductsListLoaded();
							returnedProductName = farmerProfilePage
									.selectProductFromProductList(data.get("Product" + "_" + i));
							farmerProfilePage.addToCart_Button(returnedProductName);
							farmerProfilePage.waitForToastNotificationToBeDisappeared();
						}

						farmerProfilePage.checkOut_Button();
						farmerProfilePage.waitForPageLoad();
						test.log(LogStatus.INFO,
								"Now Verifying the Products added to bag is showing correct Or Not... ");
						OrderCheckOutPage orderCheckOutPage = new OrderCheckOutPage(driver, test);
						PageFactory.initElements(driver, orderCheckOutPage);
						String returnedNameFromCheckoutPageFor_Product1 = orderCheckOutPage
								.productsIn_MyBag(data.get("Product_1"));
						String returnedNameFromCheckoutPageFor_Product2 = orderCheckOutPage
								.productsIn_MyBag(data.get("Product_2"));
						String returnedNameFromCheckoutPageFor_Product3 = orderCheckOutPage
								.productsIn_MyBag(data.get("Product_3"));

						if (returnedNameFromCheckoutPageFor_Product1.equalsIgnoreCase(data.get("Product_1"))
								&& returnedNameFromCheckoutPageFor_Product2.equalsIgnoreCase(data.get("Product_2"))
								&& returnedNameFromCheckoutPageFor_Product3.equalsIgnoreCase(data.get("Product_3"))) {
							test.log(LogStatus.INFO,
									"User is able to see expected Products i.e."
											+ returnedNameFromCheckoutPageFor_Product1 + " , "
											+ returnedNameFromCheckoutPageFor_Product2 + " , "
											+ returnedNameFromCheckoutPageFor_Product3 + " in  My Bag successfully");

							test.log(LogStatus.INFO, "Now Placing an Order with products available in Bag");

							orderCheckOutPage.createOrder_Button();
							orderCheckOutPage.waitForPageLoad();
							orderCheckOutPage.switch_To_Active_Element();
							String OrderId = orderCheckOutPage.orderConfirmation_ModalDialog(
									data.get("OrderSuccess_ContentMessage"), data.get("OrderSuccess_ContentDetails"));

							if (!OrderId.isEmpty()) {

								test.log(LogStatus.PASS,
										"Order has been placed successfully with Order Id : " + OrderId);

							} else {
								reportFailure(
										"Place Order Failed ! Unable to receive created Order Id From Order Confirmation Modal Dialog ");
							}

							try {

								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							orderCheckOutPage.closeModalDialog();
							orderCheckOutPage.waitForPageLoad();
							farmerProfilePage.scrollPageUsingJavaScriptExecutor(0, 900);

							String ActualOrderId;

							test.log(LogStatus.INFO,
									"Now Clicking on Order History Tab to search and open Order by Order Id");
							boolean isOrderHistoryTabSelected = isElementPresent(
									CRMConstants.IS_ORDER_HISTORY_TAB_SELECTED);
							if (isOrderHistoryTabSelected) {
								test.log(LogStatus.INFO, "Opening Order Details Page for Order Id: " + OrderId);
								ActualOrderId = farmerProfilePage.openPlacedOrder(OrderId);

							} else {
								test.log(LogStatus.INFO, "Opening Order Details Page for Order Id: " + OrderId);
								farmerProfilePage.orderHistory_Tab();
								ActualOrderId = farmerProfilePage.openPlacedOrder(OrderId);

							}

							farmerProfilePage.waitForPageLoad();

							OrderDetailsPage orderDetailsPage = new OrderDetailsPage(driver, test);
							PageFactory.initElements(driver, orderDetailsPage);

							test.log(LogStatus.INFO, "Verifying the correct Order with order id is opened..");
							String OrderIdAvailableOnOrderDetailsPage = orderDetailsPage
									.getOrderIdFromOrderDetailsPage();

							if (ActualOrderId.equals(OrderIdAvailableOnOrderDetailsPage)) {
								test.log(LogStatus.INFO, "Order Details Page with Order Id :"
										+ OrderIdAvailableOnOrderDetailsPage + " has been opened successfully");
								takeScreenShot();

							} else {
								reportFailure("Failure : Incorrect Order Id on Order Details Page is displaying");
							}

							orderDetailsPage.scrollPageUsingJavaScriptExecutor(0, 400);
							boolean isExpectedProduct_1_Available = orderDetailsPage
									.isExpectedProductAvailableOnPage(returnedNameFromCheckoutPageFor_Product1);

							if (isExpectedProduct_1_Available) {
								test.log(LogStatus.INFO, "Now creating Ticket for the Product :"
										+ returnedNameFromCheckoutPageFor_Product1);
								orderDetailsPage.newTicketButton_Click(returnedNameFromCheckoutPageFor_Product1);
								orderDetailsPage.switch_To_Active_Element();

								orderDetailsPage.selectItemSelectedDropdownBox_Click();
								orderDetailsPage.selectItemFromDropdown(data.get("Product_1"));
								orderDetailsPage.selectReasonDropdownBox_Click();
								orderDetailsPage.selectReasonFromDropdown(data.get("ItemIssueReason_Product_1"));

								orderDetailsPage.modelHeader();
								orderDetailsPage.orderIssueRemarks_Inputbox(data.get("Remarks_Product_1"));
								orderDetailsPage.orderIssueSubmit_Button();

								String getServerResponse = orderDetailsPage.getServerResponseInToaster();
								// orderCheckOutPage.waitForToastNotificationToBeDisappeared();
								getTicketIdForProduct_1 = orderDetailsPage.getGeneratedTicketIdForEachItemInOrder(
										returnedNameFromCheckoutPageFor_Product1);
								if (getServerResponse.equals(
										"Ticket No." + getTicketIdForProduct_1 + " was successfully created.")) {
									test.log(LogStatus.PASS, "Order Ticket with Ticket Id : " + getTicketIdForProduct_1
											+ "has been created successfully");
									takeScreenShot();
									test.log(LogStatus.PASS, "Also the generated Ticket Id " + getTicketIdForProduct_1
											+ " is showing as expected on Order Details Page");
									takeScreenShot();

								} else {
									reportFailure(
											"Sorry... Unfortunately unable to create Ticket with reason stated as ' "
													+ getServerResponse + " ' ");
								}

							}

							else {
								reportFailure("Failure : Ordered Products .i.e. " + data.get("Product_1")
										+ " are not visible on page accurately");
							}

							orderDetailsPage.scrollPageUsingJavaScriptExecutor(0, 400);
							boolean isExpectedProduct_2_Available = orderDetailsPage
									.isExpectedProductAvailableOnPage(returnedNameFromCheckoutPageFor_Product2);

							if (isExpectedProduct_2_Available) {
								test.log(LogStatus.INFO, "Now creating Ticket for the Product :"
										+ returnedNameFromCheckoutPageFor_Product2);
								test.log(LogStatus.INFO, "Clicking on New Ticket Button");
								orderDetailsPage.newTicketButton_Click(returnedNameFromCheckoutPageFor_Product2);
								orderDetailsPage.switch_To_Active_Element();
								orderDetailsPage.selectItemSelectedDropdownBox_Click();
								orderDetailsPage.selectItemFromDropdown(data.get("Product_2"));
								orderDetailsPage.selectReasonDropdownBox_Click();
								orderDetailsPage.selectReasonFromDropdown(data.get("ItemIssueReason_Product_2"));

								orderDetailsPage.modelHeader();
								orderDetailsPage.orderIssueRemarks_Inputbox(data.get("Remarks_Product_2"));
								orderDetailsPage.orderIssueSubmit_Button();

								String getServerResponse = orderDetailsPage.getServerResponseInToaster();
								// orderCheckOutPage.waitForToastNotificationToBeDisappeared();
								getTicketIdForProduct_2 = orderDetailsPage.getGeneratedTicketIdForEachItemInOrder(
										returnedNameFromCheckoutPageFor_Product2);
								if (getServerResponse.equals(
										"Ticket No." + getTicketIdForProduct_2 + " was successfully created.")) {
									test.log(LogStatus.PASS, "Order Ticket with Ticket Id : " + getTicketIdForProduct_2
											+ "has been created successfully");
									takeScreenShot();
									test.log(LogStatus.PASS, "Also the generated Ticket Id " + getTicketIdForProduct_2
											+ " is showing as expected on Order Details Page");
									takeScreenShot();

								} else {
									reportFailure(
											"Sorry... Unfortunately unable to create Ticket with reason stated as ' "
													+ getServerResponse + " ' ");
								}

							}

							else {
								reportFailure("Failure : Ordered Products .i.e. " + data.get("Product_2")
										+ " are not visible on page accurately");
							}

							orderDetailsPage.scrollPageUsingJavaScriptExecutor(0, 400);
							boolean isExpectedProduct_3_Available = orderDetailsPage
									.isExpectedProductAvailableOnPage(returnedNameFromCheckoutPageFor_Product3);

							if (isExpectedProduct_3_Available) {
								test.log(LogStatus.INFO, "Now creating Ticket for the Product :"
										+ returnedNameFromCheckoutPageFor_Product3);
								test.log(LogStatus.INFO, "Clicking on New Ticket Button");
								orderDetailsPage.newTicketButton_Click(returnedNameFromCheckoutPageFor_Product3);
								orderDetailsPage.switch_To_Active_Element();
								orderDetailsPage.selectItemSelectedDropdownBox_Click();
								orderDetailsPage.selectItemFromDropdown(data.get("Product_3"));
								orderDetailsPage.selectReasonDropdownBox_Click();
								orderDetailsPage.selectReasonFromDropdown(data.get("ItemIssueReason_Product_3"));

								orderDetailsPage.modelHeader();
								orderDetailsPage.orderIssueRemarks_Inputbox(data.get("Remarks_Product_3"));
								orderDetailsPage.orderIssueSubmit_Button();

								String getServerResponse = orderDetailsPage.getServerResponseInToaster();
								// orderCheckOutPage.waitForToastNotificationToBeDisappeared();
								getTicketIdForProduct_3 = orderDetailsPage.getGeneratedTicketIdForEachItemInOrder(
										returnedNameFromCheckoutPageFor_Product3);
								if (getServerResponse.equals(
										"Ticket No." + getTicketIdForProduct_3 + " was successfully created.")) {
									test.log(LogStatus.PASS, "Order Ticket with Ticket Id : " + getTicketIdForProduct_3
											+ "has been created successfully");
									takeScreenShot();
									test.log(LogStatus.PASS, "Also the generated Ticket Id " + getTicketIdForProduct_3
											+ " is showing as expected on Order Details Page");
									takeScreenShot();

								} else {
									reportFailure(
											"Sorry... Unfortunately unable to create Ticket with reason stated as ' "
													+ getServerResponse + " ' ");
								}

							}

							else {
								reportFailure("Failure : Ordered Product .i.e. " + data.get("Product_3")
										+ " are not visible on page accurately");
							}

							test.log(LogStatus.INFO, "Navigating to the Farmer Profile Page");
							orderDetailsPage.farmerProfile_Button();

							orderDetailsPage.scrollPageUsingJavaScriptExecutor(0, 600);
							test.log(LogStatus.INFO, "Clicking on Ticket History Tab");
							farmerProfilePage.gotoTicketHistoryTab();
							orderDetailsPage.scrollPageUsingJavaScriptExecutor(0, 950);
							boolean isTicketAvailableOnTicketHistoryPageForProduct1 = farmerProfilePage
									.isTicketIdAvailableOnTicketHistoryTab(getTicketIdForProduct_1, OrderId,
											returnedNameFromCheckoutPageFor_Product1);
							boolean isTicketAvailableOnTicketHistoryPageForProduct2 = farmerProfilePage
									.isTicketIdAvailableOnTicketHistoryTab(getTicketIdForProduct_2, OrderId,
											returnedNameFromCheckoutPageFor_Product2);
							boolean isTicketAvailableOnTicketHistoryPageForProduct3 = farmerProfilePage
									.isTicketIdAvailableOnTicketHistoryTab(getTicketIdForProduct_3, OrderId,
											returnedNameFromCheckoutPageFor_Product3);
							if (isTicketAvailableOnTicketHistoryPageForProduct1) {
								test.log(LogStatus.PASS,
										"Generated Ticket Id :" + getTicketIdForProduct_1 + " for Product :"
												+ returnedNameFromCheckoutPageFor_Product1
												+ " is showing on Ticket History Page as expected !!!");
								takeScreenShot();
							} else {
								reportFailure("Failure : Unable to find expected ticket id :" + getTicketIdForProduct_1
										+ "for Product: " + returnedNameFromCheckoutPageFor_Product1);

							}

							if (isTicketAvailableOnTicketHistoryPageForProduct2) {
								test.log(LogStatus.PASS,
										"Generated Ticket Id :" + getTicketIdForProduct_2 + " for Product :"
												+ returnedNameFromCheckoutPageFor_Product2
												+ " is showing on Ticket History Page as expected !!!");
								takeScreenShot();
							} else {
								reportFailure("Failure : Unable to find expected ticket id :" + getTicketIdForProduct_2
										+ "for Product: " + returnedNameFromCheckoutPageFor_Product2);
							}

							if (isTicketAvailableOnTicketHistoryPageForProduct3) {
								test.log(LogStatus.PASS,
										"Generated Ticket Id :" + getTicketIdForProduct_3 + " for Product :"
												+ returnedNameFromCheckoutPageFor_Product3
												+ " is showing on Ticket History Page as expected !!!");
								takeScreenShot();

							} else {
								reportFailure("Failure : Unable to find expected ticket id :" + getTicketIdForProduct_3
										+ "for Product: " + returnedNameFromCheckoutPageFor_Product3);
							}
							farmerProfilePage.farmerLogOut();
							farmerSearchPage.waitForPageLoad();
							farmerSearchPage.waitForToastNotificationToBeDisappeared();
							homePage.logOut();

						} else {
							reportFailure("User is NOT able to see expected Products in My Bag ");

						}

					} else if (returnObject instanceof HomePage) {
						reportFailure("Failure : Unable to navigate to the Farmer Profile Page");
					}

				} else if (pageObj instanceof HomePage) {
					reportFailure("Failure : Unable to navigate to the Farmer Search Page");
				}

			}
		} catch (Exception t) {
			System.out.println(t.getMessage());
			String errorReceived=t.getMessage();
			if(!errorReceived.isEmpty())
			{
				reportScriptError("Script Failure stated as ' "+ errorReceived+" ' ");
			}
			
		}
	}

	@AfterMethod
	public void quit() {
		if (extentReport != null) {
			extentReport.endTest(test);
			extentReport.flush();
		}
		if (driver != null)
			driver.quit();
	}

	@DataProvider
	public Object[][] getData() {
		return DataUtil.getData(xls_reader, testCaseName);
	}

}
